package com.woed.tomato.audio;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectSet;

public class SoundLoop implements Disposable {

    private Sound sound;
    private boolean isStarted;
    private boolean isPaused;
    private ObjectSet<Object> clients = new ObjectSet<>();

    public SoundLoop(FileHandle fileHandle) {
        this.sound = Gdx.audio.newSound(fileHandle);
    }

    public void checkIn(Object client) {
        clients.add(client);
        if (!isStarted) {
            sound.loop();
            isStarted = true;
        }
        if (isPaused) {
            sound.resume();
            isPaused = false;
        }
    }

    public void checkOut(Object client) {
        clients.remove(client);
        if (clients.size == 0) {
            sound.pause();
            isPaused = true;
        }
    }

    @Override
    public void dispose() {
        sound.stop();
        sound.dispose();
    }
}
