package com.woed.tomato.audio;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public abstract class MultipleSound implements Sound {

    private Array<Sound> sounds = new Array<>();
    private int lastIndex = -1;
    private boolean randomEnabled;
    private Sound currentSound;

    public MultipleSound() {
        this(false);
    }

    public MultipleSound(boolean randomEnabled) {
        this.randomEnabled = randomEnabled;
    }

    public Sound getNextSound() {
        int index;
        if (randomEnabled) {
            index = MathUtils.random(sounds.size - 1);
        } else {
            index = ++lastIndex % sounds.size;
        }
        return sounds.get(index);
    }

    @Override
    public long play() {
        currentSound.stop();
        return getNextSound().play();
    }

    @Override
    public long play(float volume) {
        currentSound.stop();
        return getNextSound().play(volume);
    }

    @Override
    public long play(float volume, float pitch, float pan) {
        currentSound.stop();
        return getNextSound().play(volume, pitch, pan);
    }

    @Override
    public long loop() {
        currentSound.stop();
        return getNextSound().loop();
    }

    @Override
    public long loop(float volume) {
        currentSound.stop();
        return getNextSound().loop(volume);
    }

    @Override
    public long loop(float volume, float pitch, float pan) {
        currentSound.stop();
        return getNextSound().loop(volume, pitch, pan);
    }

    @Override
    public void stop() {
        currentSound.stop();
    }

    @Override
    public void pause() {
        currentSound.stop();
    }

    @Override
    public void resume() {
        currentSound.resume();
    }

    @Override
    public void dispose() {
        for (Sound sound : sounds) {
            sound.dispose();
        }
    }

    @Override
    public void stop(long soundId) {
        currentSound.stop(soundId);
    }

    @Override
    public void pause(long soundId) {
        currentSound.pause(soundId);
    }

    @Override
    public void resume(long soundId) {
        currentSound.resume(soundId);
    }

    @Override
    public void setLooping(long soundId, boolean looping) {
        currentSound.setLooping(soundId, looping);
    }

    @Override
    public void setPitch(long soundId, float pitch) {
        currentSound.setPitch(soundId, pitch);
    }

    @Override
    public void setVolume(long soundId, float volume) {
        currentSound.setVolume(soundId, volume);
    }

    @Override
    public void setPan(long soundId, float pan, float volume) {
        currentSound.setPan(soundId, pan, volume);
    }
}
