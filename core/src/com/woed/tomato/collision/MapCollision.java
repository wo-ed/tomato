package com.woed.tomato.collision;

import com.badlogic.gdx.utils.Pool;

public class MapCollision implements Pool.Poolable {

    public MapCollision() {
    }

    public boolean left;
    public boolean right;
    public boolean top;
    public boolean bottom;
    public String groundType;

    public void reset() {
        left = false;
        right = false;
        top = false;
        bottom = false;
        groundType = null;
    }
}
