package com.woed.tomato.collision;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

public class CollisionCorrection implements Pool.Poolable {
    public Vector2 newPos = new Vector2();
    public boolean xCollision = false;
    public boolean yCollision = false;

    public CollisionCorrection() {
    }

    public void reset() {
        newPos.set(0, 0);
        xCollision = false;
        yCollision = false;
    }
}
