package com.woed.tomato.main;

public interface TipProvider {
    public String getCurrentTip();
}
