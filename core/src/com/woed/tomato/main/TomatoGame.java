package com.woed.tomato.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.woed.tomato.screen.MainMenuScreen;
import com.woed.tomato.screen.ScreenExchangeObject;
import com.woed.tomato.util.AdsController;
import com.woed.tomato.util.OsManager;

public class TomatoGame extends Game {

    public static final float RESOLUTION_X = 16 / 3;
    public static final float RESOLUTION_Y = 3;

    public static final float VIEWPORT_RESOLUTION_SCALE = 320;

    public static final float VIEWPORT_WIDTH = RESOLUTION_X * VIEWPORT_RESOLUTION_SCALE; //320 * 4 = 1280
    public static final float VIEWPORT_HEIGHT = RESOLUTION_Y * VIEWPORT_RESOLUTION_SCALE; //320 * 3 = 960

    private SpriteBatch batch;
    private ScreenExchangeObject exchangeObject;
    private boolean isUpdated;

    private OsManager osManager;

    private FPSLogger fpsLogger;
    private Color bgColor;

    public TomatoGame(OsManager osManager) {
        this.osManager = osManager;
    }

    @Override
    public void create() {
        batch = new SpriteBatch();
        exchangeObject = new ScreenExchangeObject(osManager);
        Viewport menuViewport = new FitViewport(TomatoGame.VIEWPORT_WIDTH, TomatoGame.VIEWPORT_HEIGHT);
        Viewport gameViewport = new ExtendViewport(TomatoGame.VIEWPORT_WIDTH, TomatoGame.VIEWPORT_HEIGHT);

        exchangeObject.setMenuViewport(menuViewport);
        exchangeObject.setGameViewport(gameViewport);
        exchangeObject.setBatch(batch);
        exchangeObject.setGame(this);
        Gdx.input.setInputProcessor(exchangeObject.getInputMultiplexer());
        Gdx.input.setCatchBackKey(true);
        fpsLogger = new FPSLogger();

        AdsController adsController = osManager.getAdsController();
        if (adsController.areAdsEnabled() && adsController.isWifiConnected()) {
            adsController.showBannerAd();
        }

        bgColor = Color.BLACK;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!isUpdated) {
            isUpdated = exchangeObject.areDefaultResourcesLoaded();
            if (isUpdated) {
                setScreen(new MainMenuScreen(exchangeObject));
            }
        }

        super.render();
        //fpsLogger.log();
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        exchangeObject.dispose();
    }

    protected ScreenExchangeObject getExchangeObject() {
        return exchangeObject;
    }
}
