package com.woed.tomato.main;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

public class TomatoTipProvider implements TipProvider {

    private Array<String> tips;
    private String currentTip;

    public TomatoTipProvider() {
        tips = new Array<>();
        tips.add("Have you tried dying less?");
        //tips.add("Tilt your device gently.")
        //tips.add("Yeah, that wasn't gently.")
        tips.add("They see me rollin' ...");
        tips.add("Don't ask me.");
        //tips.add("I can't play for you.");
        //tips.add("AFK");
        //tips.add("You shouldn't be here. Please commit suicide.");
        //tips.add("Shouldn't you be studying right now?");
        //tips.add("Just ONE more time!");
        //tips.add("Just make sure you don't die.");
        //tips.add("Can't help you with this one.");
        tips.add("Lol.");
        tips.add("Less dying, more beating the level.");
        tips.add("Jump over the ... nevermind.");
    }

    @Override
    public String getCurrentTip() {
        if (currentTip == null) {
            int index = MathUtils.random(tips.size - 1);
            currentTip = tips.get(index);
        }
        return currentTip;
    }
}
