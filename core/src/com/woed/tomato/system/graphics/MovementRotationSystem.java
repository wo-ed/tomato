package com.woed.tomato.system.graphics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.RotationSyncComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.system.physics.GravitySystem;

public class MovementRotationSystem extends IteratingSystem {

    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TextureRegionComponent> tm = ComponentMapper.getFor(TextureRegionComponent.class);
    private static final ComponentMapper<RotationSyncComponent> mm = ComponentMapper.getFor(RotationSyncComponent.class);
    private static final ComponentMapper<RotationComponent> rm = ComponentMapper.getFor(RotationComponent.class);
    private static final Family family = Family.all(RotationComponent.class, RotationSyncComponent.class, VelocityComponent.class).get();

    public MovementRotationSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        RotationComponent rotationComponent = rm.get(entity);
        RotationSyncComponent rotationSyncComponent = mm.get(entity);
        VelocityComponent velocity = vm.get(entity);

        double radians = Math.asin(velocity.x / GravitySystem.GRAVITY_Y) * rotationSyncComponent.rotationScale;
        double degrees = MathUtils.radiansToDegrees * radians;

        rotationComponent.rotation += degrees;
        rotationComponent.rotation %= 360;
    }
}
