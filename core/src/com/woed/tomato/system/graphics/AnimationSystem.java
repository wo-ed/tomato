package com.woed.tomato.system.graphics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.woed.tomato.component.graphics.AnimationComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.util.FloatTimer;

public class AnimationSystem extends IteratingSystem {

    private static final ComponentMapper<AnimationComponent> am = ComponentMapper.getFor(AnimationComponent.class);
    private static final ComponentMapper<TextureRegionComponent> tm = ComponentMapper.getFor(TextureRegionComponent.class);
    private static final Family family = Family.all(AnimationComponent.class, TextureRegionComponent.class).get();

    public AnimationSystem() {
        super(family);
    }

    private FloatTimer timer = new FloatTimer();

    @Override
    public void update(float deltaTime) {
        timer.addStateTime(deltaTime);
        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        AnimationComponent animationComponent = am.get(entity);
        TextureRegionComponent textureRegionComponent = tm.get(entity);

        TextureRegion frame = animationComponent.animation.getKeyFrame(timer.getStateTime());
        textureRegionComponent.region = frame;
    }
}
