package com.woed.tomato.system.graphics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.woed.tomato.component.camera.StaticZoomComponent;
import com.woed.tomato.component.camera.SynchronizingBoundsComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.map.TiledMapTileLayerComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.util.Util;

import java.util.Comparator;

public class DrawingSystem extends SortedIteratingSystem {
    private static final ComponentMapper<TextureRegionComponent> tm = ComponentMapper.getFor(TextureRegionComponent.class);
    private static final ComponentMapper<ParticleEffectComponent> pam = ComponentMapper.getFor(ParticleEffectComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<TiledMapTileLayerComponent> mm = ComponentMapper.getFor(TiledMapTileLayerComponent.class);
    private static final ComponentMapper<ScrollingLayerComponent> slm = ComponentMapper.getFor(ScrollingLayerComponent.class);
    private static final ComponentMapper<StaticZoomComponent> szm = ComponentMapper.getFor(StaticZoomComponent.class);
    private static final ComponentMapper<RotationComponent> rm = ComponentMapper.getFor(RotationComponent.class);
    private static final ComponentMapper<SynchronizingBoundsComponent> sncm = ComponentMapper.getFor(SynchronizingBoundsComponent.class);
    private static final Family family = Family.one(TextureRegionComponent.class, ParticleEffectComponent.class, TiledMapTileLayerComponent.class).get();

    private Batch batch;
    private OrthographicCamera camera;
    private OrthogonalTiledMapRenderer renderer; //no disposal needed
    private boolean zoomChanged;
    private Viewport viewport;

    // same as box2d values, ~60fps looks good.
    float PARTICLE_SYSTEM_STEP = 1 / 60f;
    float PARTICLE_SYSTEM_MAX_STEP = 0.25f;

    private boolean isBlendFunctionCleanUpNeeded = false;

    public DrawingSystem(Batch batch, Viewport viewport) {
        super(family, ZIndexComparator.INSTANCE);
        this.batch = batch;
        this.viewport = viewport;
        this.camera = (OrthographicCamera) viewport.getCamera();
        this.renderer = new OrthogonalTiledMapRenderer(null, batch);
    }

    @Override
    public void update(float deltaTime) {
        AnimatedTiledMapTile.updateAnimationBaseTime();
        viewport.apply();
        renderer.setView(camera); //also sets the projection matrix to camera.combined
        batch.begin();
        super.update(deltaTime);
        batch.end();
        checkBlendFunction();
    }

    public void processEntity(Entity entity, float deltaTime) {
        TextureRegionComponent textureRegionComponent = tm.get(entity);
        ParticleEffectComponent effectComponent = pam.get(entity);
        TiledMapTileLayerComponent mapLayerComponent = mm.get(entity);
        BoundsComponent boundsComponent = bm.get(entity);
        ScrollingLayerComponent scrollingLayerComponent = slm.get(entity);
        StaticZoomComponent zoomComponent = szm.get(entity);
        RotationComponent rotationComponent = rm.get(entity);
        SynchronizingBoundsComponent syncComponent = sncm.get(entity);

        //TODO: find a better way for this
        float oldZoom = camera.zoom;
        if (zoomComponent != null && zoomComponent.zoom != oldZoom) {
            camera.zoom = zoomComponent.zoom;
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            zoomChanged = true;
        }

        Rectangle bounds = null;
        float x = 0.0f;
        float y = 0.0f;
        float rotation = 0.0f;

        if (boundsComponent != null) {
            bounds = boundsComponent.bounds;
            x = bounds.x;
            y = bounds.y;
        }

        if (rotationComponent != null) {
            rotation = rotationComponent.rotation;
        }

        if (effectComponent != null && (!effectComponent.culling || Util.isRectangleInViewport(bounds, camera))) {
            ParticleEffect effect = effectComponent.effect != null ? effectComponent.effect : effectComponent.pooledEffect;

            if (bounds != null) {
                setParticleEmitterSpawnSize(effect, bounds.width, bounds.height);
            }
            if (rotationComponent != null) {
                for (ParticleEmitter emitter : effect.getEmitters()) {
                    float angle = rotation + 90;
                    emitter.getAngle().setHigh(angle);
                    emitter.getAngle().setLow(angle);
                }
            }

            effect.setPosition(x + effectComponent.viewportWidthOffsetFactor * camera.viewportWidth + effectComponent.offsetX,
                    y + effectComponent.viewportHeightOffsetFactor * camera.viewportHeight + effectComponent.offsetY);

//            // max frame time to avoid spiral of death (on slow devices)
//            float frameTime = Math.min(deltaTime, PARTICLE_SYSTEM_MAX_STEP);
//
//            // accumulator is per particle effect, not globally.
//            effectComponent.accumulator += frameTime;
//            while (effectComponent.accumulator >= PARTICLE_SYSTEM_STEP) {
//
//                if (effect.isComplete()) {
//                    effect.allowCompletion();
//                    //continue or reset()?
//                }
//
//                // update in step
//                effect.update(PARTICLE_SYSTEM_STEP);
//                effectComponent.accumulator -= PARTICLE_SYSTEM_STEP;
//            }

            drawEffect(effect, deltaTime);
        }


        if (textureRegionComponent != null) {
            checkBlendFunction();

            TextureRegion region = textureRegionComponent.region;
            x += textureRegionComponent.offsetX;
            y += textureRegionComponent.offsetY;

            region.flip(textureRegionComponent.flipX, textureRegionComponent.flipY);

            float width = region.getRegionWidth() * textureRegionComponent.scaleX;
            float height = region.getRegionHeight() * textureRegionComponent.scaleY;

            if (syncComponent != null && bounds != null) {
                if (syncComponent.syncWidthEnabled) {
                    width = bounds.width;
                }

                if (syncComponent.syncHeightEnabled) {
                    height = bounds.height;
                }
            }

            if (scrollingLayerComponent != null) {
                float regionWidth = textureRegionComponent.region.getRegionWidth();
                int spritesCount;

                if (regionWidth >= camera.viewportWidth * camera.zoom) {
                    spritesCount = 2;
                } else {
                    spritesCount = (int) Math.ceil(camera.viewportWidth * camera.zoom / regionWidth) + 1;
                }

                float cameraPosLeft = camera.position.x - camera.viewportWidth * camera.zoom * 0.5f;
                float cameraPosBottom = camera.position.y - camera.viewportHeight * camera.zoom * 0.5f;

                for (int i = 0; i < spritesCount; i++) {
                    x = cameraPosLeft - (cameraPosLeft * scrollingLayerComponent.velocityScale) % regionWidth + regionWidth * i;
                    if (cameraPosLeft < 0) {
                        x -= regionWidth;
                    }
                    y = cameraPosBottom - (camera.position.y - camera.viewportHeight * camera.zoom * 0.5f) * scrollingLayerComponent.velocityScale;

                    batch.draw(
                            region,
                            x, y,
                            textureRegionComponent.originX, textureRegionComponent.originY,
                            width, height,
                            1f, 1f,
                            rotation
                    );
                }
            } else {
                batch.draw(
                        region,
                        x, y,
                        textureRegionComponent.originX, textureRegionComponent.originY,
                        width, height,
                        1f, 1f,
                        rotation
                );
            }
            region.flip(textureRegionComponent.flipX, textureRegionComponent.flipY);
        }

        if (mapLayerComponent != null && mapLayerComponent.layer.isVisible()) {
            checkBlendFunction();
            renderer.setMap(mapLayerComponent.map);
            renderer.renderTileLayer(mapLayerComponent.layer);
        }

        //TODO: find a better way for this
        if (zoomChanged) {
            camera.zoom = oldZoom;
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            zoomChanged = false;
        }
    }

    private void drawEffect(ParticleEffect effect, float deltaTime) {
        for (ParticleEmitter emitter : effect.getEmitters()) {
            drawEmitter(emitter, deltaTime);
        }

    }

    private void drawEmitter(ParticleEmitter emitter, float deltaTime) {
        isBlendFunctionCleanUpNeeded = !emitter.cleansUpBlendFunction() && (emitter.isPremultipliedAlpha() || emitter.isAdditive());
        emitter.draw(batch, deltaTime);
    }

    private void checkBlendFunction() {
        if (isBlendFunctionCleanUpNeeded) {
            cleanUpBlendFunction();
        }
    }

    private void cleanUpBlendFunction() {
        isBlendFunctionCleanUpNeeded = false;
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    public static void setParticleEmitterSpawnSize(ParticleEffect effect, float width, float height) {
        for (ParticleEmitter emitter : effect.getEmitters()) {
            emitter.getSpawnWidth().setLow(width);
            emitter.getSpawnWidth().setHigh(width);
            emitter.getSpawnHeight().setLow(height);
            emitter.getSpawnHeight().setHigh(height);
        }
    }

    private static class ZIndexComparator implements Comparator<Entity> {

        private static final ZIndexComparator INSTANCE = new ZIndexComparator();

        private ZIndexComparator() {

        }

        private static final ComponentMapper<ZIndexComponent> zm = ComponentMapper.getFor(ZIndexComponent.class);
        private static final ComponentMapper<ParticleEffectComponent> pam = ComponentMapper.getFor(ParticleEffectComponent.class);

        @Override
        public int compare(Entity e1, Entity e2) {
            ZIndexComponent zIndexComponent1 = zm.get(e1);
            ZIndexComponent zIndexComponent2 = zm.get(e2);
//            ParticleEffectComponent pec1 = pam.get(e1);
//            ParticleEffectComponent pec2 = pam.get(e2);
//
//            if (pec1 != null && pec2 == null) {
//                return -1;
//            } else if (pec2 != null && pec1 == null) {
//                return 1;
//            }

            int z1 = zIndexComponent1 != null ? zIndexComponent1.z : 0;
            int z2 = zIndexComponent2 != null ? zIndexComponent2.z : 0;

            return Integer.compare(z1, z2);
        }
    }
}