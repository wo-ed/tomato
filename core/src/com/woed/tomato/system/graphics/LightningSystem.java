package com.woed.tomato.system.graphics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.woed.tomato.component.graphics.LightningComponent;

public class LightningSystem extends IteratingSystem {
    private static final ComponentMapper<LightningComponent> lm = ComponentMapper.getFor(LightningComponent.class);
    private static final Family family = Family.all(LightningComponent.class).get();

    private Batch batch;

    public LightningSystem(Batch batch) {
        super(family);
        this.batch = batch;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        int limit = lm.get(entity).limit;
        int x = MathUtils.random(limit - 1);
        if (x == limit - 1) {
            Gdx.gl.glClearColor(1, 1, 1, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            batch.setColor(Color.WHITE);
        }
    }
}
