package com.woed.tomato.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.woed.tomato.component.input.KeyboardComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class KeyboardSystem extends IteratingSystem {
    public static final float WALK_VELOCITY = 2048.0f;
    public static final float JUMP_VELOCITY = 1024.0f;

    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> cm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final Family family = Family.all(KeyboardComponent.class, VelocityComponent.class, TiledCollisionComponent.class).get();

    public KeyboardSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent velocityComponent = vm.get(entity);
        TiledCollisionComponent collisionComponent = cm.get(entity);

        float velocityX = 0.0f;
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) {
            velocityX += WALK_VELOCITY;
            velocityComponent.x = velocityX;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)) {
            velocityX -= WALK_VELOCITY;
            velocityComponent.x = velocityX;
        }
        if ((Gdx.input.isKeyPressed(Input.Keys.SPACE) || Gdx.input.isKeyPressed(Input.Keys.W))
                && collisionComponent.mapCollision.bottom) {
            velocityComponent.y = +JUMP_VELOCITY;
        }
    }
}
