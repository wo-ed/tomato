package com.woed.tomato.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.woed.tomato.component.input.ControllerComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class ControllerSystem extends IteratingSystem {
    public static final float WALK_VELOCITY = 2048.0f;
    public static final float JUMP_VELOCITY = 1024.0f;

    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> cm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final Family family = Family.all(ControllerComponent.class, VelocityComponent.class).get();

    public ControllerSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent velocity = vm.get(entity);
        TiledCollisionComponent collisionComponent = cm.get(entity);

        if (Controllers.getControllers().size > 0) {
            Controller controller = Controllers.getControllers().first();
            //degrees = -controller.getAxis(3) * 45;
            velocity.x = controller.getAxis(0) * WALK_VELOCITY;
            if (controller.getButton(2)) {
                if (collisionComponent.mapCollision.bottom) {
                    velocity.y = +JUMP_VELOCITY;
                }
            }
        }
    }
}
