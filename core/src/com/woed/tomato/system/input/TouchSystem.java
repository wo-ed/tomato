package com.woed.tomato.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.input.TouchComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.system.physics.GravitySystem;

public class TouchSystem extends IteratingSystem {
    public static final float JUMP_VELOCITY = 1024;

    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TextureRegionComponent> tm = ComponentMapper.getFor(TextureRegionComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> cm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final Family family = Family.all(TouchComponent.class, VelocityComponent.class).get();

    public TouchSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent velocity = vm.get(entity);
        TiledCollisionComponent collisionComponent = cm.get(entity);

        if (Gdx.input.isTouched()) {

            float x = Gdx.input.getX();
            float width = Gdx.graphics.getWidth();
            float center = width / 2;
            float ratio = -(x - center) / width;
            velocity.x = GravitySystem.GRAVITY_Y * ratio;

            if (collisionComponent.mapCollision.bottom) {
                velocity.y = +JUMP_VELOCITY;
            }
        }

        /*
        if(Gdx.input.isTouched()) {
            float x = Gdx.input.getX();
            float width = Gdx.graphics.getWidth();
            float halfScreen = width / 2;

            if(x < halfScreen) {
                float quarterScreen = halfScreen / 2;
                float ratio = -(x - quarterScreen) / halfScreen;
                velocity.x = GravitySystem.GRAVITY_Y * ratio;

            } else if (collisionComponent.mapCollision.bottom) {
                velocity.y = +JUMP_VELOCITY;
            }
        }
        */
    }
}
