package com.woed.tomato.system.input;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.woed.tomato.component.input.PitchComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.system.physics.GravitySystem;

public class PitchSystem extends IteratingSystem {
    public static final float JUMP_VELOCITY = 1024.0f;

    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> cm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final Family family = Family.all(PitchComponent.class, VelocityComponent.class).get();

    public PitchSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent velocity = vm.get(entity);
        TiledCollisionComponent collisionComponent = cm.get(entity);

        float degrees = Gdx.input.getPitch();
        velocity.x = GravitySystem.GRAVITY_Y * MathUtils.sinDeg(degrees);

        if (Gdx.input.isTouched() && collisionComponent.mapCollision.bottom) {
            velocity.y = +JUMP_VELOCITY;
        }
    }
}
