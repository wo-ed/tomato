package com.woed.tomato.system.trigger;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.map.HidingMapLayerComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.util.PlayerComponent;

public class HidingSystem extends IteratingSystem {
    private static final ComponentMapper<HidingMapLayerComponent> hm = ComponentMapper.getFor(HidingMapLayerComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final Family family = Family.all(HidingMapLayerComponent.class).get();

    private static final Family playerFamily = Family.all(PlayerComponent.class, BoundsComponent.class).get();
    private ImmutableArray<Entity> players;

    public HidingSystem() {
        super(family);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        players = getEngine().getEntitiesFor(playerFamily);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        HidingMapLayerComponent hidingComponent = hm.get(entity);

        for (Entity player : players) {
            Rectangle playerBounds = bm.get(player).bounds;

            for (Rectangle tileBounds : hidingComponent.rectangles) {
                if (tileBounds.overlaps(playerBounds)) {
                    hidingComponent.layer.setVisible(false);
                    return;
                }
            }
        }

        hidingComponent.layer.setVisible(true);
    }
}
