package com.woed.tomato.system.trigger;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.component.util.PlayerComponent;
import com.woed.tomato.world.EntityRemover;

public class FallingDeathSystem extends IteratingSystem {
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<PlayerComponent> pm = ComponentMapper.getFor(PlayerComponent.class);
    private static final Family family = Family.all(BoundsComponent.class).one(MortalComponent.class, PlayerComponent.class).get();

    private FallingListener fallingListener;
    private EntityRemover entityRemover;

    public FallingDeathSystem(FallingListener fallingListener, EntityRemover entityRemover) {
        super(family);
        this.fallingListener = fallingListener;
        this.entityRemover = entityRemover;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        boolean player = pm.get(entity) != null;
        Rectangle bounds = bm.get(entity).bounds;

        if (bounds.y < 0 - bounds.height) {
            if (player) {
                fallingListener.playerFallen(entity);
            } else {
                entityRemover.removeEntity(entity);
            }
        }
    }

    public interface FallingListener {
        void playerFallen(Entity player);
    }
}
