package com.woed.tomato.system.trigger;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.I18NBundle;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.CollectibleComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.FakeFinishComponent;
import com.woed.tomato.component.trigger.FinishComponent;
import com.woed.tomato.component.trigger.FreezeComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.component.trigger.TipComponent;
import com.woed.tomato.component.util.PlayerComponent;
import com.woed.tomato.system.graphics.MovementRotationSystem;
import com.woed.tomato.system.physics.MovementSystem;
import com.woed.tomato.world.EntityRemover;

public class TriggerSystem extends IteratingSystem {
    private static final ComponentMapper<CollectibleComponent> cm = ComponentMapper.getFor(CollectibleComponent.class);
    private static final ComponentMapper<FinishComponent> fm = ComponentMapper.getFor(FinishComponent.class);
    private static final ComponentMapper<FakeFinishComponent> fakm = ComponentMapper.getFor(FakeFinishComponent.class);
    private static final ComponentMapper<MortalComponent> mm = ComponentMapper.getFor(MortalComponent.class);
    private static final ComponentMapper<TipComponent> tm = ComponentMapper.getFor(TipComponent.class);
    private static final ComponentMapper<DeadlyComponent> dm = ComponentMapper.getFor(DeadlyComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<FreezeComponent> fzm = ComponentMapper.getFor(FreezeComponent.class);
    private static final Family playerFamily = Family.all(PlayerComponent.class).get();
    private static final Family family = Family.one(CollectibleComponent.class, FinishComponent.class,
            FakeFinishComponent.class, MortalComponent.class, TipComponent.class, DeadlyComponent.class)
            .all(BoundsComponent.class)
            .get();

    private ImmutableArray<Entity> players;

    private Rectangle entityBounds;
    private Rectangle playerBounds;

    private TriggerListener triggerListener;
    private EntityRemover entityRemover;

    private I18NBundle bundle;

    public TriggerSystem(TriggerListener triggerListener, EntityRemover entityRemover) {
        super(family);
        this.triggerListener = triggerListener;
        this.entityRemover = entityRemover;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        players = getEngine().getEntitiesFor(playerFamily);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        for (Entity player : players) {
            trigger(entity, player);
        }
    }

    public void trigger(Entity entity, Entity player) {
        entityBounds = bm.get(entity).bounds;
        playerBounds = bm.get(player).bounds;

        if (!playerBounds.overlaps(entityBounds)) {
            return;
        }

        TipComponent tip = tm.get(entity);
        if (tip != null) {
            String text = null;
            try {
                text = bundle.get(tip.text);
            } catch (Exception ex) {
                text = "wrong key";
            }
            triggerListener.tipHit(text);
        }

        CollectibleComponent collectible = cm.get(entity);
        if (collectible != null) {
            removeGameObject(entity);
            triggerListener.collectibleHit();
            return;
        }

        FinishComponent finish = fm.get(entity);
        if (finish != null) {
            triggerListener.finished();
            return;
        }

        MortalComponent mortal = mm.get(entity);
        if (mortal != null && triggerJumpToKill(player)) {
            VelocityComponent playerVelocity = vm.get(player);
            playerVelocity.y = 800;
            removeGameObject(entity);
            triggerListener.enemyHit();
            return;
        }

        DeadlyComponent deadly = dm.get(entity);
        if (deadly != null) {
            triggerListener.playerHit();
            return;
        }

        FakeFinishComponent fake = fakm.get(entity);
        if (fake != null && !fake.soundWasPlayed) {
            fake.soundWasPlayed = true;
            entity.remove(TextureRegionComponent.class);
            triggerListener.fakeFinished();
        }

        FreezeComponent freeze = fzm.get(entity);
        if (freeze != null && playerBounds.y <= entityBounds.y) {
            getEngine().removeSystem(getEngine().getSystem(MovementSystem.class));
            getEngine().removeSystem(getEngine().getSystem(MovementRotationSystem.class));
        }
    }

    private boolean triggerJumpToKill(Entity player) {
        VelocityComponent playerVelocity = vm.get(player);

        if (playerBounds.y > entityBounds.y && playerVelocity.y <= 0) {
            return true;
        }
        return false;
    }

    private void removeGameObject(Entity entity) {
        entityRemover.removeEntity(entity);
    }

    public I18NBundle getBundle() {
        return bundle;
    }

    public void setBundle(I18NBundle bundle) {
        this.bundle = bundle;
    }

    public interface TriggerListener {
        void tipHit(String tip);

        void collectibleHit();

        void finished();

        void fakeFinished();

        void playerHit();

        void enemyHit();
    }
}
