package com.woed.tomato.system.trigger;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.camera.OutOfViewportComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.util.Util;
import com.woed.tomato.world.EntityRemover;

public class OutOfViewportSystem extends IteratingSystem {
    private static final ComponentMapper<OutOfViewportComponent> om = ComponentMapper.getFor(OutOfViewportComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<ParticleEffectComponent> pem = ComponentMapper.getFor(ParticleEffectComponent.class);
    private static final Family family = Family.all(MortalComponent.class, OutOfViewportComponent.class, BoundsComponent.class).get();

    private static Rectangle viewportRect = new Rectangle();
    private OrthographicCamera camera;
    private EntityRemover entityRemover;

    public OutOfViewportSystem(OrthographicCamera camera, EntityRemover entityRemover) {
        super(family);
        this.camera = camera;
        this.entityRemover = entityRemover;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        OutOfViewportComponent ovc = om.get(entity);

        if (!ovc.top && !ovc.bottom && !ovc.left && !ovc.right) {
            return;
        }

        Rectangle bounds = bm.get(entity).bounds;
        viewportRect = Util.viewportToRect(camera, viewportRect);

        if (!bounds.overlaps(viewportRect)) {
            if (ovc.bottom && bounds.y < viewportRect.y - bounds.height) {
                entityRemover.removeEntity(entity);
            } else if (ovc.top && bounds.y > viewportRect.y + viewportRect.height) {
                entityRemover.removeEntity(entity);
            } else if (ovc.left && bounds.x < viewportRect.x - bounds.width) {
                entityRemover.removeEntity(entity);
            } else if (ovc.right && bounds.x > viewportRect.x + viewportRect.width) {
                entityRemover.removeEntity(entity);
            }
        }
    }
}
