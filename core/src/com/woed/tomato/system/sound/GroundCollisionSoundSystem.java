package com.woed.tomato.system.sound;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.woed.tomato.collision.MapCollision;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.sound.GroundCollisionSoundComponent;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.screen.Settings;

public class GroundCollisionSoundSystem extends IteratingSystem {

    private static final ComponentMapper<GroundCollisionSoundComponent> gm = ComponentMapper.getFor(GroundCollisionSoundComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> tc = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final Family family = Family.all(GroundCollisionSoundComponent.class, TiledCollisionComponent.class).get();

    private AssetManager assetManager;
    private boolean isSoundEnabled;

    private String lastGroundType;
    private Sound sound;

    public GroundCollisionSoundSystem(AssetManager assetManager, Settings settings) {
        super(family);
        this.assetManager = assetManager;
        this.isSoundEnabled = settings.isSoundEnabled();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        GroundCollisionSoundComponent soundComponent = gm.get(entity);
        MapCollision mapCollision = tc.get(entity).mapCollision;

        String groundType = mapCollision.groundType;

        if (groundType != null && (sound == null || !groundType.equals(lastGroundType))) {
            String soundPath = Assets.Sounds.PATH + "/" + groundType;
            sound = assetManager.get(soundPath, Sound.class);
            lastGroundType = groundType;
        }

        // play the sound the first time
        if (mapCollision.bottom && sound != null && !soundComponent.soundWasPlayed
                && isSoundEnabled) {

            sound.play();

            soundComponent.soundWasPlayed = true;
        } else if (!mapCollision.bottom) {
            soundComponent.soundWasPlayed = false;
        }
    }
}
