package com.woed.tomato.system.util;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.woed.tomato.component.util.SpawningComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.util.IntTimer;

public class SpawningSystem extends IteratingSystem {
    private static final ComponentMapper<SpawningComponent> cm = ComponentMapper.getFor(SpawningComponent.class);
    private static final Family family = Family.all(SpawningComponent.class).get();

    private IntTimer counter = new IntTimer();

    private EntityCreator entityCreator;

    public SpawningSystem(EntityCreator entityCreator) {
        super(family);
        this.entityCreator = entityCreator;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        counter.addStateTime(1);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        SpawningComponent spawningComponent = cm.get(entity);

        if (counter.getStateTime() % spawningComponent.frames == 0) {
            entityCreator.addSingleGameObject(spawningComponent.mapObject);
        }
    }
}
