package com.woed.tomato.system.ai;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.woed.tomato.component.ai.FlyingAttackingNpcComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.util.PlayerComponent;
import com.woed.tomato.util.MathFunctions;

public class FlyingAttackingNpcSystem extends IteratingSystem {
    private static final ComponentMapper<FlyingAttackingNpcComponent> fm = ComponentMapper.getFor(FlyingAttackingNpcComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<ParticleEffectComponent> pm = ComponentMapper.getFor(ParticleEffectComponent.class);

    private static final Family family = Family.all(FlyingAttackingNpcComponent.class, VelocityComponent.class).get();
    private static final Family playerFamily = Family.all(PlayerComponent.class, BoundsComponent.class).get();

    private ImmutableArray<Entity> players;
    private static Vector2 v1 = new Vector2();
    private static Vector2 v2 = new Vector2();

    private OrthographicCamera camera;

    public FlyingAttackingNpcSystem(OrthographicCamera camera) {
        super(family);
        this.camera = camera;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        players = engine.getEntitiesFor(playerFamily);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        FlyingAttackingNpcComponent ai = fm.get(entity);
        VelocityComponent velocity = vm.get(entity);

        if (players.size() == 0) {
            return;
        }

        //TODO: instead for first: nearest
        Entity target = players.first();

        Rectangle bounds = bm.get(entity).bounds;
        Rectangle targetBounds = bm.get(target).bounds;

        ParticleEffect effect = pm.get(entity).pooledEffect;


        float angle = MathFunctions.getAngle(targetBounds.getPosition(v1), bounds.getPosition(v2));
        ParticleEmitter emitter = effect.getEmitters().get(0);
        emitter.getAngle().setHigh(angle - 45, angle + 45);
        emitter.getAngle().setLow(angle);

        //TODO: dx & dy > 0

        float dx = targetBounds.x - bounds.x;
        float dy = targetBounds.y - bounds.y;
        float vx = Math.min(ai.maxV, ai.maxV * Math.abs(dy / dx));
        float vy = Math.min(ai.maxV, ai.maxV * Math.abs(dx / dy));
        velocity.x = dx / Math.abs(dx) * vy;
        velocity.y = dy / Math.abs(dy) * vx;

        //make the entity follow the camera
        bounds.x = Math.max(bounds.x, camera.position.x - camera.viewportWidth / 2 * camera.zoom - bounds.width);
    }
}
