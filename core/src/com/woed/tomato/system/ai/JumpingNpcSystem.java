package com.woed.tomato.system.ai;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.woed.tomato.component.ai.JumpingNpcComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class JumpingNpcSystem extends IteratingSystem {

    private static final ComponentMapper<JumpingNpcComponent> jm = ComponentMapper.getFor(JumpingNpcComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> cm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);

    private static final Family family = Family.all(JumpingNpcComponent.class, TiledCollisionComponent.class, VelocityComponent.class).get();

    private boolean random = false;

    public JumpingNpcSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {

        TiledCollisionComponent collisionComponent = cm.get(entity);
        VelocityComponent velocity = vm.get(entity);
        JumpingNpcComponent ai = jm.get(entity);

        if (collisionComponent.mapCollision.bottom) {
            if (random && !collisionComponent.mapCollision.left &&
                    !collisionComponent.mapCollision.right) {
                velocity.x = Math.abs(velocity.x);
                if (MathUtils.randomBoolean()) {
                    velocity.x = -velocity.x;
                }
            }
            velocity.y = ai.jumpVelocity;
        }
    }
}
