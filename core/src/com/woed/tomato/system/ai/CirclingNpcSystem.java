package com.woed.tomato.system.ai;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.woed.tomato.component.ai.CirclingNpcComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class CirclingNpcSystem extends IteratingSystem {
    private final static ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private final static Family family = Family.all(CirclingNpcComponent.class, VelocityComponent.class).get();

    boolean down = true;
    boolean left = false;
    private float factorX = 1;
    private float factorY = 0;
    private float vMax = 300;
    private float incPerFrame = 0.01f;

    public CirclingNpcSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent velocity = vm.get(entity);

        if (left) {
            if (factorX <= -1) {
                left = false;
                factorX += incPerFrame;
            } else {
                factorX -= incPerFrame;
            }
        } else {
            if (factorX >= 1) {
                left = true;
                factorX -= incPerFrame;
            } else {
                factorX += incPerFrame;
            }
        }

        if (down) {
            if (factorY <= -1) {
                down = false;
                factorY += incPerFrame;
            } else {
                factorY -= incPerFrame;
            }
        } else {
            if (factorY >= 1) {
                down = true;
                factorY -= incPerFrame;
            } else {
                factorY += incPerFrame;
            }
        }

        velocity.x = vMax * factorX;
        velocity.y = vMax * factorY;
    }
}
