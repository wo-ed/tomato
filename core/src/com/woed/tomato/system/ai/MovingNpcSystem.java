package com.woed.tomato.system.ai;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.ai.MovingNpcComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class MovingNpcSystem extends IteratingSystem {

    private static final ComponentMapper<MovingNpcComponent> wm = ComponentMapper.getFor(MovingNpcComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final ComponentMapper<TiledCollisionComponent> tcm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final ComponentMapper<TextureRegionComponent> trm = ComponentMapper.getFor(TextureRegionComponent.class);

    private static final Family family = Family.all(MovingNpcComponent.class, BoundsComponent.class,
            VelocityComponent.class, TiledCollisionComponent.class,
            TextureRegionComponent.class).get();

    public MovingNpcSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        MovingNpcComponent movement = wm.get(entity);
        VelocityComponent velocity = vm.get(entity);
        TiledCollisionComponent collisionComponent = tcm.get(entity);

        TextureRegionComponent textureRegionComponent = trm.get(entity);
        Rectangle bounds = bm.get(entity).bounds;

        boolean directionRight = velocity.x > 0;

        if (movement.movementZoneWidth > 0) {
            float leftBorder = movement.initialX;
            float rightBorder = movement.initialX + movement.movementZoneWidth;
            float travelDistance = velocity.x * deltaTime;

            if (directionRight && bounds.x + bounds.width + travelDistance > rightBorder) {
                directionRight = false;
            } else if (!directionRight && bounds.x + travelDistance < leftBorder) {
                directionRight = true;
            }
        }

        if (collisionComponent.mapCollision.left) {
            directionRight = true;
        } else if (collisionComponent.mapCollision.right) {
            directionRight = false;
        }

        if (directionRight) {
            velocity.x = Math.abs(movement.velocityX);
        } else {
            velocity.x = -Math.abs(movement.velocityX);
        }

        if (movement.flipping) {
            textureRegionComponent.flipX = !directionRight;
        }
    }
}
