package com.woed.tomato.system.physics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class TiledCollisionSystem extends IteratingSystem {

    private TiledMapTileLayer layer;
    private float tileWidth;
    private float tileHeight;

    private static final ComponentMapper<TiledCollisionComponent> tm = ComponentMapper.getFor(TiledCollisionComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final Family family = Family.all(TiledCollisionComponent.class, BoundsComponent.class, VelocityComponent.class).get();

    private static Rectangle tileRect = new Rectangle();
    private static Rectangle oldBounds = new Rectangle();
    private static Rectangle tmpBounds = new Rectangle();
    private static Rectangle pathRectX = new Rectangle();
    private static Rectangle pathRectY = new Rectangle();
    private static Vector2 newPos = new Vector2();

    public TiledCollisionSystem(TiledMap tiledMap) {
        super(family);
        layer = (TiledMapTileLayer) tiledMap.getLayers().get("collision");
        tileWidth = layer.getTileWidth();
        tileHeight = layer.getTileHeight();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TiledCollisionComponent collisionComponent = tm.get(entity);
        VelocityComponent velocity = vm.get(entity);

        collisionComponent.mapCollision.reset();

        Rectangle bounds = bm.get(entity).bounds;
        oldBounds.set(collisionComponent.oldX, collisionComponent.oldY, bounds.width, bounds.height);
        tmpBounds.set(0, 0, oldBounds.width, oldBounds.height);

        newPos.set(oldBounds.x, oldBounds.y);

        int d = MathUtils.ceil(Math.max(bounds.width, bounds.height) / Math.max(tileWidth, tileHeight));
        int startX = (int) (bounds.x / tileWidth) - d;
        int endX = (int) (bounds.x / tileWidth) + d;
        int startY = (int) (bounds.y / tileHeight) - d;
        int endY = (int) (bounds.y / tileHeight) + d;

        if (velocity.x != 0) {
            tmpBounds.setX(bounds.x);
            tmpBounds.setY(oldBounds.y);

            for (int y = startY; y <= endY; y++) {
                for (int x = startX; x <= endX; x++) {

                    TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                    if (cell != null) {
                        tileRect.set(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
                        correctHorizontalPosition(tmpBounds, velocity, collisionComponent);
                    }
                }
            }

            newPos.x = tmpBounds.x;
        }

        if (velocity.y != 0) {
            tmpBounds.setX(newPos.x);
            tmpBounds.setY(bounds.y);

            for (int y = startY; y <= endY; y++) {
                for (int x = startX; x <= endX; x++) {

                    TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                    if (cell != null) {
                        tileRect.set(x * tileWidth, y * tileHeight, tileWidth, tileHeight);
                        correctVerticalPosition(tmpBounds, velocity, collisionComponent, cell);
                    }
                }
            }

            newPos.y = tmpBounds.y;
        }

        bounds.x = newPos.x;
        bounds.y = newPos.y;
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;

        // stops the velocity from increasing while object ist grounded
        // if used before collision correction: object doesn't fall from a platform but floats in the air
        if ((collisionComponent.mapCollision.bottom && velocity.y < 0)
                || (collisionComponent.mapCollision.top && velocity.y > 0)) {
            velocity.y = 0.0f;
        }
    }

    private void correctHorizontalPosition(
            Rectangle bounds,
            VelocityComponent velocity,
            TiledCollisionComponent collisionComponent) {

        if (bounds.overlaps(tileRect)) {

            boolean left = bounds.x < collisionComponent.oldX;
            boolean right = bounds.x > collisionComponent.oldX;

            if (left) {
                bounds.x = tileRect.x + tileWidth;
                collisionComponent.mapCollision.left = true;
            } else if (right) {
                bounds.x = tileRect.x - bounds.width;
                collisionComponent.mapCollision.right = true;
            }
        }
    }

    private void correctVerticalPosition(
            Rectangle bounds,
            VelocityComponent velocity,
            TiledCollisionComponent collisionComponent,
            TiledMapTileLayer.Cell cell) {

        if (bounds.overlaps(tileRect)) {

            boolean down = bounds.y < collisionComponent.oldY;
            boolean up = bounds.y > collisionComponent.oldY;

            if (down) {
                bounds.y = tileRect.y + tileHeight;
                collisionComponent.mapCollision.bottom = true;
                collisionComponent.mapCollision.groundType = cell.getTile().getProperties().get("groundType", null, String.class);
                velocity.y = 0.0f;
            } else if (up) {
                bounds.y = tileRect.y - bounds.height;
                collisionComponent.mapCollision.top = true;
            }
        }
    }
}
