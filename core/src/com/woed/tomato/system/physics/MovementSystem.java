package com.woed.tomato.system.physics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class MovementSystem extends IteratingSystem {
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final Family family = Family.all(BoundsComponent.class, VelocityComponent.class).get();

    public MovementSystem() {
        super(family);
    }

    public void processEntity(Entity entity, float deltaTime) {
        Rectangle bounds = bm.get(entity).bounds;
        VelocityComponent velocity = vm.get(entity);

        bounds.x += velocity.x * deltaTime;
        bounds.y += velocity.y * deltaTime;
    }
}