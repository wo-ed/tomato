package com.woed.tomato.system.physics;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.VelocityComponent;

public class GravitySystem extends IteratingSystem {
    public static final float GRAVITY_X = 0;
    public static final float GRAVITY_Y = -9.81f * 256;

    private static final ComponentMapper<GravityComponent> gm = ComponentMapper.getFor(GravityComponent.class);
    private static final ComponentMapper<VelocityComponent> vm = ComponentMapper.getFor(VelocityComponent.class);
    private static final Family family = Family.all(GravityComponent.class, VelocityComponent.class).get();

    public GravitySystem() {
        super(family);
    }

    public void processEntity(Entity entity, float deltaTime) {
        GravityComponent gravityComponent = gm.get(entity);
        VelocityComponent velocity = vm.get(entity);

        velocity.x += GRAVITY_X * gravityComponent.gravityScaleX * deltaTime;
        velocity.y += GRAVITY_Y * gravityComponent.gravityScaleY * deltaTime;
    }
}