package com.woed.tomato.system.camera;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.camera.CameraComponent;
import com.woed.tomato.component.physics.BoundsComponent;

public class FollowingCameraSystem extends IteratingSystem {
    private static final ComponentMapper<CameraComponent> cm = ComponentMapper.getFor(CameraComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final Family family = Family.all(CameraComponent.class).get();

    private float cameraDistanceY = 0;
    private float cameraDistanceX = 0;
    private boolean isVertical;
    private float velocityScale = 2;

    public FollowingCameraSystem() {
        super(family);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        CameraComponent cameraComponent = cm.get(entity);

        Entity target = cameraComponent.target;
        Rectangle targetBounds = bm.get(target).bounds;
        OrthographicCamera camera = cameraComponent.camera;

        cameraDistanceY = (targetBounds.y - camera.position.y) * camera.zoom;
        //cameraDistanceX = getGameObject().getX() - camera.position.x;

        if (cameraDistanceY != 0) {

            if (Math.abs(cameraDistanceY) < 0.01f) {
                cameraDistanceY = 0;
            } else {
                float amount = cameraDistanceY * deltaTime * velocityScale;
                cameraDistanceY -= amount;
                camera.position.y += amount;
            }
            if (camera.position.y < camera.viewportHeight * camera.zoom * 0.5f) {
                camera.position.y = camera.viewportHeight * camera.zoom * 0.5f;
            }
        }

        /*
            if (cameraDistanceX != 0) {
                if (Math.abs(cameraDistanceX) < 0.01f) {
                    cameraDistanceX = 0;
                } else {
                    float amount = cameraDistanceX * deltaTime * 20;
                    cameraDistanceX -= amount;
                    camera.position.x += amount;
                }
            }
*/
        camera.position.x = targetBounds.x;
        camera.update();
    }
}
