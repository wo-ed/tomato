package com.woed.tomato.system.camera;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.camera.StaticZoomComponent;
import com.woed.tomato.component.camera.SynchronizingBoundsComponent;
import com.woed.tomato.component.physics.BoundsComponent;

public class SynchronizingBoundsSystem extends IteratingSystem {
    private static final ComponentMapper<SynchronizingBoundsComponent> sm = ComponentMapper.getFor(SynchronizingBoundsComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final ComponentMapper<StaticZoomComponent> szm = ComponentMapper.getFor(StaticZoomComponent.class);
    private static final Family family = Family.all(SynchronizingBoundsComponent.class, BoundsComponent.class).get();

    private OrthographicCamera camera;

    public SynchronizingBoundsSystem(OrthographicCamera camera) {
        super(family);
        this.camera = camera;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        SynchronizingBoundsComponent syncComponent = sm.get(entity);
        Rectangle bounds = bm.get(entity).bounds;
        StaticZoomComponent staticZoom = szm.get(entity);

        if (syncComponent.syncWidthEnabled) {
            bounds.width = camera.viewportWidth * syncComponent.syncWidthFactor;
            if (staticZoom != null) {
                bounds.width *= staticZoom.zoom;
            } else {
                bounds.width *= camera.zoom;
            }
        }

        if (syncComponent.syncHeightEnabled) {
            bounds.height = camera.viewportHeight * syncComponent.syncHeightFactor;
            if (staticZoom != null) {
                bounds.height *= staticZoom.zoom;
            } else {
                bounds.height *= camera.zoom;
            }
        }
    }
}
