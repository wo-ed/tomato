package com.woed.tomato.system.camera;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.camera.ZoomingAreaComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.util.PlayerComponent;

public class AutoZoomingCameraSystem extends IteratingSystem {
    private static final ComponentMapper<ZoomingAreaComponent> zm = ComponentMapper.getFor(ZoomingAreaComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);
    private static final Family playerFamily = Family.all(PlayerComponent.class, BoundsComponent.class).get();
    private static final Family family = Family.all(ZoomingAreaComponent.class).get();
    private OrthographicCamera camera;
    private Rectangle playerRect;
    private ZoomingAreaComponent lastZoomingArea;
    private float defaultZoom = 1.0f;
    private float defaultRate = 0.1f;
    private boolean overlapFound = false;
    private ImmutableArray<Entity> players;

    public AutoZoomingCameraSystem(OrthographicCamera camera) {
        super(family);
        this.camera = camera;
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        players = getEngine().getEntitiesFor(playerFamily);
    }

    @Override
    public void update(float deltaTime) {
        overlapFound = false;
        playerRect = bm.get(players.first()).bounds;
        super.update(deltaTime);
        if (!overlapFound) {
            if (lastZoomingArea != null) {
                adjustZoom(defaultZoom, defaultRate);
            } else {
                camera.zoom = defaultZoom;
            }
        }
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        ZoomingAreaComponent zoomingArea = zm.get(entity);

        if (playerRect.overlaps(zoomingArea.rect)) {
            overlapFound = true;
            adjustZoom(zoomingArea.zoom, zoomingArea.rate);
            lastZoomingArea = zoomingArea;
        }
    }

    private void adjustZoom(float neededZoom, float rate) {
        if (camera.zoom < neededZoom) {
            camera.zoom = Math.min(camera.zoom + rate, neededZoom);
        } else if (camera.zoom > neededZoom) {
            camera.zoom = Math.max(camera.zoom - rate, neededZoom);
        }
    }
}
