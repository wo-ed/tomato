package com.woed.tomato.system.camera;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.camera.AttachableComponent;
import com.woed.tomato.component.physics.BoundsComponent;

public class AttachmentSystem extends IteratingSystem {
    private static final ComponentMapper<AttachableComponent> am = ComponentMapper.getFor(AttachableComponent.class);
    private static final ComponentMapper<BoundsComponent> bm = ComponentMapper.getFor(BoundsComponent.class);

    private static final Family family = Family.all(AttachableComponent.class, BoundsComponent.class).get();

    private Camera camera;

    public AttachmentSystem(Camera camera) {
        super(family);
        this.camera = camera;
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        AttachableComponent attachableComponent = am.get(entity);
        Rectangle bounds = bm.get(entity).bounds;

        if (attachableComponent.attachXEnabled) {
            bounds.x = attachableComponent.viewportWidthOffsetFactor * camera.viewportWidth +
                    attachableComponent.x + camera.position.x - camera.viewportWidth / 2;
        }

        if (attachableComponent.attachYEnabled) {
            bounds.y = attachableComponent.viewportHeightOffsetFactor * camera.viewportHeight +
                    attachableComponent.y + camera.position.y - camera.viewportHeight / 2;
        }
    }
}
