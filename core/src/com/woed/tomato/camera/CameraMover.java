package com.woed.tomato.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;

public class CameraMover {

    private Camera camera;

    private Vector2 velocity = new Vector2();
    private Vector2 idleVelocity = new Vector2();

    private Vector2 maxVelocity;
    private Vector2 minVelocity;

    private Vector2 maxPosition;
    private Vector2 minPosition;

    private boolean bounce = true;

    public CameraMover(Camera camera) {
        this.camera = camera;
    }

    private void checkVelocity(float vx, float vy) {
        if (maxVelocity != null && (maxVelocity.x < vx || maxVelocity.y < vy)) {
            throw new IllegalArgumentException();
        }
        if (minVelocity != null && (minVelocity.x > vx || minVelocity.y > vy)) {
            throw new IllegalArgumentException();
        }
    }

    public float getX() {
        return camera.position.x;
    }

    public float getY() {
        return camera.position.y;
    }

    public void setX(float x) {
        camera.position.x = clampX(x);
    }

    public void setY(float y) {
        camera.position.y = clampY(y);
    }

    private float clampX(float x) {
        if (maxPosition != null && x > maxPosition.x) {
            x = maxPosition.x;
        }
        if (minPosition != null && x < minPosition.x) {
            x = minPosition.x;
        }
        return x;
    }

    private float clampY(float y) {
        if (maxPosition != null && y > maxPosition.y) {
            y = maxPosition.y;
        }
        if (minPosition != null && y < minPosition.y) {
            y = minPosition.y;
        }
        return y;
    }

    public void move(float delta) {
        setX(camera.position.x + velocity.x * delta);
        setY(camera.position.y + velocity.y * delta);
        camera.update();
    }

    public void idle() {
        velocity.set(idleVelocity);
    }

    public void stop() {
        setVelocity(Vector2.Zero);
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        setVelocity(velocity.x, velocity.y);
    }

    public void setVelocity(float vx, float vy) {
        checkVelocity(vx, vy);
        this.velocity.set(vx, vy);
    }

    public Vector2 getIdleVelocity() {
        return idleVelocity;
    }

    public void setIdleVelocity(Vector2 idleVelocity) {
        checkVelocity(idleVelocity.x, idleVelocity.y);
        this.idleVelocity.set(idleVelocity);
    }

    public Vector2 getMaxVelocity() {
        return maxVelocity;
    }

    public void setMaxVelocity(Vector2 maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    public Vector2 getMinVelocity() {
        return minVelocity;
    }

    public void setMinVelocity(Vector2 minVelocity) {
        this.minVelocity = minVelocity;
    }

    public Vector2 getMaxPosition() {
        return maxPosition;
    }

    public void setMaxPosition(Vector2 maxPosition) {
        this.maxPosition = maxPosition;
    }

    public Vector2 getMinPosition() {
        return minPosition;
    }

    public void setMinPosition(Vector2 minPosition) {
        this.minPosition = minPosition;
    }
}
