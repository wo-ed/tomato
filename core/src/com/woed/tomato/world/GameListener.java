package com.woed.tomato.world;

public interface GameListener {
    void gameOver();

    void gameFinished();

    void gamePausedChanged(boolean paused);
}
