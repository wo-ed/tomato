package com.woed.tomato.world;

import com.badlogic.ashley.core.Entity;

public interface EntityRemover {
    void removeEntity(Entity entity);
}
