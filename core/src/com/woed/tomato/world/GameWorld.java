package com.woed.tomato.world;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.util.StaticComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.level.TiledLevel;
import com.woed.tomato.main.TomatoGame;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.screen.Settings;
import com.woed.tomato.system.ai.CirclingNpcSystem;
import com.woed.tomato.system.ai.FlyingAttackingNpcSystem;
import com.woed.tomato.system.ai.JumpingNpcSystem;
import com.woed.tomato.system.ai.MovingNpcSystem;
import com.woed.tomato.system.camera.AttachmentSystem;
import com.woed.tomato.system.camera.AutoZoomingCameraSystem;
import com.woed.tomato.system.camera.FollowingCameraSystem;
import com.woed.tomato.system.camera.SynchronizingBoundsSystem;
import com.woed.tomato.system.graphics.AnimationSystem;
import com.woed.tomato.system.graphics.DrawingSystem;
import com.woed.tomato.system.graphics.LightningSystem;
import com.woed.tomato.system.graphics.MovementRotationSystem;
import com.woed.tomato.system.input.ControllerSystem;
import com.woed.tomato.system.input.KeyboardSystem;
import com.woed.tomato.system.input.PitchSystem;
import com.woed.tomato.system.input.TouchSystem;
import com.woed.tomato.system.physics.GravitySystem;
import com.woed.tomato.system.physics.MovementSystem;
import com.woed.tomato.system.physics.TiledCollisionSystem;
import com.woed.tomato.system.sound.GroundCollisionSoundSystem;
import com.woed.tomato.system.trigger.FallingDeathSystem;
import com.woed.tomato.system.trigger.HidingSystem;
import com.woed.tomato.system.trigger.OutOfViewportSystem;
import com.woed.tomato.system.trigger.TriggerSystem;
import com.woed.tomato.system.util.SpawningSystem;

public class GameWorld implements TriggerSystem.TriggerListener, FallingDeathSystem.FallingListener, EntityRemover {
    private static final Family nonStaticFamily = Family.exclude(StaticComponent.class).get();

    private static final PitchSystem pitchSystem = new PitchSystem();
    private static final ControllerSystem controllerSystem = new ControllerSystem();
    private static final KeyboardSystem keyboardSystem = new KeyboardSystem();
    private static final TouchSystem touchSystem = new TouchSystem();
    private static final GravitySystem gravitySystem = new GravitySystem();
    private static final MovementSystem movementSystem = new MovementSystem();
    private static final FollowingCameraSystem followingCameraSystem = new FollowingCameraSystem();
    private static final CirclingNpcSystem circlingNpcSystem = new CirclingNpcSystem();
    private static final JumpingNpcSystem jumpingNpcSystem = new JumpingNpcSystem();
    private static final MovingNpcSystem movingNpcSystem = new MovingNpcSystem();
    private static final HidingSystem hidingSystem = new HidingSystem();
    private static final MovementRotationSystem movementRotationSystem = new MovementRotationSystem();
    private static final AnimationSystem animationSystem = new AnimationSystem();

    private Batch batch;
    private TiledLevel level;
    private TiledMap map;

    private EntityCreator entityCreator;

    private TriggerSystem triggerSystem;

    private OrthographicCamera camera = new OrthographicCamera();
    private PooledEngine engine = new PooledEngine();
    private SpawningSystem spawningSystem;
    private Viewport viewport = new ExtendViewport(TomatoGame.VIEWPORT_WIDTH, TomatoGame.VIEWPORT_HEIGHT, camera);

    private boolean gameOver = false;
    private boolean paused = false;
    private boolean finished = false;

    private boolean staticEntitiesAdded = false;

    private int deathsCount = -1;
    private String tip = null;

    private GameListener gameListener;

    private AssetManager assetManager;
    private Settings settings;

    private ParticleEffect bloodEffect;
    private ParticleEffect swampEffect;
    private Sound laughter;

    private boolean isSoundEnabled;

    private static final ComponentMapper<ParticleEffectComponent> pem = ComponentMapper.getFor(ParticleEffectComponent.class);

    private ImmutableArray<Entity> nonStaticEntities = engine.getEntitiesFor(nonStaticFamily);

    public GameWorld(Batch batch, TiledLevel level, AssetManager assetManager, Settings settings) {
        this.batch = batch;
        this.level = level;
        this.map = level.getMap();
        this.assetManager = assetManager;
        this.settings = settings;
        this.isSoundEnabled = settings.isSoundEnabled();
        this.laughter = assetManager.get(Assets.Sounds.LAUGHTER_OGG);

        entityCreator = new EntityCreator(assetManager, engine);

        addSystems();
        setSystemsPaused(false);
    }

    private void addSystems() {
        /* Input */
        engine.addSystem(pitchSystem);
        engine.addSystem(controllerSystem);
        engine.addSystem(keyboardSystem);
//        engine.addSystem(touchSystem);

        /* Physics */
        engine.addSystem(gravitySystem);
        engine.addSystem(movementSystem);
        engine.addSystem(new TiledCollisionSystem(map));

        /* Camera */
        engine.addSystem(new AutoZoomingCameraSystem(camera));
        engine.addSystem(followingCameraSystem);
        engine.addSystem(new AttachmentSystem(camera));
        engine.addSystem(new SynchronizingBoundsSystem(camera));

        /* Ai */
        engine.addSystem(circlingNpcSystem);
        engine.addSystem(new FlyingAttackingNpcSystem(camera));
        engine.addSystem(jumpingNpcSystem);
        engine.addSystem(movingNpcSystem);

        /* Trigger */
        triggerSystem = new TriggerSystem(this, this);
        triggerSystem.setBundle(assetManager.get(
                Assets.Properties.BUNDLE_PROPERTIES.replace(".properties", ""), I18NBundle.class));
        engine.addSystem(triggerSystem);
        engine.addSystem(new FallingDeathSystem(this, this));
        engine.addSystem(new OutOfViewportSystem(camera, this));
        engine.addSystem(hidingSystem);

        /* Graphics */
        engine.addSystem(movementRotationSystem);
        engine.addSystem(animationSystem);
        engine.addSystem(new LightningSystem(batch));
        engine.addSystem(new DrawingSystem(batch, viewport));

        /* Sound */
        engine.addSystem(new GroundCollisionSoundSystem(assetManager, settings));

        /* Utils */
        spawningSystem = new SpawningSystem(entityCreator);
        engine.addSystem(spawningSystem);
    }

    public void render(float deltaTime) {
        tip = null;

        engine.update(deltaTime);
        if (gameOver) {
            gameOver = false;
            startLevel();
        }
    }

    public void startLevel() {
        deathsCount++;
        camera.zoom = 1.0f;
        removeNonStaticEntities();
        if (!staticEntitiesAdded) {
            addStaticEntities();
            staticEntitiesAdded = true;
        }
        addNonStaticEntities();
        entityCreator.freeAllPooledEffects();
    }

    private void addNonStaticEntities() {
        entityCreator.addGameObjects(map);
        entityCreator.addCameraEntity(camera);
    }

    private void removeNonStaticEntities() {
        while (nonStaticEntities.size() > 0) {
            removeEntity(nonStaticEntities.first());
        }
    }

    private void addStaticEntities() {
        entityCreator.addSurroundingEntities(level);
        entityCreator.addMapLayerEntities(map);
        entityCreator.addZoomingAreaEntities(map);

        Entity bloodEntity = entityCreator.addBloodParticleEntity();
        bloodEffect = bloodEntity.getComponent(ParticleEffectComponent.class).effect;

        Entity swampEntity = entityCreator.addSwampParticleEntity();
        swampEffect = swampEntity.getComponent(ParticleEffectComponent.class).effect;
    }

    public boolean isPaused() {
        return paused;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public void finished() {
        this.finished = true;
        gameListener.gameFinished();
        setSystemsPaused(this.finished);
    }

    @Override
    public void fakeFinished() {
        if (isSoundEnabled) {
            laughter.play();
        }
    }

    public void setPaused(boolean paused) {
        setMusicPaused(paused);
        if (!isFinished()) {
            this.paused = paused;
            gameListener.gamePausedChanged(paused);
            setSystemsPaused(paused);
        }
    }

    private void setSystemsPaused(boolean paused) {
        gravitySystem.setProcessing(!paused);
        movementSystem.setProcessing(!paused);
        movementRotationSystem.setProcessing(!paused);
        spawningSystem.setProcessing(!paused);
        triggerSystem.setProcessing(!paused);
    }

    private void setMusicPaused(boolean paused) {
        if (level.getMusic() != null) {
            Music music = level.getMusic().getAsset();
            if (!paused && !music.isPlaying() && isSoundEnabled) {
                music.play();
            } else if (paused && music.isPlaying()) {
                music.pause();
            }
        }
    }

    public void loadDependencies() {
        entityCreator.loadDependencies(level);
    }

    public void setDeathsCount(int deathsCount) {
        this.deathsCount = deathsCount;
    }

    public int getDeathsCount() {
        return deathsCount;
    }

    public boolean isTipVisible() {
        return tip != null;
    }

    public String getTip() {
        return tip;
    }

    public GameListener getGameListener() {
        return this.gameListener;
    }

    public void setGameListener(GameListener listener) {
        this.gameListener = listener;
    }

    public void gameOver() {
        gameOver = true;
        gameListener.gameOver();
        if (Gdx.input.isPeripheralAvailable(Input.Peripheral.Vibrator)
                && settings.isVibrationEnabled()) {
            Gdx.input.vibrate(32);
        }
    }

    public PooledEngine getEngine() {
        return engine;
    }

    @Override
    public void removeEntity(Entity entity) {
        ParticleEffectComponent pec = pem.get(entity);
        if (pec != null) {
            entityCreator.freePooledEffect(pec.pooledEffect);
        }
        getEngine().removeEntity(entity);
    }

    @Override
    public void playerFallen(Entity player) {
        gameOver();
        if (level.getFallingSound() != null && level.getFallingSound().getAsset() != null
                && isSoundEnabled) {
            level.getFallingSound().getAsset().play();
        }
        swampEffect.start();
    }

    @Override
    public void tipHit(String tip) {
        this.tip = tip;
    }

    @Override
    public void collectibleHit() {
        if (isSoundEnabled) {
            level.getGameObjectHitSound().play();
        }
    }

    @Override
    public void playerHit() {
        gameOver();
        bloodEffect.start();
    }

    @Override
    public void enemyHit() {
        if (isSoundEnabled) {
            level.getGameObjectHitSound().play();
        }
    }

    public void updateViewport(int viewportWidth, int viewportHeight) {
        viewport.update(viewportWidth, viewportHeight);
    }
}
