package com.woed.tomato.resource;

public final class Assets {
    public static final class SpriteSheets {
        public static final String PATH = "spriteSheets";
        public static final String TEXTURES_ATLAS = "spriteSheets/textures.atlas";
        public static final String TEXTURES_PNG = "spriteSheets/textures.png";
    }
    public static final class Levels {
        public static final String PATH = "levels";
        public static final String LEAVES128_TSX = "levels/leaves128.tsx";
        public static final String MOOR128_ATLAS = "levels/moor128.atlas";
        public static final String MOOR128_TSX = "levels/moor128.tsx";
        public static final String NOVEMBER_TMX = "levels/november.tmx";
        public static final String MOORSNOW128_TSX = "levels/moorsnow128.tsx";
        public static final String LEAVES128_ATLAS = "levels/leaves128.atlas";
        public static final String FALLING_SNOW_TMX = "levels/falling_snow.tmx";
        public static final String BIG_GUYS_TMX = "levels/big_guys.tmx";
        public static final String DARKWOOD_TMX = "levels/darkwood.tmx";
        public static final String FINAL_STAGE_TMX = "levels/final_stage.tmx";
        public static final String BOREDOM_TO_DEATH_TMX = "levels/boredom_to_death.tmx";
        public static final String SNOW128_PNG = "levels/snow128.png";
        public static final String MOORSNOW128_ATLAS = "levels/moorsnow128.atlas";
        public static final String MOOR128_PNG = "levels/moor128.png";
        public static final String MOORSNOW128_PNG = "levels/moorsnow128.png";
        public static final String ICE_CAVE_TMX = "levels/ice_cave.tmx";
        public static final String SNOW128_ATLAS = "levels/snow128.atlas";
        public static final String SILENT_SWAMP_TMX = "levels/silent_swamp.tmx";
        public static final String LEAVES128_PNG = "levels/leaves128.png";
        public static final String THE_ENTRANCE_TMX = "levels/the_entrance.tmx";
        public static final String THE_HILL_TMX = "levels/the_hill.tmx";
        public static final String CURSED_GRAVEYARD_TMX = "levels/cursed_graveyard.tmx";
        public static final String LEVEL_ONE_TMX = "levels/level_one.tmx";
        public static final String SUNSET_TMX = "levels/sunset.tmx";
        public static final String SNOW128_TSX = "levels/snow128.tsx";
    }
    public static final class ParticleEffects {
        public static final String PATH = "particleEffects";
        public static final String AURORA_P = "particleEffects/aurora.p";
        public static final String CLOUDS_P = "particleEffects/clouds.p";
        public static final String FOG2_P = "particleEffects/fog2.p";
        public static final String GHOST_P = "particleEffects/ghost.p";
        public static final String ANGRY_SKULL_P = "particleEffects/angry_skull.p";
        public static final String GHOST_DARK_P = "particleEffects/ghost_dark.p";
        public static final String BAT_P = "particleEffects/bat.p";
        public static final String SNOW_P = "particleEffects/snow.p";
        public static final String RAIN_P = "particleEffects/rain.p";
        public static final String SKULL_P = "particleEffects/skull.p";
        public static final String FOG_P = "particleEffects/fog.p";
        public static final String LANTERN_P = "particleEffects/lantern.p";
        public static final String DEAD_LEAVES_2_P = "particleEffects/dead_leaves_2.p";
        public static final String LEAVES_P = "particleEffects/leaves.p";
        public static final String MAGICAL_BUTTON_P = "particleEffects/magical_button.p";
        public static final String DEAD_LEAVES_P = "particleEffects/dead_leaves.p";
        public static final String STARS_P = "particleEffects/stars.p";
        public static final String DEAD_BLOOD_P = "particleEffects/dead_blood.p";
        public static final String DEAD_SWAMP_P = "particleEffects/dead_swamp.p";
    }
    public static final class Textures {
        public static final String PATH = "textures";
        public static final String MUSHROOM1_PNG = "textures/mushroom1.png";
        public static final String MARSHLAND0_PNG = "textures/marshland0.png";
        public static final String TOMBSTONE2_PNG = "textures/tombstone2.png";
        public static final String GRASS_PNG = "textures/grass.png";
        public static final String MUSHROOM2_PNG = "textures/mushroom2.png";
        public static final String SNOW_LAYER1_PNG = "textures/snow_layer1.png";
        public static final String FACE_EYES_CLOSED_PNG = "textures/face_eyes_closed.png";
        public static final String SWAMP_PNG = "textures/swamp.png";
        public static final String TOMBSTONE1_PNG = "textures/tombstone1.png";
        public static final String TREES_PNG = "textures/trees.png";
        public static final String GROUND_LEAVES_PNG = "textures/ground_leaves.png";
        public static final String GROUND_MUD_PNG = "textures/ground_mud.png";
        public static final String SNOW_LAYER0_PNG = "textures/snow_layer0.png";
        public static final String GROUND_GRAVEYARD_PNG = "textures/ground_graveyard.png";
        public static final String GROUND_SKULLS_PNG = "textures/ground_skulls.png";
        public static final String TOMBSTONE3_PNG = "textures/tombstone3.png";
        public static final String LANTERN1_PNG = "textures/lantern1.png";
        public static final String LANTERN0_PNG = "textures/lantern0.png";
        public static final String FACE_EYES_OPEN_PNG = "textures/face_eyes_open.png";
        public static final String BULBRUSH1_PNG = "textures/bulbrush1.png";
    }
    public static final class Sounds {
        public static final String PATH = "sounds";
        public static final String HORROR_OGG = "sounds/horror.ogg";
        public static final String SPLASH_OGG = "sounds/splash.ogg";
        public static final String SNOW_MUSIC2_OGG = "sounds/snow_music2.ogg";
        public static final String SNOW_OGG = "sounds/snow.ogg";
        public static final String WIND_OGG = "sounds/wind.ogg";
        public static final String LAUGHTER_OGG = "sounds/laughter.ogg";
        public static final String BIRDS_MUSIC_OGG = "sounds/birds_music.ogg";
        public static final String RAIN_OGG = "sounds/rain.ogg";
        public static final String MUD_OGG = "sounds/mud.ogg";
        public static final String FOREST_MUSIC_OGG = "sounds/forest_music.ogg";
        public static final String SNOW_MUSIC_OGG = "sounds/snow_music.ogg";
        public static final String LEAVES_OGG = "sounds/leaves.ogg";
        public static final String BITE_OGG = "sounds/bite.ogg";
        public static final String SWAMP_MUSIC_OGG = "sounds/swamp_music.ogg";
        public static final String CRICKETS_OGG = "sounds/crickets.ogg";
    }
    public static final class Properties {
        public static final String PATH = "properties";
        public static final String BUNDLE_DE_PROPERTIES = "properties/bundle_de.properties";
        public static final String BUNDLE_PROPERTIES = "properties/bundle.properties";
    }
    public static final class Ui {
        public static final String PATH = "ui";
        public static final String UISKIN_JSON = "ui/uiskin.json";
        public static final String GREEN_DOWN_PNG = "ui/green_down.png";
        public static final String DEFAULT_PNG = "ui/default.png";
        public static final String MVBOLI_TTF = "ui/mvboli.ttf";
        public static final String GREEN_UP_PNG = "ui/green_up.png";
        public static final String PARTICLE_PNG = "ui/particle.png";
        public static final String DEFAULT_FNT = "ui/default.fnt";
        public static final String UISKIN_PNG = "ui/uiskin.png";
        public static final String MTCORSVA_TTF = "ui/MTCORSVA.TTF";
        public static final String UISKIN_ATLAS = "ui/uiskin.atlas";
    }
}