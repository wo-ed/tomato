package com.woed.tomato.resource;

public final class AtlasKeysTest {
    public static final class Textures {
        public static final String ACTORS_BAT_FLYING_FLYING = "actors/bat/flying/flying";
        public static final String ACTORS_SNOWMAN_STAND_STAND_ = "actors/snowman/stand/stand_";
        public static final String ACTORS_LANTERN_DEFAULT_DEFAULT0 = "actors/lantern/default/default0";
        public static final String ACTORS_LANTERN_DEFAULT_DEFAULT1 = "actors/lantern/default/default1";
        public static final String ACTORS_MONSTER_WALK_WALK0 = "actors/monster/walk/walk0";
        public static final String ACTORS_MONSTER_WALK_WALK1 = "actors/monster/walk/walk1";
        public static final String ACTORS_MOON_FULLMOON_FULLMOON1 = "actors/moon/fullmoon/fullmoon1";
        public static final String ACTORS_MOON_FULLMOON_FULLMOON2 = "actors/moon/fullmoon/fullmoon2";
        public static final String ACTORS_PUMPKIN_DEFAULT_DEFAULT0 = "actors/pumpkin/default/default0";
        public static final String ACTORS_PUMPKIN_DEFAULT_DEFAULT1 = "actors/pumpkin/default/default1";
        public static final String ACTORS_SCARECROW_DEFAULT_DEFAULT = "actors/scarecrow/default/default";
        public static final String ACTORS_SKULL_DEFAULT_DEFAULT0 = "actors/skull/default/default0";
        public static final String PARTICLES_SKULL = "particles/skull";
        public static final String ACTORS_SKULL_DEFAULT_DEFAULT1 = "actors/skull/default/default1";
        public static final String ACTORS_SNOWBALL_DEFAULT_DEFAULT0 = "actors/snowball/default/default0";
        public static final String ACTORS_TOMATO_DEFAULT_DEFAULT0 = "actors/tomato/default/default0";
        public static final String ACTORS_TOMATO_DEFAULT_DEFAULT1 = "actors/tomato/default/default1";
        public static final String ACTORS_TOMATO_DEFAULT_DEFAULT2 = "actors/tomato/default/default2";
        public static final String ACTORS_TREE_WINTER_WINTER0 = "actors/tree/winter/winter0";
        public static final String PARTICLES_AURORA = "particles/aurora";
        public static final String PARTICLES_CLOUD = "particles/cloud";
        public static final String PARTICLES_FOG256 = "particles/fog256";
        public static final String PARTICLES_FOG2S1024 = "particles/fog2s1024";
        public static final String PARTICLES_FOG2S256 = "particles/fog2s256";
        public static final String PARTICLES_FOG2S512 = "particles/fog2s512";
        public static final String PARTICLES_LEAF = "particles/leaf";
        public static final String PARTICLES_LIGHT = "particles/light";
        public static final String PARTICLES_PARTICLE = "particles/particle";
        public static final String PARTICLES_PARTICLE2 = "particles/particle2";
        public static final String PARTICLES_PARTICLE2_L = "particles/particle2_L";
        public static final String PARTICLES_PARTICLE_L = "particles/particle_L";
        public static final String PARTICLES_PRE_PARTICLE = "particles/pre_particle";
        public static final String PARTICLES_PRE_PARTICLE_L = "particles/pre_particle_L";
        public static final String PARTICLES_PRE_PARTICLE_S = "particles/pre_particle_s";
        public static final String PARTICLES_RAINDROP = "particles/raindrop";
        public static final String PARTICLES_SNOW = "particles/snow";
        public static final String PARTICLES_STAR = "particles/star";
    }
}