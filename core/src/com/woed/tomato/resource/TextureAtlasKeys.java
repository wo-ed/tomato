package com.woed.tomato.resource;

public class TextureAtlasKeys {
    public static final String ENTITY = "actors/";
    public static final String PARTICLES = "particles/";
}
