package com.woed.tomato.action;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Action;

public class FadeAction extends Action {

    public static final float DEFAULT_TIME_STEP = 0.2f;
    public static final float DEFAULT_START_VALUE = 0;
    public static final float DEFAULT_END_VALUE = 1;

    private boolean firstTimeAct = true;
    protected float timeStep;
    protected float startValue;
    protected float endValue;
    protected boolean fadeIn;
    private float initialValue;

    public FadeAction() {
        this(DEFAULT_TIME_STEP);
    }

    public FadeAction(float timeStep) {
        this(timeStep, DEFAULT_START_VALUE, DEFAULT_END_VALUE, DEFAULT_START_VALUE);
    }

    public FadeAction(float timeStep, float startValue, float endValue, float initialValue) {
        this.timeStep = timeStep;
        this.startValue = startValue;
        this.endValue = endValue;
        fadeIn = startValue < endValue;
        timeStep = Math.abs(timeStep);
        this.initialValue = initialValue;
    }

    @Override
    public boolean act(float delta) {
        Color color = actor.getColor();
        float newAlpha;
        boolean done = false;

        if (firstTimeAct) {
            firstTimeAct = false;
            newAlpha = initialValue;
        } else {
            if (fadeIn) {
                newAlpha = color.a + timeStep;
            } else {
                newAlpha = color.a - timeStep;
            }
            if ((fadeIn && newAlpha >= endValue) || (!fadeIn && newAlpha <= endValue)) {
                newAlpha = endValue;
                done = true;
            }
        }

        actor.setColor(color.r, color.g, color.b, newAlpha);
        return done;
    }
}
