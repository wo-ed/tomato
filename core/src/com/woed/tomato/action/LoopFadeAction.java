package com.woed.tomato.action;

public class LoopFadeAction extends FadeAction {

    private boolean isStopped;
    private int timeLag = 1;
    private int counter = 0;

    public LoopFadeAction() {
        super();
    }

    public LoopFadeAction(float timeStep) {
        super(timeStep);
    }

    public LoopFadeAction(float timeStep, float startValue, float endValue, float initialValue) {
        super(timeStep, startValue, endValue, initialValue);
    }

    @Override
    public boolean act(float delta) {
        boolean done = super.act(delta);
        if (done && counter % timeLag == 0) {
            float oldStartValue = startValue;
            startValue = endValue;
            endValue = oldStartValue;
            fadeIn = startValue < endValue;
        }
        counter++;
        return isStopped();
    }

    public boolean isStopped() {
        return isStopped;
    }

    public void setStopped(boolean isStopped) {
        this.isStopped = isStopped;
    }

    public int getTimeLag() {
        return timeLag;
    }

    public void setTimeLag(int timeLag) {
        this.timeLag = timeLag;
    }
}
