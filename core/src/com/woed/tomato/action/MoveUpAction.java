package com.woed.tomato.action;

import com.badlogic.gdx.scenes.scene2d.Action;

public class MoveUpAction extends Action {

    private float amountY;
    private float maxY;

    @Override
    public boolean act(float delta) {
        if (target.getY() < maxY) {
            target.setY(target.getY() + amountY);
            return false;
        }
        return true;
    }

    public float getAmountY() {
        return amountY;
    }

    public void setAmountY(float amountY) {
        this.amountY = amountY;
    }

    public float getMaxY() {
        return maxY;
    }

    public void setMaxY(float maxY) {
        this.maxY = maxY;
    }
}
