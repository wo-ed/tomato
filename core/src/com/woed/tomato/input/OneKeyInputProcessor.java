package com.woed.tomato.input;

import com.badlogic.gdx.InputProcessor;

public class OneKeyInputProcessor implements InputProcessor {

    private enum KeyState {
        UP,
        DOWN
    }

    private int keycode;
    private KeyState keyState = KeyState.UP;
    private boolean isKeyPressed = false;

    public OneKeyInputProcessor(int keycode) {
        this.keycode = keycode;
    }

    public boolean isKeyPressed() {
        boolean result = isKeyPressed;
        isKeyPressed = false;
        return result;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == this.keycode) {
            if (keyState != KeyState.DOWN) {
                keyState = KeyState.DOWN;
                isKeyPressed = true;
            }
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == this.keycode) {
            keyState = KeyState.UP;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
