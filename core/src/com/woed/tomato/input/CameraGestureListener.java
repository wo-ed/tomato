package com.woed.tomato.input;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.woed.tomato.camera.CameraMover;

public class CameraGestureListener implements GestureDetector.GestureListener {

    private OrthographicCamera orthographicCamera;
    private CameraMover cameraMover;
    private float initialScale = 1;

    public CameraGestureListener(
            CameraMover cameraMover) {
        this.cameraMover = cameraMover;
        if (cameraMover.getCamera() instanceof OrthographicCamera) {
            this.orthographicCamera = (OrthographicCamera) cameraMover.getCamera();
        }
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        if (orthographicCamera != null) {
            velocityX *= orthographicCamera.zoom;
            velocityY *= orthographicCamera.zoom;
        }
        cameraMover.setVelocity(-velocityX, velocityY);
        return false;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        cameraMover.stop();
        if (orthographicCamera != null) {
            initialScale = orthographicCamera.zoom;
        }
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        if (orthographicCamera != null) {
            float ratio = initialDistance / distance;
            float oldZoom = orthographicCamera.zoom;
            orthographicCamera.zoom = MathUtils.clamp(initialScale * ratio, 0.5f, 2.0f);
            float factor = orthographicCamera.zoom / oldZoom;
            cameraMover.getMinPosition().x *= factor;
            cameraMover.getMinPosition().y *= factor;

            //TODO
            cameraMover.getMaxPosition().x /= factor;
            cameraMover.getMaxPosition().y /= factor;

            orthographicCamera.update();
        }
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (orthographicCamera != null) {
            deltaX *= orthographicCamera.zoom;
            deltaY *= orthographicCamera.zoom;
        }
        cameraMover.stop();
        cameraMover.setX(cameraMover.getX() - deltaX);
        cameraMover.setY(cameraMover.getY() + deltaY);
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public void pinchStop() {

    }
}
