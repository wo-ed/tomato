package com.woed.tomato.graphics;

import com.badlogic.gdx.utils.Pool;

public class AnimationPool extends Pool<AnimationPool.PooledAnimation> {

    public AnimationPool(int initialCapacity, int max) {
        super(initialCapacity, max);
    }

    protected PooledAnimation newObject() {
        return new PooledAnimation();
    }

    public PooledAnimation obtain() {
        PooledAnimation effect = super.obtain();
        effect.reset();
        return effect;
    }

    public class PooledAnimation extends PoolableAnimation {
        PooledAnimation() {
            super();
        }

        @Override
        public void reset() {
            super.reset();
        }

        public void free() {
            AnimationPool.this.free(this);
        }
    }
}

