package com.woed.tomato.graphics;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.woed.tomato.util.FloatTimer;

public class Animator implements Pool.Poolable {

    private ObjectMap<String, PoolableAnimation> animations = new ObjectMap<>();
    private PoolableAnimation activeAnimation = null;
    private String animationState = "";
    private FloatTimer timer = new FloatTimer();

    public TextureRegion getFrame() {
        return getKeyFrame(getStateTime());
    }

    public TextureRegion getKeyFrame(float stateTime) {
        return getActiveAnimation().getKeyFrame(stateTime);
    }

    public float getStateTime() {
        return timer.getStateTime();
    }

    public void setStateTime(float stateTime) {
        timer.setStateTime(stateTime);
    }

    public void addStateTime(float stateTime) {
        timer.addStateTime(stateTime);
    }

    public PoolableAnimation getAnimation(String animationState) {
        return animations.get(animationState);
    }

    public PoolableAnimation addAnimation(String animationState, PoolableAnimation animation) {
        return animations.put(animationState, animation);
    }

    public PoolableAnimation removeAnimation(String animationState) {
        return animations.remove(animationState);
    }

    public PoolableAnimation getActiveAnimation() {
        return activeAnimation;
    }

    public String getAnimationState() {
        return animationState;
    }

    public void setAnimationState(String animationState) {
        PoolableAnimation animation = animations.get(animationState, null);
        if (animation == null) {
            throw new RuntimeException(String.format("Animation %s not found.", animationState));
        }
        this.activeAnimation = animation;
        this.animationState = animationState;
    }

    public Array<String> getAnimationStates() {
        return animations.keys().toArray();
    }

    @Override
    public void reset() {
        animations.clear();
        activeAnimation = null;
        animationState = "";
        timer.reset();
    }
}
