package com.woed.tomato.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.Disposable;

public class GradientLayer implements Disposable {

    private ShapeRenderer renderer = new ShapeRenderer();
    private float x;
    private float y;
    private float width;
    private float height;
    private Color colorLeftBottom;
    private Color colorRightBottom;
    private Color colorRightTop;

    private Color colorLeftTop;

    public GradientLayer(Color color) {
        this(color, color, color, color);
    }

    public GradientLayer(Color colorLeftBottom, Color colorRightBottom, Color colorRightTop, Color colorLeftTop) {
        this(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),
                colorLeftBottom, colorRightBottom, colorRightTop, colorLeftTop);
    }

    public GradientLayer(float x, float y, float width, float height,
                         Color colorLeftBottom, Color colorRightBottom, Color colorRightTop, Color colorLeftTop) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.colorLeftBottom = colorLeftBottom;
        this.colorRightBottom = colorRightBottom;
        this.colorRightTop = colorRightTop;
        this.colorLeftTop = colorLeftTop;
        renderer.setAutoShapeType(true);
    }

    public void render() {
        renderer.begin(ShapeType.Filled);
        renderer.rect(x, y, width, height, colorLeftBottom, colorRightBottom, colorRightTop, colorLeftTop);
        renderer.end();
    }

    @Override
    public void dispose() {
        renderer.dispose();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Color getColorLeftBottom() {
        return colorLeftBottom;
    }

    public void setColorLeftBottom(Color colorLeftBottom) {
        this.colorLeftBottom = colorLeftBottom;
    }

    public Color getColorRightBottom() {
        return colorRightBottom;
    }

    public void setColorRightBottom(Color colorRightBottom) {
        this.colorRightBottom = colorRightBottom;
    }

    public Color getColorRightTop() {
        return colorRightTop;
    }

    public void setColorRightTop(Color colorRightTop) {
        this.colorRightTop = colorRightTop;
    }

    public Color getColorLeftTop() {
        return colorLeftTop;
    }

    public void setColorLeftTop(Color colorLeftTop) {
        this.colorLeftTop = colorLeftTop;
    }
}
