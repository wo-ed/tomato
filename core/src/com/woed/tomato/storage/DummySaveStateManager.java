package com.woed.tomato.storage;

public class DummySaveStateManager implements SaveStateManager {
    @Override
    public void setLevelCompleted(int levelId) {

    }

    @Override
    public void setLevelCollectiblesFound(int levelId) {

    }

    @Override
    public boolean isLevelCompleted(int levelId) {
        return true;
    }

    @Override
    public boolean areLevelCollectiblesFound(int levelId) {
        return true;
    }
}
