package com.woed.tomato.storage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class PreferencesSaveStateManager implements SaveStateManager {
    private Preferences prefs = Gdx.app.getPreferences("SaveState");

    private static final String LEVEL_COMPLETED = "%s-completed";
    private static final String COLLECTIBLES_FOUND = "%s-collectiblesFound";

    @Override
    public void setLevelCompleted(int levelId) {
        prefs.putBoolean(String.format(LEVEL_COMPLETED, levelId), true);
        prefs.flush();
    }

    @Override
    public void setLevelCollectiblesFound(int levelId) {
        prefs.putBoolean(String.format(COLLECTIBLES_FOUND, levelId), true);
        prefs.flush();
    }

    @Override
    public boolean isLevelCompleted(int levelId) {
        return prefs.getBoolean(String.format(LEVEL_COMPLETED, levelId), false);
    }

    @Override
    public boolean areLevelCollectiblesFound(int levelId) {
        return prefs.getBoolean(String.format(COLLECTIBLES_FOUND, levelId), false);
    }
}
