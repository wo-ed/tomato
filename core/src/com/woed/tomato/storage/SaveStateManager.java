package com.woed.tomato.storage;

public interface SaveStateManager {
    void setLevelCompleted(int levelId);

    void setLevelCollectiblesFound(int levelId);

    boolean isLevelCompleted(int levelId);

    boolean areLevelCollectiblesFound(int levelId);
}
