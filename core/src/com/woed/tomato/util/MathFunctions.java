package com.woed.tomato.util;

import com.badlogic.gdx.math.Vector2;

public class MathFunctions {

    public static float getAngle(Vector2 v1, Vector2 v2) {
        float angle = (float) Math.toDegrees(Math.atan2(v2.y - v1.y, v2.x - v1.x));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }
}
