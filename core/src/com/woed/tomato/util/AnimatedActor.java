package com.woed.tomato.util;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class AnimatedActor extends Actor {
    private Animation<TextureRegion> animation;
    private FloatTimer counter = new FloatTimer();
    private TextureRegion frame = null;
    private boolean stretching = false;

    public AnimatedActor(Animation<TextureRegion> animation) {
        this.animation = animation;
        updateFrame();
        setSize(frame.getRegionWidth(), frame.getRegionHeight());
    }

    public AnimatedActor(float duration, TextureRegion... keyFrames) {
        this(new Animation<>(duration, keyFrames));
    }

    public AnimatedActor(float duration, Array<? extends TextureRegion> keyFrames) {
        this(new Animation<>(duration, keyFrames));
    }

    public AnimatedActor(float duration, Array<? extends TextureRegion> keyFrames, Animation.PlayMode playMode) {
        this(new Animation<>(duration, keyFrames, playMode));
    }

    public Animation<TextureRegion> getAnimation() {
        return animation;
    }

    public void setAnimation(Animation<TextureRegion> animation) {
        this.animation = animation;
    }

    private void updateFrame() {
        frame = animation.getKeyFrame(counter.getStateTime());
    }

    @Override
    public void act(float deltaTime) {
        super.act(deltaTime);
        counter.addStateTime(deltaTime);
        updateFrame();
    }

    @Override
    public void draw(Batch batch, float alpha) {
        super.draw(batch, alpha);
        float width = stretching ? getWidth() : frame.getRegionWidth();
        float height = stretching ? getHeight() : frame.getRegionHeight();

        batch.draw(frame,
                getX(), getY(),
                getOriginX(), getOriginY(),
                width, height,
                getScaleX(), getScaleY(),
                getRotation());
    }

    public boolean isStretching() {
        return stretching;
    }

    public void setStretching(boolean stretching) {
        this.stretching = stretching;
    }
}
