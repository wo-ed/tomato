package com.woed.tomato.util;

public class DummyAdsController implements com.woed.tomato.util.AdsController {

    @Override
    public void showBannerAd() {

    }

    @Override
    public void hideBannerAd() {

    }

    @Override
    public boolean isWifiConnected() {
        return false;
    }

    @Override
    public void showInterstitialAd(Runnable then) {

    }

    @Override
    public boolean areAdsEnabled() {
        return false;
    }
}
