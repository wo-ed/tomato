package com.woed.tomato.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Disposable;
import com.woed.tomato.resource.Assets;

public class MagicalTextButton extends TextButton implements Disposable {

    private ParticleEffect effect;
    private ParticleEmitter emitter;

    public boolean isSyncEnabled() {
        return syncEnabled;
    }

    public void setSyncEnabled(boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
    }

    private boolean syncEnabled = true;

    public MagicalTextButton(String text, Skin skin) {
        super(text, skin);
    }

    public MagicalTextButton(String text, Skin skin, String styleName) {
        super(text, skin, styleName);
    }

    public MagicalTextButton(String text, TextButtonStyle style) {
        super(text, style);
    }

    public void syncEmitter() {
        emitter.getSpawnWidth().setLow(getWidth());
        emitter.getSpawnWidth().setHigh(getWidth());
        emitter.getSpawnHeight().setLow(getHeight());
        emitter.getSpawnHeight().setHigh(getHeight());
        emitter.setPosition(getX() + getWidth() * 0.5f, getY() + getHeight() * 0.5f);
    }

    public void initialize(AssetManager assetManager) {
        effect = new ParticleEffect(assetManager.get(Assets.ParticleEffects.MAGICAL_BUTTON_P, ParticleEffect.class));
        emitter = effect.getEmitters().first();
        if (syncEnabled) {
            syncEmitter();
        }
        effect.start();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (syncEnabled) {
            syncEmitter();
        }
        effect.update(Gdx.graphics.getDeltaTime());
        effect.draw(batch);
        super.draw(batch, alpha);
    }

    @Override
    public void dispose() {
        effect.dispose();
    }
}
