package com.woed.tomato.util;

/**
 * Created by Wolfgang on 05.05.2016.
 */
public interface OsManager {
    AdsController getAdsController();

    boolean isScreenAlwaysOn();

    void setScreenAlwaysOn(boolean screenAlwaysOn);
}
