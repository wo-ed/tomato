package com.woed.tomato.util;

import com.badlogic.gdx.utils.Pool;

public class FloatTimer implements Pool.Poolable {

    private float stateTime = 0.0f;

    public float getStateTime() {
        return stateTime;
    }

    public void setStateTime(float stateTime) {
        this.stateTime = stateTime;
    }

    public void addStateTime(float deltaTime) {
        stateTime += deltaTime;
    }

    @Override
    public void reset() {
        stateTime = 0.0f;
    }
}
