package com.woed.tomato.util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;

public class AssetWrapper<T> implements Disposable {

    protected AssetManager assetManager;
    protected String path;
    protected T asset;
    protected Class<T> type;

    public AssetWrapper(String path, AssetManager assetManager, Class<T> type) {
        this.path = path;
        this.assetManager = assetManager;
        this.type = type;
    }

    public void loadAsset() {
        assetManager.load(path, type);
    }

    public void unloadAsset() {
        assetManager.unload(path);
    }

    public T getAsset() {
        if (asset == null) {
            asset = assetManager.get(path);
        }
        return asset;
    }

    public String getPath() {
        return path;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void dispose() {
        unloadAsset();
    }
}
