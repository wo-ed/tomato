package com.woed.tomato.util;

/**
 * Created by Wolfgang on 17.08.2015.
 */
public interface AdsController {
    void showBannerAd();

    void hideBannerAd();

    boolean isWifiConnected();

    void showInterstitialAd(Runnable then);

    boolean areAdsEnabled();
}
