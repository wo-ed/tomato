package com.woed.tomato.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;

public class Util {

    public static Color colorFromInt(int r, int g, int b, int a) {
        float f = 255;
        return new Color(Color.rgba8888(r / f, g / f, b / f, a / f));
    }

    private static Rectangle tmp = new Rectangle(); //use only in isRectangleInViewport(float, float, float, float, OrthographicCamera)

    public static boolean isRectangleInViewport(float x, float y, float width, float height, OrthographicCamera camera) {
        tmp.set(x, y, width, height);
        return isRectangleInViewport(tmp, camera);
    }

    private static Rectangle viewportRect = new Rectangle(); //use only in isRectangleInViewport(Rectangle, OrthographicCamera)

    public static boolean isRectangleInViewport(Rectangle r, OrthographicCamera camera) {
        viewportRect = viewportToRect(camera, viewportRect);
        return viewportRect.overlaps(r);
    }

    public static Rectangle viewportToRect(OrthographicCamera camera, Rectangle r) {
        float cameraX = camera.position.x - camera.viewportWidth / 2 * camera.zoom;
        float cameraY = camera.position.y - camera.viewportHeight / 2 * camera.zoom;
        r.set(cameraX, cameraY, camera.viewportWidth * camera.zoom, camera.viewportHeight * camera.zoom);
        return r;
    }
}
