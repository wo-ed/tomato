package com.woed.tomato.util;

import com.badlogic.gdx.utils.Pool;

public class IntTimer implements Pool.Poolable {

    private int stateTime = 0;

    public int getStateTime() {
        return stateTime;
    }

    public void setStateTime(int stateTime) {
        this.stateTime = stateTime;
    }

    public void addStateTime(int deltaTime) {
        stateTime += deltaTime;
    }

    @Override
    public void reset() {
        stateTime = 0;
    }
}
