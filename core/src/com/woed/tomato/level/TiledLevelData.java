package com.woed.tomato.level;

public class TiledLevelData {
    private int id;
    private String name;
    private String mapPath;

    public TiledLevelData() {

    }

    public TiledLevelData(String mapPath, String name, int id) {
        this.mapPath = mapPath;
        this.name = name;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMapPath() {
        return mapPath;
    }

    public void setMapPath(String mapPath) {
        this.mapPath = mapPath;
    }
}
