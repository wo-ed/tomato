package com.woed.tomato.level;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.utils.Array;
import com.woed.tomato.util.AssetWrapper;

public class TiledLevel {
    private TiledLevelData data;
    private TiledMap map;
    private Color bgColorTop;
    private Color bgColorBottom;
    private Color batchColor;
    private Array<String> foreground = new Array<>();
    private Array<String> background = new Array<>();
    private AssetWrapper<Music> music = null;
    private AssetWrapper<Sound> fallingSound = null;
    private Sound gameObjectHitSound = null;
    private ParticleEffect gameOverEffect;

    public TiledLevelData getData() {
        return data;
    }

    public void setData(TiledLevelData data) {
        this.data = data;
    }

    public TiledMap getMap() {
        return map;
    }

    public void setMap(TiledMap map) {
        this.map = map;
    }

    public Array<String> getForeground() {
        return foreground;
    }

    public Array<String> getBackground() {
        return background;
    }

    public AssetWrapper<Music> getMusic() {
        return music;
    }

    public void setMusic(AssetWrapper<Music> music) {
        this.music = music;
    }

    public AssetWrapper<Sound> getFallingSound() {
        return fallingSound;
    }

    public void setFallingSound(AssetWrapper<Sound> fallingSound) {
        this.fallingSound = fallingSound;
    }

    public Sound getGameObjectHitSound() {
        return gameObjectHitSound;
    }

    public void setGameObjectHitSound(Sound gameObjectHitSound) {
        this.gameObjectHitSound = gameObjectHitSound;
    }

    public Color getBgColorBottom() {
        return bgColorBottom;
    }

    public void setBgColorBottom(Color bgColorBottom) {
        this.bgColorBottom = bgColorBottom;
    }

    public Color getBgColorTop() {
        return bgColorTop;
    }

    public void setBgColorTop(Color bgColorTop) {
        this.bgColorTop = bgColorTop;
    }

    public Color getBatchColor() {
        return batchColor;
    }

    public void setBatchColor(Color batchColor) {
        this.batchColor = batchColor;
    }
}
