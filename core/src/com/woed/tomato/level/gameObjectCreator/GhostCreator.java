package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.ai.FlyingAttackingNpcComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class GhostCreator extends BaseGameObjectCreator {
    public static final int COLLISION_WIDTH = 64;
    public static final int COLLISION_HEIGHT = 64;
    public static final int OFFSET_X = 32;
    public static final int OFFSET_Y = 0;

    public GhostCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));
        entity.add(engine.createComponent(FlyingAttackingNpcComponent.class));

        entity.add(createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT));

        boolean dark = Boolean.parseBoolean(obj.getProperties().get("dark", "false", String.class));
        String effectPath = dark ? Assets.ParticleEffects.GHOST_DARK_P : Assets.ParticleEffects.GHOST_P;

        ParticleEffectComponent pc = entityCreator.createPooledParticleEffectComponent(effectPath, 1, 2);
        pc.offsetX = OFFSET_X;
        pc.offsetY = OFFSET_Y;
        entity.add(pc);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.GHOST_P, ParticleEffect.class, particleEffectParameter);
        assetManager.load(Assets.ParticleEffects.GHOST_DARK_P, ParticleEffect.class, particleEffectParameter);
    }
}
