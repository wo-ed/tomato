package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.camera.OutOfViewportComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class BatCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "bat/";

    public static final String FLYING = NAME + "flying/";

    public static final int OFFSET_X = -24;
    public static final int OFFSET_Y = -48;
    public static final int COLLISION_WIDTH = 48;
    public static final int COLLISION_HEIGHT = 28;

    public BatCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(MortalComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));

        entity.add(createAnimationComponent(BatCreator.FLYING, 1.0f / 16.0f, PoolableAnimation.PlayMode.LOOP));
        // entity.add(entityCreator.createPooledParticleEffectComponent(Assets.ParticleEffects.BAT_P, 1, 1000));
        entity.add(createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT));

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.offsetX = OFFSET_X;
        textureRegionComponent.offsetY = OFFSET_Y;
        entity.add(textureRegionComponent);

        VelocityComponent velocity = engine.createComponent(VelocityComponent.class);
        velocity.x = defaultVelocityX;
        entity.add(velocity);

        OutOfViewportComponent ovc = engine.createComponent(OutOfViewportComponent.class);
        ovc.left = true;
        entity.add(ovc);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        // assetManager.load(Assets.ParticleEffects.BAT_P, ParticleEffect.class, particleEffectParameter);
    }
}
