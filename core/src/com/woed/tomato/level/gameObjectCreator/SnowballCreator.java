package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.RotationSyncComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class SnowballCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "snowball/";

    public static final String DEFAULT = NAME + "default/";
    public static final String DEFAULT_0 = DEFAULT + "default0";

    public static final int OFFSET_X = 0;
    public static final int OFFSET_Y = 0;
    public static final int COLLISION_WIDTH = 128;
    public static final int COLLISION_HEIGHT = 128;
    public static final int ORIGIN_X = 64;
    public static final int ORIGIN_Y = 64;

    public SnowballCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(GravityComponent.class));
        entity.add(engine.createComponent(MortalComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.region = atlas.findRegion(SnowballCreator.DEFAULT_0);
        entity.add(textureRegionComponent);

        ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
        index.z = 2996;
        entity.add(index);

        BoundsComponent boundsComponent = createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;
        entity.add(boundsComponent);

        TiledCollisionComponent collisionComponent = engine.createComponent(TiledCollisionComponent.class);
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;
        entity.add(collisionComponent);

        bounds.setSize(SnowballCreator.COLLISION_WIDTH, SnowballCreator.COLLISION_HEIGHT);

        textureRegionComponent.offsetX = SnowballCreator.OFFSET_X;
        textureRegionComponent.offsetY = SnowballCreator.OFFSET_Y;
        textureRegionComponent.originX = SnowballCreator.ORIGIN_X;
        textureRegionComponent.originY = SnowballCreator.ORIGIN_Y;

        entity.add(createMovingNpcComponent(obj, bounds.x, bounds.y));

        entity.add(engine.createComponent(RotationComponent.class));
        RotationSyncComponent rotationSyncComponent = engine.createComponent(RotationSyncComponent.class);
        rotationSyncComponent.rotationScale = 0.67f;
        entity.add(rotationSyncComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}