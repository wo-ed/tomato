package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.trigger.FreezeComponent;
import com.woed.tomato.level.EntityCreator;

public class FreezeTipCreator extends ScarecrowCreator {
    public FreezeTipCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    protected Entity create(MapObject obj) {
        Entity entity = super.create(obj);
        entity.add(engine.createComponent(FreezeComponent.class));
        return entity;
    }
}
