package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.woed.tomato.component.ai.MovingNpcComponent;
import com.woed.tomato.component.graphics.AnimationComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.resource.TextureAtlasKeys;

public abstract class BaseGameObjectCreator {
    public static final float defaultVelocityX = -512;
    public static final float defaultVelocityY = 0.0f;
    public static final float jumpVelocity = 1024;

    protected EntityCreator entityCreator;
    protected PooledEngine engine;
    protected TextureAtlas atlas;
    protected AssetManager assetManager;

    private Array<TextureRegion> tempTextureRegionArray = new Array<>(TextureRegion.class);

    protected ParticleEffectLoader.ParticleEffectParameter particleEffectParameter
            = new ParticleEffectLoader.ParticleEffectParameter();

    public BaseGameObjectCreator(EntityCreator entityCreator) {
        this.entityCreator = entityCreator;
        this.engine = entityCreator.getEngine();
        this.atlas = entityCreator.getAtlas();
        this.assetManager = entityCreator.getAssetManager();

        particleEffectParameter.atlasFile = Assets.SpriteSheets.TEXTURES_ATLAS;
        particleEffectParameter.atlasPrefix = TextureAtlasKeys.PARTICLES;
    }

    protected BoundsComponent createBoundsComponent(MapObject obj, float width, float height) {
        BoundsComponent boundsComponent = engine.createComponent(BoundsComponent.class);
        Rectangle bounds = boundsComponent.bounds;
        bounds.x = obj.getProperties().get("x", Float.class);
        bounds.y = obj.getProperties().get("y", Float.class);
        bounds.width = width;
        bounds.height = height;
        return boundsComponent;
    }

    protected MovingNpcComponent createMovingNpcComponent(MapObject obj, float initialX, float initialY) {
        float movementZoneWidth = 0.0f;
        float movementZoneHeight = 0.0f;
        if (obj instanceof RectangleMapObject) {
            movementZoneWidth = obj.getProperties().get("width", Float.class);
            movementZoneHeight = obj.getProperties().get("height", Float.class);
        }

        MovingNpcComponent movingNpcComponent = engine.createComponent(MovingNpcComponent.class);
        movingNpcComponent.velocityX = defaultVelocityX;
        movingNpcComponent.velocityY = defaultVelocityY;
        movingNpcComponent.movementZoneWidth = movementZoneWidth;
        movingNpcComponent.movementZoneHeight = movementZoneHeight;
        movingNpcComponent.initialX = initialX;
        movingNpcComponent.initialY = initialY;
        movingNpcComponent.flipping = false;
        return movingNpcComponent;
    }

    public AnimationComponent createAnimationComponent(String animationKey, float frameDuration, PoolableAnimation.PlayMode playMode) {
        // prepare key for startsWith(key)
        if (!animationKey.endsWith("/")) {
            animationKey += "/";
        }

        for (TextureAtlas.AtlasRegion region : atlas.getRegions()) {
            if (region.name.startsWith(animationKey)) {
                tempTextureRegionArray.add(region);
            }
        }

        AnimationComponent animationComponent = engine.createComponent(AnimationComponent.class);
        PoolableAnimation animation = animationComponent.animation;
        animation.setKeyFrames(tempTextureRegionArray);
        animation.setFrameDuration(frameDuration);
        animation.setPlayMode(playMode);

        tempTextureRegionArray.clear();

        return animationComponent;
    }

    public abstract void add(MapObject obj);

    public abstract void loadDependencies();
}
