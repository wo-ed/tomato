package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.RotationSyncComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class PumpkinCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "pumpkin/";

    public static final String DEFAULT = NAME + "default/";

    public static final float ORIGIN_X = 62;
    public static final float ORIGIN_Y = 49;
    public static final float OFFSET_X = -24;
    public static final float OFFSET_Y = -16;
    public static final float COLLISION_WIDTH = 80;
    public static final float COLLISION_HEIGHT = 64;

    public PumpkinCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        float scale = Float.parseFloat(obj.getProperties().get("scale", "1.0f", String.class));

        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(GravityComponent.class));
        entity.add(engine.createComponent(MortalComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));

        entity.add(createAnimationComponent(DEFAULT, 1.0f / 16.0f, PoolableAnimation.PlayMode.LOOP_RANDOM));

        ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
        index.z = 2998;
        entity.add(index);

        BoundsComponent boundsComponent = createBoundsComponent(obj, COLLISION_WIDTH * scale, COLLISION_HEIGHT * scale);
        Rectangle bounds = boundsComponent.bounds;
        entity.add(boundsComponent);

        entity.add(createMovingNpcComponent(obj, bounds.x, bounds.y));

        TiledCollisionComponent collisionComponent = engine.createComponent(TiledCollisionComponent.class);
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;
        entity.add(collisionComponent);

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.scaleX = scale;
        textureRegionComponent.scaleY = scale;
        textureRegionComponent.offsetX = OFFSET_X * scale;
        textureRegionComponent.offsetY = OFFSET_Y * scale;
        textureRegionComponent.originX = ORIGIN_X * scale;
        textureRegionComponent.originY = ORIGIN_Y * scale;
        entity.add(textureRegionComponent);

        entity.add(engine.createComponent(RotationComponent.class));
        RotationSyncComponent rotationSyncComponent = engine.createComponent(RotationSyncComponent.class);
        rotationSyncComponent.rotationScale = 0.67f / scale;
        entity.add(rotationSyncComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
