package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.trigger.FakeFinishComponent;
import com.woed.tomato.component.trigger.FinishComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class TreeCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "tree/";

    public static final String WINTER = NAME + "winter/";
    public static final String WINTER_0 = WINTER + "winter0";

    public static final float TRIGGER_WIDTH = 134;
    public static final float TRIGGER_HEIGHT = 1;
    public static final float OFFSET_X = -218;

    public TreeCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        if(!obj.getName().equals("fake_finish")) {
            entity.add(engine.createComponent(FinishComponent.class));
        } else {
            entity.add(engine.createComponent(FakeFinishComponent.class));
        }

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.region = atlas.findRegion(TreeCreator.WINTER_0);
        textureRegionComponent.offsetX = OFFSET_X;
        entity.add(textureRegionComponent);

        BoundsComponent boundsComponent = createBoundsComponent(obj, TRIGGER_WIDTH, TRIGGER_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;
        bounds.x -= OFFSET_X;
        entity.add(boundsComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
