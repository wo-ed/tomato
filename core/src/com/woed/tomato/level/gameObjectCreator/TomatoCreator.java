package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.RotationSyncComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.input.ControllerComponent;
import com.woed.tomato.component.input.KeyboardComponent;
import com.woed.tomato.component.input.PitchComponent;
import com.woed.tomato.component.input.TouchComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.sound.GroundCollisionSoundComponent;
import com.woed.tomato.component.util.PlayerComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class TomatoCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "tomato/";

    public static final String DEFAULT = NAME + "default/";

    public static final int OFFSET_X = -24;
    public static final int OFFSET_Y = -16;
    public static final int COLLISION_WIDTH = 80;
    public static final int COLLISION_HEIGHT = 64;
    public static final int ORIGIN_X = 62;
    public static final int ORIGIN_Y = 49;

    public TomatoCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(createAnimationComponent(TomatoCreator.DEFAULT, 0.125f, PoolableAnimation.PlayMode.LOOP_PINGPONG));
        entity.add(engine.createComponent(PitchComponent.class));
        entity.add(engine.createComponent(KeyboardComponent.class));
        entity.add(engine.createComponent(TouchComponent.class));
        entity.add(engine.createComponent(ControllerComponent.class));
        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(GravityComponent.class));
        entity.add(engine.createComponent(PlayerComponent.class));
        entity.add(engine.createComponent(RotationComponent.class));
        entity.add(engine.createComponent(RotationSyncComponent.class));

        GroundCollisionSoundComponent soundComponent = engine.createComponent(GroundCollisionSoundComponent.class);
        entity.add(soundComponent);

        ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
        index.z = 2999;
        entity.add(index);

        BoundsComponent boundsComponent = createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;

        entity.add(boundsComponent);

        TiledCollisionComponent collisionComponent = engine.createComponent(TiledCollisionComponent.class);
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;
        entity.add(collisionComponent);

        bounds.setSize(TomatoCreator.COLLISION_WIDTH, TomatoCreator.COLLISION_HEIGHT);

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.offsetX = TomatoCreator.OFFSET_X;
        textureRegionComponent.offsetY = TomatoCreator.OFFSET_Y;
        textureRegionComponent.originX = TomatoCreator.ORIGIN_X;
        textureRegionComponent.originY = TomatoCreator.ORIGIN_Y;
        entity.add(textureRegionComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
