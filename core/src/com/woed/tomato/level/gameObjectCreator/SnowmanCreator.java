package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.graphics.AnimationComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class SnowmanCreator extends BaseTipCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "snowman/";

    public static final String STAND = NAME + "stand/";

    public SnowmanCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = create(obj);

        AnimationComponent animationComponent = createAnimationComponent(STAND, 0.125f, PoolableAnimation.PlayMode.LOOP);
        entity.add(animationComponent);
        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.region = animationComponent.animation.getKeyFrame(0.0f);
        entity.add(textureRegionComponent);

        entity.add(createBoundsComponent(obj,
                textureRegionComponent.region.getRegionWidth(),
                textureRegionComponent.region.getRegionHeight()));

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
