package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.trigger.CollectibleComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.resource.TextureAtlasKeys;

public class LanternCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "lantern/";

    public static final String DEFAULT = NAME + "default/";

    public static final float TRIGGER_WIDTH = 128f;
    public static final float TRIGGER_HEIGHT = 128f;

    public LanternCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(TextureRegionComponent.class));
        entity.add(createAnimationComponent(LanternCreator.DEFAULT, 1 / 32f, PoolableAnimation.PlayMode.LOOP_RANDOM));
        entity.add(engine.createComponent(CollectibleComponent.class));

        ParticleEffectComponent pec = entityCreator.createPooledParticleEffectComponent(Assets.ParticleEffects.LANTERN_P, 1, 100);
        pec.offsetX = LanternCreator.TRIGGER_WIDTH / 2;
        pec.offsetY = LanternCreator.TRIGGER_HEIGHT / 8;
        entity.add(pec);

        BoundsComponent boundsComponent = createBoundsComponent(obj, TRIGGER_WIDTH, TRIGGER_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;
        entity.add(boundsComponent);

        bounds.setSize(LanternCreator.TRIGGER_WIDTH, LanternCreator.TRIGGER_HEIGHT);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.LANTERN_P, ParticleEffect.class, particleEffectParameter);
    }
}
