package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class ScarecrowCreator extends BaseTipCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "scarecrow/";

    public static final String DEFAULT = NAME + "default/";
    public static final String DEFAULT_0 = DEFAULT + "default";

    public ScarecrowCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = create(obj);

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.region = atlas.findRegion(DEFAULT_0);
        entity.add(textureRegionComponent);

        entity.add(createBoundsComponent(obj,
                textureRegionComponent.region.getRegionWidth(),
                textureRegionComponent.region.getRegionHeight()));

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
