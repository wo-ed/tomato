package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.ai.JumpingNpcComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.graphics.RotationComponent;
import com.woed.tomato.component.graphics.RotationSyncComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.resource.TextureAtlasKeys;

public class SkullCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "skull/";

    public static final String DEFAULT = NAME + "default/";

    public static final int OFFSET_X = -38;
    public static final int OFFSET_Y = -8;
    public static final int COLLISION_WIDTH = 55;
    public static final int COLLISION_HEIGHT = 103;
    public static final int ORIGIN_X = 69;
    public static final int ORIGIN_Y = 62;

    public SkullCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(GravityComponent.class));
        entity.add(engine.createComponent(MortalComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));
        entity.add(createAnimationComponent(SkullCreator.DEFAULT, 1.0f / 16.0f, PoolableAnimation.PlayMode.LOOP_RANDOM));

        ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
        index.z = 2995;
        entity.add(index);

        BoundsComponent boundsComponent = createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;
        entity.add(boundsComponent);

        TiledCollisionComponent collisionComponent = engine.createComponent(TiledCollisionComponent.class);
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;
        entity.add(collisionComponent);

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.offsetX = SkullCreator.OFFSET_X;
        textureRegionComponent.offsetY = SkullCreator.OFFSET_Y;
        textureRegionComponent.originX = SkullCreator.ORIGIN_X;
        textureRegionComponent.originY = SkullCreator.ORIGIN_Y;
        entity.add(textureRegionComponent);

        entity.add(createMovingNpcComponent(obj, bounds.x, bounds.y));

        entity.add(engine.createComponent(RotationComponent.class));
        RotationSyncComponent rotationSyncComponent = engine.createComponent(RotationSyncComponent.class);
        rotationSyncComponent.rotationScale = 0.5f;
        entity.add(rotationSyncComponent);

        JumpingNpcComponent jumpingMovement = engine.createComponent(JumpingNpcComponent.class);
        jumpingMovement.jumpVelocity = jumpVelocity;
        entity.add(jumpingMovement);

        /*ParticleEffectComponent pec = entityCreator.createPooledParticleEffectComponent(Assets.ParticleEffects.SKULL_P, 1, 100);
        pec.offsetX = 128 / 2f + OFFSET_X;
        pec.offsetY = 128 / 2f + OFFSET_Y;
        entity.add(pec);*/

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.SKULL_P, ParticleEffect.class, particleEffectParameter);
    }
}
