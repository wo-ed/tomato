package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.woed.tomato.component.trigger.TipComponent;
import com.woed.tomato.level.EntityCreator;

public abstract class BaseTipCreator extends BaseGameObjectCreator {
    public BaseTipCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    protected Entity create(MapObject obj) {
        Entity entity = engine.createEntity();

        TipComponent tip = engine.createComponent(TipComponent.class);
        tip.text = obj.getProperties().get("tip", "default", String.class);
        entity.add(tip);

        return entity;
    }
}
