package com.woed.tomato.level.gameObjectCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Rectangle;
import com.woed.tomato.component.ai.MovingNpcComponent;
import com.woed.tomato.component.graphics.AnimationComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.physics.GravityComponent;
import com.woed.tomato.component.physics.TiledCollisionComponent;
import com.woed.tomato.component.physics.VelocityComponent;
import com.woed.tomato.component.trigger.DeadlyComponent;
import com.woed.tomato.component.trigger.MortalComponent;
import com.woed.tomato.graphics.PoolableAnimation;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class MonsterCreator extends BaseGameObjectCreator {
    public static final String NAME = TextureAtlasKeys.ENTITY + "monster/";

    public static final String WALK = NAME + "walk/";

    public static final int OFFSET_X = 0;
    public static final int OFFSET_Y = 0;
    public static final int COLLISION_WIDTH = 128;
    public static final int COLLISION_HEIGHT = 128;

    public MonsterCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(MapObject obj) {
        Entity entity = engine.createEntity();
        entity.add(engine.createComponent(VelocityComponent.class));
        entity.add(engine.createComponent(GravityComponent.class));
        entity.add(engine.createComponent(MortalComponent.class));
        entity.add(engine.createComponent(DeadlyComponent.class));

        AnimationComponent animationComponent = createAnimationComponent(MonsterCreator.WALK, 0.125f, PoolableAnimation.PlayMode.LOOP);
        entity.add(animationComponent);

        ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
        index.z = 2997;
        entity.add(index);

        BoundsComponent boundsComponent = createBoundsComponent(obj, COLLISION_WIDTH, COLLISION_HEIGHT);
        Rectangle bounds = boundsComponent.bounds;
        entity.add(boundsComponent);

        TiledCollisionComponent collisionComponent = engine.createComponent(TiledCollisionComponent.class);
        collisionComponent.oldX = bounds.x;
        collisionComponent.oldY = bounds.y;
        entity.add(collisionComponent);

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.offsetX = MonsterCreator.OFFSET_X;
        textureRegionComponent.offsetY = MonsterCreator.OFFSET_Y;
        textureRegionComponent.region = animationComponent.animation.getKeyFrame(0.0f);
        entity.add(textureRegionComponent);

        MovingNpcComponent movingNpcComponent = createMovingNpcComponent(obj, bounds.x, bounds.y);
        movingNpcComponent.flipping = true;
        entity.add(movingNpcComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
