package com.woed.tomato.level;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.woed.tomato.component.camera.AttachableComponent;
import com.woed.tomato.component.camera.CameraComponent;
import com.woed.tomato.component.camera.StaticZoomComponent;
import com.woed.tomato.component.camera.SynchronizingBoundsComponent;
import com.woed.tomato.component.camera.ZoomingAreaComponent;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.map.HidingMapLayerComponent;
import com.woed.tomato.component.map.TiledMapTileLayerComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.util.PlayerComponent;
import com.woed.tomato.component.util.SpawningComponent;
import com.woed.tomato.component.util.StaticComponent;
import com.woed.tomato.level.gameObjectCreator.BaseGameObjectCreator;
import com.woed.tomato.level.gameObjectCreator.BatCreator;
import com.woed.tomato.level.gameObjectCreator.FreezeTipCreator;
import com.woed.tomato.level.gameObjectCreator.GhostCreator;
import com.woed.tomato.level.gameObjectCreator.LanternCreator;
import com.woed.tomato.level.gameObjectCreator.MonsterCreator;
import com.woed.tomato.level.gameObjectCreator.PumpkinCreator;
import com.woed.tomato.level.gameObjectCreator.ScarecrowCreator;
import com.woed.tomato.level.gameObjectCreator.SkullCreator;
import com.woed.tomato.level.gameObjectCreator.SnowballCreator;
import com.woed.tomato.level.gameObjectCreator.SnowmanCreator;
import com.woed.tomato.level.gameObjectCreator.TomatoCreator;
import com.woed.tomato.level.gameObjectCreator.TreeCreator;
import com.woed.tomato.level.surroundingCreator.BaseSurroundingCreator;
import com.woed.tomato.level.surroundingCreator.GroundSnowCreator;
import com.woed.tomato.level.surroundingCreator.LightningCreator;
import com.woed.tomato.level.surroundingCreator.Marshland2Creator;
import com.woed.tomato.level.surroundingCreator.MarshlandCreator;
import com.woed.tomato.level.surroundingCreator.MoonCreator;
import com.woed.tomato.level.surroundingCreator.SimpleParticleEntityCreator;
import com.woed.tomato.level.surroundingCreator.SimpleScrollingEntityCreator;
import com.woed.tomato.level.surroundingCreator.StarsCreator;
import com.woed.tomato.level.surroundingCreator.TreesCreator;
import com.woed.tomato.resource.Assets;

public class EntityCreator {

    private TextureAtlas atlas;
    private PooledEngine engine;
    private AssetManager assetManager;

    private ObjectMap<String, ParticleEffectPool> effectPools = new ObjectMap<>();
    private Array<ParticleEffectPool.PooledEffect> pooledEffects = new Array<>();
    private final ObjectMap<String, BaseGameObjectCreator> gameObjectCreators = new ObjectMap<>();
    private final ObjectMap<String, BaseSurroundingCreator> surroundingCreators = new ObjectMap<>();

    private static final Family playerFamily = Family.all(PlayerComponent.class).get();

    private ParticleEffectPool getParticleEffectPool(String path, int initialCapacity, int max) {
        ParticleEffectPool pool = effectPools.get(path, null);
        if (pool == null) {
            pool = new ParticleEffectPool(assetManager.get(path, ParticleEffect.class), initialCapacity, max);
            effectPools.put(path, pool);
        }
        return pool;
    }

    public EntityCreator(AssetManager assetManager, PooledEngine engine) {
        this.assetManager = assetManager;
        this.atlas = assetManager.get(Assets.SpriteSheets.TEXTURES_ATLAS);
        this.engine = engine;
        initCreators();
    }

    private void initCreators() {
        gameObjectCreators.put("tip", new ScarecrowCreator(this));
        gameObjectCreators.put("tip2", new SnowmanCreator(this));
        gameObjectCreators.put("finish", new TreeCreator(this));
        gameObjectCreators.put("fake_finish", new TreeCreator(this));
        gameObjectCreators.put("flying", new BatCreator(this));
        gameObjectCreators.put("flying2", new GhostCreator(this));
        gameObjectCreators.put("jumping", new SkullCreator(this));
        gameObjectCreators.put("simple", new PumpkinCreator(this));
        gameObjectCreators.put("simple2", new MonsterCreator(this));
        gameObjectCreators.put("simple3", new SnowballCreator(this));
        gameObjectCreators.put("collectible", new LanternCreator(this));
        gameObjectCreators.put("start", new TomatoCreator(this));
        gameObjectCreators.put("freeze_tip", new FreezeTipCreator(this));

        surroundingCreators.put("stars", new StarsCreator(this));
        surroundingCreators.put("moon", new MoonCreator(this));
        surroundingCreators.put("lightning", new LightningCreator(this));
//        surroundingCreators.put("rainclouds", new RainCloudsCreator(this));
        surroundingCreators.put("marshland", new MarshlandCreator(this));
        surroundingCreators.put("marshland2", new Marshland2Creator(this));
        surroundingCreators.put("ground_snow", new GroundSnowCreator(this));
//        surroundingCreators.put("trees", new TreesCreator(this));
        surroundingCreators.put("grass", new SimpleScrollingEntityCreator(this, Assets.Textures.GRASS_PNG));
        surroundingCreators.put("swamp", new SimpleScrollingEntityCreator(this, Assets.Textures.SWAMP_PNG));
        surroundingCreators.put("ground_mud", new SimpleScrollingEntityCreator(this, Assets.Textures.GROUND_MUD_PNG));
        surroundingCreators.put("ground_leaves", new SimpleScrollingEntityCreator(this, Assets.Textures.GROUND_LEAVES_PNG));
        surroundingCreators.put("ground_skulls", new SimpleScrollingEntityCreator(this, Assets.Textures.GROUND_SKULLS_PNG));
        surroundingCreators.put("ground_graveyard", new SimpleScrollingEntityCreator(this, Assets.Textures.GROUND_GRAVEYARD_PNG));
        surroundingCreators.put("fog", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.FOG_P,
                true, true, 1, 0.5f, 0.5f, 0.25f));
        surroundingCreators.put("fog2", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.FOG2_P,
                true, true, 1, 1, 0.5f, 0.5f));
        surroundingCreators.put("rain", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.RAIN_P,
                true, true, 1, 0, 0, 1));
        surroundingCreators.put("snow", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.SNOW_P,
                true, true, 1, 0, 0, 1));
        surroundingCreators.put("aurora", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.AURORA_P,
                true, true, 1, 1, 0.5f, 0.5f));
        surroundingCreators.put("falling_leaves", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.LEAVES_P,
                true, true, 0, 1, 1, 0));
//        surroundingCreators.put("darkclouds", new SimpleParticleEntityCreator(this, Assets.ParticleEffects.CLOUDS_P, true, true, 0, 0.5f,
//                1, 0.5f));
//        surroundingCreators.put("clouds", new WhiteCloudsCreator(this));
    }

    private MapObjects getGameObjects(TiledMap map) {
        return map.getLayers().get("enemies").getObjects();
    }

    public void loadDependencies(TiledLevel level) {
        TiledMap map = level.getMap();
        MapObjects objects = getGameObjects(map);
        for (MapObject obj : objects) {
            BaseGameObjectCreator gameObjectCreator = gameObjectCreators.get(obj.getName());
            gameObjectCreator.loadDependencies();
        }

        for (int i = 0; i < level.getBackground().size; i++) {
            String key = level.getBackground().get(i);
            BaseSurroundingCreator creator = surroundingCreators.get(key, null);
            if (creator != null) {
                creator.loadDependencies();
            }
        }
        for (int i = 0; i < level.getForeground().size; i++) {
            String key = level.getForeground().get(i);
            BaseSurroundingCreator creator = surroundingCreators.get(key, null);
            if (creator != null) {
                creator.loadDependencies();
            }
        }

        TiledMapTileLayer tileLayer = (TiledMapTileLayer) level.getMap().getLayers().get("collision");
        for (int row = 0; row < tileLayer.getHeight(); row++) {
            for (int col = 0; col < tileLayer.getWidth(); col++) {
                TiledMapTileLayer.Cell cell = tileLayer.getCell(col, row);
                if (cell != null) {
                    String groundType = cell.getTile().getProperties().get("groundType", null, String.class);
                    if (groundType != null) {
                        assetManager.load(Assets.Sounds.PATH + "/" + groundType, Sound.class);
                    }
                }
            }
        }
    }

    public void addGameObjects(TiledMap map) {
        MapObjects objects = getGameObjects(map);
        for (MapObject obj : objects) {
            addGameObject(obj);
        }
    }

    public void addGameObject(MapObject obj) {
        boolean spawning = Boolean.parseBoolean(obj.getProperties().get("spawning", "false", String.class));
        if (spawning) {
            addSpawningGameObject(obj);
        } else {
            addSingleGameObject(obj);
        }
    }

    private void addSpawningGameObject(MapObject obj) {
        Entity entity = engine.createEntity();
        SpawningComponent spawningComponent = engine.createComponent(SpawningComponent.class);
        spawningComponent.mapObject = obj;
        spawningComponent.frames = Integer.parseInt(obj.getProperties().get("frames", "100", String.class));
        entity.add(spawningComponent);
        engine.addEntity(entity);
    }

    public void addSingleGameObject(MapObject obj) {
        gameObjectCreators.get(obj.getName()).add(obj);
    }

    public ParticleEffectComponent createPooledParticleEffectComponent(String path, int capacity, int maxCapacity) {
        ParticleEffectComponent pc = engine.createComponent(ParticleEffectComponent.class);
        pc.path = path;
        ParticleEffectPool pool = getParticleEffectPool(path, capacity, maxCapacity);
        ParticleEffectPool.PooledEffect pooledEffect = pool.obtain();
        pooledEffects.add(pooledEffect);
        pc.pooledEffect = pooledEffect;
        pc.pooledEffect.setEmittersCleanUpBlendFunction(false);
        return pc;
    }

    public void addMapLayerEntities(TiledMap map) {

        for (int i = 0; i < map.getLayers().getCount(); i++) {
            MapLayer layer = map.getLayers().get(i);

            if (layer instanceof TiledMapTileLayer) {
                TiledMapTileLayer tileLayer = (TiledMapTileLayer) layer;

                Entity entity = engine.createEntity();

                TiledMapTileLayerComponent layerComponent = engine.createComponent(TiledMapTileLayerComponent.class);
                layerComponent.layer = tileLayer;
                layerComponent.map = map;
                entity.add(layerComponent);

                ZIndexComponent index = engine.createComponent(ZIndexComponent.class);
                if (layer.getProperties().containsKey("z_index")) {
                    index.z = Integer.parseInt(layer.getProperties().get("z_index", String.class));
                } else {
                    index.z = 4000 + i;
                }
                entity.add(index);

                if (layer.getName().equals("secret")) {
                    HidingMapLayerComponent hidingComponent = createHidingMapLayerComponent(tileLayer);
                    entity.add(hidingComponent);
                }

                entity.add(engine.createComponent(StaticComponent.class));

                engine.addEntity(entity);
            }
        }
    }

    public void addZoomingAreaEntities(TiledMap map) {
        MapLayer layer = map.getLayers().get("zoomingAreas");
        if (layer != null) {
            for (MapObject obj : layer.getObjects()) {
                Entity entity = engine.createEntity();
                ZoomingAreaComponent zoomingArea = engine.createComponent(ZoomingAreaComponent.class);
                float x = obj.getProperties().get("x", Float.class);
                float y = obj.getProperties().get("y", Float.class);
                float width = obj.getProperties().get("width", 0.0f, Float.class);
                float height = obj.getProperties().get("height", 0.0f, Float.class);
                zoomingArea.rect.set(x, y, width, height);
                zoomingArea.zoom = Float.parseFloat(obj.getProperties().get("zoom", "1.0f", String.class));
                zoomingArea.rate = Float.parseFloat(obj.getProperties().get("rate", "0.1f", String.class));
                entity.add(zoomingArea);
                entity.add(engine.createComponent(StaticComponent.class));
                engine.addEntity(entity);
            }
        }
    }

    public void addCameraEntity(OrthographicCamera camera) {
        Entity cameraEntity = engine.createEntity();
        ImmutableArray<Entity> players = engine.getEntitiesFor(playerFamily);
        if (players.size() > 0) {
            CameraComponent cameraComponent = engine.createComponent(CameraComponent.class);
            cameraComponent.camera = camera;
            cameraComponent.target = players.first();
            cameraEntity.add(cameraComponent);
        }
        engine.addEntity(cameraEntity);
    }

    private HidingMapLayerComponent createHidingMapLayerComponent(TiledMapTileLayer tileLayer) {
        HidingMapLayerComponent hidingComponent = engine.createComponent(HidingMapLayerComponent.class);
        hidingComponent.layer = tileLayer;

        for (int row = 0; row < tileLayer.getHeight(); row++) {
            for (int col = 0; col < tileLayer.getWidth(); col++) {
                TiledMapTileLayer.Cell cell = tileLayer.getCell(col, row);
                if (cell != null) {
                    Rectangle rect = new Rectangle();
                    rect.set(col * tileLayer.getTileWidth(), row * tileLayer.getTileHeight(),
                            tileLayer.getTileWidth(), tileLayer.getTileHeight());
                    hidingComponent.rectangles.add(rect);
                }
            }
        }
        return hidingComponent;
    }

    public void addSurroundingEntities(TiledLevel level) {
        for (int i = 0; i < level.getBackground().size; i++) {
            addSurroundingEntities(level.getBackground().get(i), false, i);
        }
        for (int i = 0; i < level.getForeground().size; i++) {
            addSurroundingEntities(level.getForeground().get(i), true, i);
        }
    }

    public void addSurroundingEntities(String effect, boolean foreground, int i) {
        int z = foreground ? 10000 + i * 100 : -9999 + i * 100;
        BaseSurroundingCreator creator = surroundingCreators.get(effect);
        if (creator != null) {
            creator.add(z, foreground);
        }
    }

    public Entity addBloodParticleEntity() {
        Entity entity = createSurroundingParticleEntity(Assets.ParticleEffects.DEAD_BLOOD_P,
                99999, true, true, 1, 1, 0.5f, 0.5f);
        engine.addEntity(entity);
        return entity;
    }

    public Entity addSwampParticleEntity() {
        Entity entity = createSurroundingParticleEntity(Assets.ParticleEffects.DEAD_SWAMP_P,
                99999, true, true, 1, 1, 0.5f, 0.5f);
        engine.addEntity(entity);
        return entity;
    }

    public void freePooledEffect(ParticleEffectPool.PooledEffect effect) {
        effect.free();
        pooledEffects.removeValue(effect, true);
    }

    public void freeAllPooledEffects() {
        for (ParticleEffectPool.PooledEffect effect : pooledEffects) {
            effect.free();
        }
        pooledEffects.clear();
    }

    public PooledEngine getEngine() {
        return engine;
    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    public Entity createSurroundingParticleEntity(String path, int z, boolean attachX, boolean attachY, float syncWidthFactor, float syncHeightFactor,
                                                  float viewportWidthOffsetFactor, float viewportHeightOffsetFactor) {
        Entity entity = engine.createEntity();

        ParticleEffectComponent effectComponent = createParticleEffectComponent(path);
        effectComponent.viewportWidthOffsetFactor = viewportWidthOffsetFactor;
        effectComponent.viewportHeightOffsetFactor = viewportHeightOffsetFactor;
        effectComponent.culling = false;
        entity.add(effectComponent);

        if (attachX || attachY) {
            AttachableComponent attachable = engine.createComponent(AttachableComponent.class);
            attachable.attachXEnabled = attachX;
            attachable.attachYEnabled = attachY;
            entity.add(attachable);
        }

        BoundsComponent boundsComponent = engine.createComponent(BoundsComponent.class);
        entity.add(boundsComponent);

        if (syncWidthFactor != 0 || syncHeightFactor != 0) {
            SynchronizingBoundsComponent syncComponent = engine.createComponent(SynchronizingBoundsComponent.class);
            syncComponent.syncWidthEnabled = syncWidthFactor != 0;
            syncComponent.syncHeightEnabled = syncHeightFactor != 0;
            syncComponent.syncWidthFactor = syncWidthFactor;
            syncComponent.syncHeightFactor = syncHeightFactor;
            entity.add(syncComponent);
        }

        ZIndexComponent zIndex = engine.createComponent(ZIndexComponent.class);
        zIndex.z = z;
        entity.add(zIndex);

        entity.add(engine.createComponent(StaticComponent.class));
        entity.add(engine.createComponent(StaticZoomComponent.class));

        return entity;
    }

    private ParticleEffectComponent createParticleEffectComponent(String path) {
        ParticleEffectComponent pc = engine.createComponent(ParticleEffectComponent.class);
        pc.path = path;
        pc.effect = new ParticleEffect(assetManager.get(path, ParticleEffect.class));
        pc.effect.setEmittersCleanUpBlendFunction(false);
        return pc;
    }
}
