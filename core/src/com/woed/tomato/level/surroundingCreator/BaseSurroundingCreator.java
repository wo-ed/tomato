package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.util.StaticComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.resource.TextureAtlasKeys;

public abstract class BaseSurroundingCreator {
    protected EntityCreator entityCreator;
    protected PooledEngine engine;
    protected TextureAtlas atlas;
    protected AssetManager assetManager;

    protected ParticleEffectLoader.ParticleEffectParameter particleEffectParameter
            = new ParticleEffectLoader.ParticleEffectParameter();

    public BaseSurroundingCreator(EntityCreator entityCreator) {
        this.entityCreator = entityCreator;
        this.engine = entityCreator.getEngine();
        this.atlas = entityCreator.getAtlas();
        this.assetManager = entityCreator.getAssetManager();

        particleEffectParameter.atlasFile = Assets.SpriteSheets.TEXTURES_ATLAS;
        particleEffectParameter.atlasPrefix = TextureAtlasKeys.PARTICLES;
    }

    protected Entity createScrollingEntity(String path, boolean foreground, int z) {
        Entity entity = engine.createEntity();

        TextureRegionComponent textureRegionComponent = engine.createComponent(TextureRegionComponent.class);
        textureRegionComponent.region = new TextureRegion(assetManager.get(path, Texture.class));
        entity.add(textureRegionComponent);

        ZIndexComponent zIndexComponent = engine.createComponent(ZIndexComponent.class);
        zIndexComponent.z = z;
        entity.add(zIndexComponent);

        ScrollingLayerComponent scrollingLayerComponent = engine.createComponent(ScrollingLayerComponent.class);
        if (foreground) {
            scrollingLayerComponent.velocityScale = 1.25f;
        } else {
            scrollingLayerComponent.velocityScale = 0.5f;
        }
        entity.add(scrollingLayerComponent);

        entity.add(engine.createComponent(StaticComponent.class));

        return entity;
    }

    public abstract void add(int z, boolean foreground);

    public abstract void loadDependencies();
}
