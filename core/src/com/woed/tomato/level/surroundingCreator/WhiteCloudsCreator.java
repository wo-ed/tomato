package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.utils.Array;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class WhiteCloudsCreator extends BaseSurroundingCreator {
    public WhiteCloudsCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    private static final float[] COLORS = { 0.4862745f, 0.4862745f, 0.4862745f };

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = entityCreator.createSurroundingParticleEntity(Assets.ParticleEffects.CLOUDS_P, z, true, true, 0, 0.5f,
                1, 0.5f);

        ParticleEffect effect = entity.getComponent(ParticleEffectComponent.class).effect;

        for (ParticleEmitter emitter : effect.getEmitters()) {
            emitter.getTint().setColors(COLORS);
            emitter.setAdditive(true);
        }


        effect.start();
        effect.update(40);

        effect.start();
        effect.update(20);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.CLOUDS_P, ParticleEffect.class, particleEffectParameter);
    }
}
