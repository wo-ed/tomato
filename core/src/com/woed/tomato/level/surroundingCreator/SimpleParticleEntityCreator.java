package com.woed.tomato.level.surroundingCreator;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.woed.tomato.level.EntityCreator;

public class SimpleParticleEntityCreator extends BaseSurroundingCreator {
    private String path;
    private boolean attachX;
    private boolean attachY;
    private float syncWidthFactor;
    private float syncHeightFactor;
    private float viewportOffsetWidthFactor;
    private float viewportOffsetHeightFactor;

    public SimpleParticleEntityCreator(EntityCreator entityCreator, String path, boolean attachX, boolean attachY,
                                       float syncWidthFactor, float syncHeightFactor, float viewportOffsetWidthFactor, float viewportOffsetHeightFactor) {
        super(entityCreator);
        this.path = path;
        this.attachX = attachX;
        this.attachY = attachY;
        this.syncWidthFactor = syncWidthFactor;
        this.syncHeightFactor = syncHeightFactor;
        this.viewportOffsetWidthFactor = viewportOffsetWidthFactor;
        this.viewportOffsetHeightFactor = viewportOffsetHeightFactor;
    }

    @Override
    public void add(int z, boolean foreground) {
        engine.addEntity(entityCreator.createSurroundingParticleEntity(path, z,
                attachX, attachY, syncWidthFactor, syncHeightFactor,
                viewportOffsetWidthFactor, viewportOffsetHeightFactor));
    }

    @Override
    public void loadDependencies() {
        assetManager.load(path, ParticleEffect.class, particleEffectParameter);
    }
}
