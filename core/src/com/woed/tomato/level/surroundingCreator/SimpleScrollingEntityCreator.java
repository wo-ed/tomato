package com.woed.tomato.level.surroundingCreator;

import com.badlogic.gdx.graphics.Texture;
import com.woed.tomato.level.EntityCreator;

public class SimpleScrollingEntityCreator extends BaseSurroundingCreator {
    private String path;

    public SimpleScrollingEntityCreator(EntityCreator entityCreator, String path) {
        super(entityCreator);
        this.path = path;
    }

    @Override
    public void add(int z, boolean foreground) {
        engine.addEntity(createScrollingEntity(path, foreground, z));
    }

    @Override
    public void loadDependencies() {
        assetManager.load(path, Texture.class);
    }
}
