package com.woed.tomato.level.surroundingCreator;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class StarsCreator extends BaseSurroundingCreator {
    public StarsCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        engine.addEntity(entityCreator.createSurroundingParticleEntity(Assets.ParticleEffects.STARS_P, z, true, true, 1, 1,
                0.5f, 0.5f));
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.STARS_P, ParticleEffect.class, particleEffectParameter);
    }
}
