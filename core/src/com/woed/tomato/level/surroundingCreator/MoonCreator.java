package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.woed.tomato.component.camera.AttachableComponent;
import com.woed.tomato.component.camera.StaticZoomComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.component.util.StaticComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.TextureAtlasKeys;

public class MoonCreator extends BaseSurroundingCreator {
    public static final String MOON = TextureAtlasKeys.ENTITY + "moon/fullmoon/fullmoon1";
    public static final String MOON_LIGHT = TextureAtlasKeys.PARTICLES + "light";

    public MoonCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        //MOON_LIGHT
        Entity lightEntity = engine.createEntity();

        TextureRegionComponent regionComponent = engine.createComponent(TextureRegionComponent.class);
        regionComponent.region = atlas.findRegion(MOON_LIGHT);
        lightEntity.add(regionComponent);

        BoundsComponent boundsComponent = engine.createComponent(BoundsComponent.class);
        boundsComponent.bounds.setSize(regionComponent.region.getRegionWidth(), regionComponent.region.getRegionHeight());
        lightEntity.add(boundsComponent);

        AttachableComponent attachable = engine.createComponent(AttachableComponent.class);
        attachable.viewportWidthOffsetFactor = 0.25f;
        attachable.viewportHeightOffsetFactor = 0.5f;
        attachable.x = -boundsComponent.bounds.width / 4;
        attachable.y = -boundsComponent.bounds.height / 4;
        lightEntity.add(attachable);

        ZIndexComponent zIndex = engine.createComponent(ZIndexComponent.class);
        zIndex.z = z;
        lightEntity.add(zIndex);

        lightEntity.add(engine.createComponent(StaticComponent.class));
        lightEntity.add(engine.createComponent(StaticZoomComponent.class));

        engine.addEntity(lightEntity);

        //MOON
        Entity moonEntity = engine.createEntity();

        regionComponent = engine.createComponent(TextureRegionComponent.class);
        regionComponent.region = atlas.findRegion(MOON);
        moonEntity.add(regionComponent);

        boundsComponent = engine.createComponent(BoundsComponent.class);
        boundsComponent.bounds.setSize(regionComponent.region.getRegionWidth(), regionComponent.region.getRegionHeight());
        moonEntity.add(boundsComponent);

        attachable = engine.createComponent(AttachableComponent.class);
        attachable.viewportWidthOffsetFactor = 0.25f;
        attachable.viewportHeightOffsetFactor = 0.5f;
        moonEntity.add(attachable);

        zIndex = engine.createComponent(ZIndexComponent.class);
        zIndex.z = z + 1;
        moonEntity.add(zIndex);

        moonEntity.add(engine.createComponent(StaticComponent.class));
        moonEntity.add(engine.createComponent(StaticZoomComponent.class));

        engine.addEntity(moonEntity);
    }

    @Override
    public void loadDependencies() {
    }
}
