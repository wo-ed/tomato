package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.woed.tomato.component.graphics.ParticleEffectComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class RainCloudsCreator extends BaseSurroundingCreator {
    public RainCloudsCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = entityCreator.createSurroundingParticleEntity(Assets.ParticleEffects.CLOUDS_P, z, true, true, 0, 0.5f,
                1, 0.5f);
        ParticleEffect effect = entity.getComponent(ParticleEffectComponent.class).effect;
        effect.start();
        effect.update(40);

        effect.start();
        effect.update(20);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.ParticleEffects.CLOUDS_P, ParticleEffect.class, particleEffectParameter);
    }
}
