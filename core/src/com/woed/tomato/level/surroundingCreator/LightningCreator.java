package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.woed.tomato.component.graphics.LightningComponent;
import com.woed.tomato.component.graphics.ZIndexComponent;
import com.woed.tomato.component.util.StaticComponent;
import com.woed.tomato.level.EntityCreator;

public class LightningCreator extends BaseSurroundingCreator {
    public LightningCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = engine.createEntity();

        entity.add(engine.createComponent(LightningComponent.class));
        entity.add(engine.createComponent(StaticComponent.class));

        ZIndexComponent zIndex = engine.createComponent(ZIndexComponent.class);
        zIndex.z = z;
        entity.add(zIndex);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
    }
}
