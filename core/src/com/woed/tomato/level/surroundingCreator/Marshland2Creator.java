package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class Marshland2Creator extends MarshlandCreator {
    public Marshland2Creator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = createScrollingEntity(Assets.Textures.MARSHLAND0_PNG, foreground, z + 1);
        ScrollingLayerComponent slc = entity.getComponent(ScrollingLayerComponent.class);
        TextureRegionComponent trc = entity.getComponent(TextureRegionComponent.class);
        if (foreground) {
            slc.velocityScale = 1.5f;
            trc.flipX = false;
        } else {
            slc.velocityScale = 0.75f;
            trc.flipX = true;
        }
        engine.addEntity(entity);

        super.add(z, foreground);
    }
}
