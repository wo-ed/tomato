package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class GroundSnowCreator extends BaseSurroundingCreator {
    public GroundSnowCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        engine.addEntity(createScrollingEntity(Assets.Textures.SNOW_LAYER1_PNG, foreground, z));
        Entity entity = createScrollingEntity(Assets.Textures.SNOW_LAYER0_PNG, foreground, z + 1);
        ScrollingLayerComponent slc2 = entity.getComponent(ScrollingLayerComponent.class);
        if (foreground) {
            slc2.velocityScale = 1.5f;
        } else {
            slc2.velocityScale = 0.75f;
        }
        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.Textures.SNOW_LAYER1_PNG, Texture.class);
        assetManager.load(Assets.Textures.SNOW_LAYER0_PNG, Texture.class);
    }
}
