package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.woed.tomato.component.camera.SynchronizingBoundsComponent;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.component.physics.BoundsComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class TreesCreator extends BaseSurroundingCreator {
    public TreesCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = createScrollingEntity(Assets.Textures.TREES_PNG, foreground, z + 1);
        ScrollingLayerComponent slc = entity.getComponent(ScrollingLayerComponent.class);
        TextureRegionComponent trc = entity.getComponent(TextureRegionComponent.class);
        if (foreground) {
            slc.velocityScale = 1.5f;
            trc.flipX = false;
        } else {
            slc.velocityScale = 0.75f;
            trc.flipX = true;
        }


        BoundsComponent boundsComponent = engine.createComponent(BoundsComponent.class);
        entity.add(boundsComponent);

        SynchronizingBoundsComponent syncComponent = engine.createComponent(SynchronizingBoundsComponent.class);
        syncComponent.syncWidthEnabled = false;
        syncComponent.syncHeightEnabled = true;
        syncComponent.syncHeightFactor = 3f;
        entity.add(syncComponent);

        engine.addEntity(entity);

        entity = createScrollingEntity(Assets.Textures.TREES_PNG, foreground, z);
        slc = entity.getComponent(ScrollingLayerComponent.class);
        trc = entity.getComponent(TextureRegionComponent.class);
        if (foreground) {
            trc.flipX = true;
        } else {
            trc.flipX = false;
        }

        boundsComponent = engine.createComponent(BoundsComponent.class);
        entity.add(boundsComponent);

        syncComponent = engine.createComponent(SynchronizingBoundsComponent.class);
        syncComponent.syncWidthEnabled = false;
        syncComponent.syncHeightEnabled = true;
        syncComponent.syncHeightFactor = 3f;
        entity.add(syncComponent);

        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.Textures.TREES_PNG, Texture.class);
    }
}
