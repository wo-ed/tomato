package com.woed.tomato.level.surroundingCreator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Texture;
import com.woed.tomato.component.graphics.ScrollingLayerComponent;
import com.woed.tomato.component.graphics.TextureRegionComponent;
import com.woed.tomato.level.EntityCreator;
import com.woed.tomato.resource.Assets;

public class MarshlandCreator extends BaseSurroundingCreator {
    public MarshlandCreator(EntityCreator entityCreator) {
        super(entityCreator);
    }

    @Override
    public void add(int z, boolean foreground) {
        Entity entity = createScrollingEntity(Assets.Textures.MARSHLAND0_PNG, foreground, z);
        ScrollingLayerComponent slc = entity.getComponent(ScrollingLayerComponent.class);
        TextureRegionComponent trc = entity.getComponent(TextureRegionComponent.class);
        if (foreground) {
            trc.flipX = true;
        } else {
            trc.flipX = false;
        }
        engine.addEntity(entity);
    }

    @Override
    public void loadDependencies() {
        assetManager.load(Assets.Textures.MARSHLAND0_PNG, Texture.class);
    }
}
