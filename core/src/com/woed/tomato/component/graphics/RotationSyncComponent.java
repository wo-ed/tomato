package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class RotationSyncComponent implements Component, Pool.Poolable {

    public float rotationScale = 1.0f;

    @Override
    public void reset() {
        rotationScale = 1.0f;
    }
}
