package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class ZIndexComponent implements Component, Pool.Poolable {
    public int z = 0;

    @Override
    public void reset() {
        z = 0;
    }
}
