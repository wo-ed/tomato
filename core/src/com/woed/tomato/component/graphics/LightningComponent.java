package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class LightningComponent implements Component, Pool.Poolable {
    public int limit = 256;

    @Override
    public void reset() {
        limit = 256;
    }
}
