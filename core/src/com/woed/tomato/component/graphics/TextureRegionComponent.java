package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Pool;

public final class TextureRegionComponent implements Component, Pool.Poolable {
    public TextureRegion region = null;
    public float offsetX = 0.0f;
    public float offsetY = 0.0f;
    public float originX = 0.0f;
    public float originY = 0.0f;
    public float scaleX = 1.0f;
    public float scaleY = 1.0f;
    public boolean flipX = false;
    public boolean flipY = false;

    @Override
    public void reset() {
        region = null;
        offsetX = 0.0f;
        offsetY = 0.0f;
        originX = 0.0f;
        originY = 0.0f;
        scaleX = 1.0f;
        scaleY = 1.0f;
        flipX = false;
        flipY = false;
    }
}
