package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class ScrollingLayerComponent implements Component, Pool.Poolable {
    public float velocityScale = 1.0f;

    @Override
    public void reset() {
        velocityScale = 1.0f;
    }
}
