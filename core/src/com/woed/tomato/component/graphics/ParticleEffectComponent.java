package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.utils.Pool;

public final class ParticleEffectComponent implements Component, Pool.Poolable {
    public ParticleEffectPool.PooledEffect pooledEffect;
    public ParticleEffect effect;
    public String path;
    public boolean culling = true;
    public float offsetX = 0.0f;
    public float offsetY = 0.0f;
    public float viewportWidthOffsetFactor = 0.0f;
    public float viewportHeightOffsetFactor = 0.0f;
    public float accumulator;

    @Override
    public void reset() {
        pooledEffect = null;
        path = null;
        culling = true;
        offsetX = 0.0f;
        offsetY = 0.0f;
        viewportWidthOffsetFactor = 0.0f;
        viewportHeightOffsetFactor = 0.0f;
        accumulator = 0.0f;
    }
}
