package com.woed.tomato.component.graphics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;
import com.woed.tomato.graphics.PoolableAnimation;

public final class AnimationComponent implements Component, Pool.Poolable {
    public PoolableAnimation animation = new PoolableAnimation();

    @Override
    public void reset() {
        animation.reset();
    }
}
