package com.woed.tomato.component.map;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Pool;

public final class TiledMapTileLayerComponent implements Component, Pool.Poolable {
    public TiledMapTileLayer layer;
    public TiledMap map;

    @Override
    public void reset() {
        layer = null;
        map = null;
    }
}
