package com.woed.tomato.component.map;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

public class HidingMapLayerComponent implements Component, Pool.Poolable {

    public Array<Rectangle> rectangles = new Array<>();
    public MapLayer layer;

    @Override
    public void reset() {
        rectangles.clear();
        layer = null;
    }
}
