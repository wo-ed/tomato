package com.woed.tomato.component.util;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.utils.Pool;

public class SpawningComponent implements Component, Pool.Poolable {
    public MapObject mapObject;
    public int frames = 1;

    @Override
    public void reset() {
        mapObject = null;
        frames = 1;
    }
}
