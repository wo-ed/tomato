package com.woed.tomato.component.util;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class PlayerComponent implements Component, Pool.Poolable {

    public int playerId = 0;

    @Override
    public void reset() {
        playerId = 0;
    }
}