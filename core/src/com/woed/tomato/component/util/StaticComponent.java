package com.woed.tomato.component.util;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class StaticComponent implements Component, Pool.Poolable {
    @Override
    public void reset() {

    }
}
