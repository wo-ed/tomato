package com.woed.tomato.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class JumpingNpcComponent implements Component, Pool.Poolable {
    public float jumpVelocity = 0.0f;

    @Override
    public void reset() {
        jumpVelocity = 0.0f;
    }
}
