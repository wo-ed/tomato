package com.woed.tomato.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class FlyingAttackingNpcComponent implements Component, Pool.Poolable {
    public static final float DEFAULT_MAX_V = 450;

    public float maxV = DEFAULT_MAX_V;

    @Override
    public void reset() {
        maxV = DEFAULT_MAX_V;
    }
}
