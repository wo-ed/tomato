package com.woed.tomato.component.ai;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class MovingNpcComponent implements Component, Pool.Poolable {
    public float initialX = 0.0f;
    public float initialY = 0.0f;
    public float movementZoneWidth = 0.0f;
    public float movementZoneHeight = 0.0f;
    public float velocityX = 0.0f;
    public float velocityY = 0.0f;
    public boolean flipping = false;

    @Override
    public void reset() {
        initialX = 0.0f;
        initialY = 0.0f;
        movementZoneWidth = 0.0f;
        movementZoneHeight = 0.0f;
        velocityX = 0.0f;
        velocityY = 0.0f;
        flipping = false;
    }
}
