package com.woed.tomato.component.sound;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class GroundCollisionSoundComponent implements Component, Pool.Poolable {

    //default value true => hack (the sound should not be played the first time)
    public boolean soundWasPlayed = true;

    @Override
    public void reset() {
        soundWasPlayed = true;
    }
}
