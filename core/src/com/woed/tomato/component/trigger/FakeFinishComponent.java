package com.woed.tomato.component.trigger;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class FakeFinishComponent implements Component, Pool.Poolable {
    public boolean soundWasPlayed = false;

    @Override
    public void reset() {
        soundWasPlayed = false;
    }
}
