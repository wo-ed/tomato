package com.woed.tomato.component.trigger;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class TipComponent implements Component, Pool.Poolable {
    public String text = "";

    @Override
    public void reset() {
        text = "";
    }
}
