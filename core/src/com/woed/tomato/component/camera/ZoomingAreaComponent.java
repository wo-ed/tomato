package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Pool;

public class ZoomingAreaComponent implements Component, Pool.Poolable {
    public Rectangle rect = new Rectangle();
    public float zoom = 0.0f;
    public float rate = 0.0f;

    @Override
    public void reset() {
        rect.set(0, 0, 0, 0);
        zoom = 0.0f;
        rate = 0.0f;
    }
}
