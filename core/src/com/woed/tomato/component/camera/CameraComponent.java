package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Pool;

public final class CameraComponent implements Component, Pool.Poolable {
    public OrthographicCamera camera;
    public Entity target;

    @Override
    public void reset() {
        camera = null;
        target = null;
    }
}
