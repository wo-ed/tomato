package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class OutOfViewportComponent implements Component, Pool.Poolable {
    public boolean top = false;
    public boolean bottom = false;
    public boolean left = false;
    public boolean right = false;

    @Override
    public void reset() {
        top = false;
        bottom = false;
        left = false;
        right = false;
    }
}
