package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class SynchronizingBoundsComponent implements Component, Pool.Poolable {
    public boolean syncWidthEnabled = true;
    public boolean syncHeightEnabled = true;
    public float syncWidthFactor = 0.0f;
    public float syncHeightFactor = 0.0f;

    @Override
    public void reset() {
        syncWidthEnabled = true;
        syncHeightEnabled = true;
        syncWidthFactor = 0.0f;
        syncHeightFactor = 0.0f;
    }
}
