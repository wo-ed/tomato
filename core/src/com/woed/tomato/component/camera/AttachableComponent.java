package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class AttachableComponent implements Component, Pool.Poolable {
    public boolean attachXEnabled = true;
    public boolean attachYEnabled = true;
    public float viewportWidthOffsetFactor = 0.0f;
    public float viewportHeightOffsetFactor = 0.0f;
    public float x = 0.0f;
    public float y = 0.0f;

    @Override
    public void reset() {
        attachXEnabled = true;
        attachYEnabled = true;
        viewportWidthOffsetFactor = 0.0f;
        viewportHeightOffsetFactor = 0.0f;
        x = 0.0f;
        y = 0.0f;
    }
}
