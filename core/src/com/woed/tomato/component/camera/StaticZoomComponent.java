package com.woed.tomato.component.camera;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class StaticZoomComponent implements Component, Pool.Poolable {
    public float zoom = 1.0f;

    @Override
    public void reset() {
        zoom = 1.0f;
    }
}
