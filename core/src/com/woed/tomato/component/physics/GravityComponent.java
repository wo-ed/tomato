package com.woed.tomato.component.physics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public final class GravityComponent implements Component, Pool.Poolable {
    public float gravityScaleX = 1.0f;
    public float gravityScaleY = 1.0f;

    @Override
    public void reset() {
        gravityScaleX = 1.0f;
        gravityScaleY = 1.0f;
    }
}
