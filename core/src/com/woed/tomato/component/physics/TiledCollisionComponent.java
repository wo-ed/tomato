package com.woed.tomato.component.physics;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;
import com.woed.tomato.collision.MapCollision;

public final class TiledCollisionComponent implements Component, Pool.Poolable {
    public MapCollision mapCollision = new MapCollision();
    public float oldX = 0.0f;
    public float oldY = 0.0f;

    @Override
    public void reset() {
        mapCollision.reset();
        oldX = 0.0f;
        oldY = 0.0f;
    }
}
