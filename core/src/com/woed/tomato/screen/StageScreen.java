package com.woed.tomato.screen;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class StageScreen implements Screen {

    private Stage stage;

    public StageScreen(Viewport viewport, Batch batch) {
        this(new Stage(viewport, batch));
    }

    public StageScreen(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void render(float deltaTime) {
        this.stage.getViewport().apply();
        this.stage.act(deltaTime);
        this.stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void dispose() {
        this.stage.dispose();
    }
}
