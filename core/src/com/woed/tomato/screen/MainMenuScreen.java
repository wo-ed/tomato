package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.woed.tomato.action.MoveUpAction;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.util.AdsController;


public class MainMenuScreen extends MenuScreen {
    private static float PAD_RIGHT = 64f;

    private Label label;
    private Button levelsButton;
    private Button settingsButton;
    private Image faceActor;
    private Texture face_eyes_open = new Texture(Assets.Textures.FACE_EYES_OPEN_PNG);
    private Texture face_eyes_closed = new Texture(Assets.Textures.FACE_EYES_CLOSED_PNG);
    private SpriteDrawable faceDrawable = new SpriteDrawable();

    public MainMenuScreen(ScreenExchangeObject exchangeObject) {
        super(exchangeObject);
        getExchangeObject().loopMusic();
        initializeUI();
    }

    public void initializeUI() {
        getRootTable().row().expandX().right();
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.font = getExchangeObject().getMediumSizedFont();
        labelStyle.fontColor = Color.BLACK;

        final Group group = new VerticalGroup().padRight(PAD_RIGHT).fill();
        getRootTable().add(group);

        label = new Label("TOMATO", labelStyle);
        label.setAlignment(Align.center, Align.center);
        group.addActor(label);

        levelsButton = getExchangeObject().createLocalizedGreenButton("levels");
        group.addActor(levelsButton);
        levelsButton.addListener(new ClickListener() {

            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {

                float maxY = getStage().getCamera().viewportHeight;
                float amount = maxY / 25.0f;

                MoveUpAction a1 = new MoveUpAction();
                a1.setMaxY(maxY);
                a1.setAmountY(amount);
                group.addAction(a1);

                MoveUpAction a2 = new MoveUpAction();
                a2.setMaxY(maxY);
                a2.setAmountY(amount);
                if (getExchangeObject().getSettings().isSoundEnabled()) {
                    // getExchangeObject().getLaughter().play();
                }
                faceActor.addAction(Actions.sequence(a2, new Action() {
                    @Override
                    public boolean act(float delta) {

                        getExchangeObject().getGame().setScreen(new LevelScreen(getExchangeObject(), LevelScreen.Movement.MOVE_UP_IN));
                        dispose();
                        return true;
                    }
                }));
            }
        });

        settingsButton = getExchangeObject().createLocalizedGreenButton("settings");
        group.addActor(settingsButton);
        settingsButton.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                getExchangeObject().getGame().setScreen(new SettingsScreen(getExchangeObject()));
                dispose();
            }
        });

        faceDrawable.setSprite(new Sprite(face_eyes_open));
        faceActor = new Image(faceDrawable);
        faceActor.setY(-face_eyes_open.getHeight() / 1.75f);
        faceActor.addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y,
                                     int pointer, int button) {
                faceDrawable.getSprite().setTexture(face_eyes_closed);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                faceDrawable.getSprite().setTexture(face_eyes_open);
            }
        });
        getRootTable().addActor(faceActor);
    }

    @Override
    public void render(float deltaTime) {
        super.render(deltaTime);
        handleBackKeyPressed();
    }

    private void handleBackKeyPressed() {
        if (Gdx.input.isKeyJustPressed(Keys.BACK) || Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
            AdsController adsController = getExchangeObject().getAdsController();
            if (adsController.areAdsEnabled() && adsController.isWifiConnected()) {
                adsController.showInterstitialAd(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("Ads", "Interstitial ad loaded");
                        Gdx.app.exit();
                    }
                });
            } else {
                Gdx.app.exit();
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        face_eyes_open.dispose();
        face_eyes_closed.dispose();
    }
}
