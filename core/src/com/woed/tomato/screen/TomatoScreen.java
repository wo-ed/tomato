package com.woed.tomato.screen;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;

public class TomatoScreen extends StageScreen implements Disposable {

    private Table rootTable = new Table();
    private ScreenExchangeObject exchangeObject;

    public TomatoScreen(Stage stage, ScreenExchangeObject exchangeObject) {
        super(stage);
        this.exchangeObject = exchangeObject;

        rootTable.setFillParent(true);
        getStage().addActor(rootTable);
    }

    public TomatoScreen(ScreenExchangeObject exchangeObject) {
        this(new Stage(exchangeObject.getMenuViewport(), exchangeObject.getBatch()), exchangeObject);

    }

    public ScreenExchangeObject getExchangeObject() {
        return exchangeObject;
    }

    public Table getRootTable() {
        return rootTable;
    }

    public void render(float deltaTime) {
        //exchangeObject.getAssetManager().update();
        super.render(deltaTime);
    }

    @Override
    public void show() {
        super.show();
        exchangeObject.getInputMultiplexer().addProcessor(getStage());
    }

    @Override
    public void hide() {
        super.hide();
        exchangeObject.getInputMultiplexer().removeProcessor(getStage());
    }

    @Override
    public void dispose() {
        super.dispose();
        exchangeObject.getInputMultiplexer().removeProcessor(getStage());
    }
}
