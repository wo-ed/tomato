package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class MenuScreen extends TomatoScreen {
    private Color bgColor = Color.valueOf(ScreenExchangeObject.TOMATO_RED);

    MenuScreen(Stage stage, ScreenExchangeObject exchangeObject) {
        super(stage, exchangeObject);
    }

    MenuScreen(ScreenExchangeObject exchangeObject) {
        super(exchangeObject);
    }

    void backgroundColor() {
        Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, bgColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render(float deltaTime) {
        backgroundColor();
        super.render(deltaTime);
    }
}
