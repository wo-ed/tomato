package com.woed.tomato.screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.I18NBundle;

import static com.badlogic.gdx.Gdx.input;

public class SettingsScreen extends MenuScreen {
    private Settings settings;
    private I18NBundle bundle;

    public SettingsScreen(ScreenExchangeObject exchangeObject) {
        super(exchangeObject);
        //getStage().setDebugAll(true);
        settings = getExchangeObject().getSettings();
        bundle = getExchangeObject().getBundle();
        initializeUI();
    }

    private void initializeUI() {
        Group checkBoxGroup = new VerticalGroup().left();

        final CheckBox soundBox = new CheckBox(bundle.get("sound"), getExchangeObject().getGreenCheckBoxStyle());
        soundBox.setChecked(settings.isSoundEnabled());
        soundBox.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                settings.setSoundEnabled(soundBox.isChecked()).saveChanges();
            }
        });
        checkBoxGroup.addActor(soundBox);

        if (input.isPeripheralAvailable(Input.Peripheral.Vibrator)) {
            final CheckBox vibrationBox = new CheckBox(bundle.get("vibration"), getExchangeObject().getGreenCheckBoxStyle());
            vibrationBox.setChecked(settings.isVibrationEnabled());
            vibrationBox.addListener(new ClickListener() {
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    settings.setVibrationEnabled(vibrationBox.isChecked()).saveChanges();
                }
            });
            checkBoxGroup.addActor(vibrationBox);
        }

        final CheckBox screenOnBox = new CheckBox(bundle.get("keep_screen_on"), getExchangeObject().getGreenCheckBoxStyle());
        screenOnBox.setChecked(settings.isScreenAlwaysOn());
        screenOnBox.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                settings.setScreenAlwaysOn(screenOnBox.isChecked()).saveChanges();
            }
        });
        checkBoxGroup.addActor(screenOnBox);

        ScrollPane scrollPane = new ScrollPane(checkBoxGroup);
        getRootTable().add(scrollPane);
    }

    public void render(float deltaTime) {
        super.render(deltaTime);
        if (input.isKeyJustPressed(Input.Keys.BACK) || input.isKeyPressed(Input.Keys.ESCAPE)) {
            StageScreen screen = new MainMenuScreen(getExchangeObject());
            getExchangeObject().getGame().setScreen(screen);
            dispose();
            return;
        }
    }
}
