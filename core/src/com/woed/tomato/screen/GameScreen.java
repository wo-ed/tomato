package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.woed.tomato.graphics.GradientLayer;
import com.woed.tomato.input.OneKeyInputProcessor;
import com.woed.tomato.level.TiledLevel;
import com.woed.tomato.util.AdsController;
import com.woed.tomato.util.MagicalTextButton;
import com.woed.tomato.world.GameListener;
import com.woed.tomato.world.GameWorld;

public class GameScreen extends TomatoScreen implements GameListener {
    private GameWorld gameWorld;
    private TiledLevel level;
    private Label deathCounterLabel;
    private VerticalGroup buttonGroup;
    private MagicalTextButton continueButton;
    private MagicalTextButton restartButton;
    private MagicalTextButton quitButton;
    private TextField textField;
    private OneKeyInputProcessor backKeyInputProcessor;
    private GradientLayer background;
    private TextButtonStyle textButtonStyle = new TextButtonStyle();
    private boolean isSoundEnbled;

    GameScreen(ScreenExchangeObject exchangeObject, GameWorld gameWorld, TiledLevel level) {
        super(new Stage(exchangeObject.getGameViewport(), exchangeObject.getBatch()), exchangeObject);
        this.level = level;
        this.gameWorld = gameWorld;
        this.isSoundEnbled = getExchangeObject().getSettings().isSoundEnabled();

        backKeyInputProcessor = new OneKeyInputProcessor(Input.Keys.BACK);
        exchangeObject.getInputMultiplexer().addProcessor(backKeyInputProcessor);
        textButtonStyle.font = getExchangeObject().getMediumSizedFont();
        textButtonStyle.fontColor = Color.CYAN;

        background = new GradientLayer(level.getBgColorBottom(), level.getBgColorBottom(),
                level.getBgColorTop(), level.getBgColorTop());

        getExchangeObject().stopMusic();
        initializeUI();
        gameWorld.setGameListener(this);

        if (level.getMusic() != null) {
            Music music = level.getMusic().getAsset();
            if (music != null && isSoundEnbled) {
                music.setLooping(true);
                music.play();
            }
        }

        gameWorld.startLevel();
    }

    private void initializeUI() {
        Table rootTable = getRootTable();
        rootTable.row().expandX().left();

        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.fontColor = Color.RED;
        labelStyle.font = getExchangeObject().getMediumSizedFont();
        deathCounterLabel = new Label("", labelStyle);
        rootTable.add(deathCounterLabel);

        rootTable.row().expandX().top();
        buttonGroup = new VerticalGroup().fill();
        buttonGroup.setVisible(false);
        rootTable.add(buttonGroup);

        rootTable.row().expand().top().fillX();
        TextField.TextFieldStyle textFieldStyle = new TextField.TextFieldStyle();
        textFieldStyle.font = getExchangeObject().getMediumSizedFont();
        textFieldStyle.fontColor = Color.RED;
        textField = new TextField("", textFieldStyle);
        textField.setDisabled(true);
        textField.setColor(Color.DARK_GRAY);
        rootTable.add(textField).bottom();

        createButtons();
    }

    private void createButtons() {
        continueButton = createMagicalTextButton(getExchangeObject().getBundle().get("continue"));
        restartButton = createMagicalTextButton(getExchangeObject().getBundle().get("restart"));
        quitButton = createMagicalTextButton(getExchangeObject().getBundle().get("quit"));
    }

    private MagicalTextButton createMagicalTextButton(String caption) {
        MagicalTextButton button = new MagicalTextButton(caption, textButtonStyle);
        button.initialize(getExchangeObject().getAssetManager());
        buttonGroup.addActor(button);
        button.addListener(new ButtonClickListener(button));
        return button;
    }

    private class ButtonClickListener extends ClickListener {
        private Button button;

        public ButtonClickListener(Button button) {
            this.button = button;
        }

        @Override
        public void touchUp(InputEvent event, float x, float y,
                            int pointer, int button) {
            handleButtonClick(this.button);
        }
    }

    private void handleButtonClick(Button button) {
        if (button == continueButton) {
            continueGame();
        } else if (button == restartButton) {
            restartGame();
        } else if (button == quitButton) {
            quitToMenu();
        }
    }

    private void continueGame() {
        if (gameWorld.isFinished()) {
            if (level.getData().getId() == LevelScreen.LAST_LEVEL_ID) {
                Screen screen = new MainMenuScreen(getExchangeObject());
                getExchangeObject().getGame().setScreen(screen);
                dispose();
            } else {
                LoadingScreen screen = new LoadingScreen(getExchangeObject(), LevelScreen.getNextLevel(level.getData().getId()));
                getExchangeObject().getGame().setScreen(screen);
                dispose();
                screen.load();
            }
        } else {
            gameWorld.setPaused(false);
        }
    }

    //TODO restart after finished bug
    private void restartGame() {
        gameWorld.setFinished(false);
        gameWorld.setPaused(false);
        gameWorld.setDeathsCount(-1);
        gameWorld.startLevel();
    }

    private void quitToMenu() {
        LevelScreen screen = new LevelScreen(getExchangeObject(), LevelScreen.Movement.NONE);
        getExchangeObject().getGame().setScreen(screen);
        dispose();
    }

    @Override
    public void render(float deltaTime) {
        background.render();

        if ((backKeyInputProcessor.isKeyPressed() || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) && !gameWorld.isFinished()) {
            gameWorld.setPaused(!gameWorld.isPaused());
        }

        getExchangeObject().getBatch().setColor(level.getBatchColor());
        gameWorld.render(deltaTime);

        //deathCounterLabel.setText(Integer.toString(gameWorld.getDeathsCount()));
        deathCounterLabel.setText(Integer.toString(Gdx.graphics.getFramesPerSecond()));
        textField.setText(gameWorld.getTip());
        textField.setVisible(gameWorld.isTipVisible());
        super.render(deltaTime);
        getExchangeObject().getBatch().setColor(Color.WHITE);
    }

    @Override
    public void gameOver() {

    }

    @Override
    public void gameFinished() {
        getExchangeObject().getSaveStateManager().setLevelCompleted(level.getData().getId());
        if (level.getMusic() != null && level.getMusic().getAsset() != null) {
            level.getMusic().getAsset().stop();
            level.getMusic().getAsset().setLooping(false);
        }
        buttonGroup.setVisible(true);
        AdsController adsController = getExchangeObject().getAdsController();

        if (adsController.areAdsEnabled()) {
            if (adsController.isWifiConnected()) {
                adsController.showInterstitialAd(new Runnable() {
                    @Override
                    public void run() {
                        Gdx.app.log("Ads", "Interstitial ad loaded");
                    }
                });
            } else {
                Gdx.app.log("Ads", "Interstitial ad not loaded, WiFi not connected");
            }
        }
    }

    @Override
    public void gamePausedChanged(boolean paused) {
        buttonGroup.setVisible(paused);
    }

    @Override
    public void dispose() {
        super.dispose();
        getExchangeObject().getInputMultiplexer().removeProcessor(backKeyInputProcessor);
        continueButton.dispose();
        restartButton.dispose();
        quitButton.dispose();
        background.dispose();
        disposeAssets();
        System.gc();
    }

    private void disposeAssets() {
        //TODO: dispose assets
        level.getMap().dispose();
        if (level.getMusic() != null) {
            level.getMusic().dispose();
        }
        if (level.getFallingSound() != null) {
            level.getFallingSound().dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        gameWorld.updateViewport(width, height);
    }
}
