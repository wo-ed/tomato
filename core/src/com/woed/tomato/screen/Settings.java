package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.woed.tomato.util.OsManager;

public class Settings {
    public static final String SETTINGS_KEY = "settings";
    public static final String SOUND_KEY = "sound";
    public static final String VIBRATION_KEY = "vibration";
    public static final String SCREEN_ON_KEY = "screen_always_on";

    public static final boolean SOUND_DEFAULT = true;
    public static final boolean VIBRATION_DEFAULT = true;
    public static final boolean SCREEN_ON_DEFAULT = false;

    private Preferences preferences = Gdx.app.getPreferences(SETTINGS_KEY);
    private OsManager osManager;

    public Settings(OsManager osManager) {
        this.osManager = osManager;
        if (preferences.getBoolean(SCREEN_ON_KEY, SCREEN_ON_DEFAULT)) {
            osManager.setScreenAlwaysOn(true);
        }
    }

    public boolean isSoundEnabled() {
        return preferences.getBoolean(SOUND_KEY, SOUND_DEFAULT);
    }

    public Settings setSoundEnabled(boolean soundEnabled) {
        preferences.putBoolean(SOUND_KEY, soundEnabled);
        return this;
    }

    public boolean isVibrationEnabled() {
        return preferences.getBoolean(VIBRATION_KEY, VIBRATION_DEFAULT);
    }

    public Settings setVibrationEnabled(boolean vibrationEnabled) {
        preferences.putBoolean(VIBRATION_KEY, vibrationEnabled);
        return this;
    }

    public boolean isScreenAlwaysOn() {
        return osManager.isScreenAlwaysOn();
    }

    public Settings setScreenAlwaysOn(boolean screenAlwaysOn) {
        osManager.setScreenAlwaysOn(screenAlwaysOn);
        preferences.putBoolean(SCREEN_ON_KEY, screenAlwaysOn);
        return this;
    }

    public void saveChanges() {
        preferences.flush();
    }
}
