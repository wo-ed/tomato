package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.woed.tomato.action.MoveUpAction;
import com.woed.tomato.level.TiledLevelData;
import com.woed.tomato.main.TomatoGame;
import com.woed.tomato.resource.Assets;

public class LevelScreen extends MenuScreen {

    private static final int TILES_PER_ROW = 6;
    private static final boolean UNLOCK_ALL = true;
    private static Array<String> LEVELS = new Array<>();

    private TiledLevelData chosenLevel;

    static {
        LEVELS.add(Assets.Levels.SUNSET_TMX);
        LEVELS.add(Assets.Levels.SILENT_SWAMP_TMX);
        LEVELS.add(Assets.Levels.THE_HILL_TMX);
        LEVELS.add(Assets.Levels.DARKWOOD_TMX);
        LEVELS.add(Assets.Levels.LEVEL_ONE_TMX);
        LEVELS.add(Assets.Levels.BOREDOM_TO_DEATH_TMX);
        LEVELS.add(Assets.Levels.NOVEMBER_TMX);
        LEVELS.add(Assets.Levels.ICE_CAVE_TMX);
        LEVELS.add(Assets.Levels.FALLING_SNOW_TMX);
        LEVELS.add(Assets.Levels.THE_ENTRANCE_TMX);
        LEVELS.add(Assets.Levels.CURSED_GRAVEYARD_TMX);
        LEVELS.add(Assets.Levels.BIG_GUYS_TMX);
        LEVELS.add(Assets.Levels.FINAL_STAGE_TMX);
    }

    static final int LAST_LEVEL_ID = LEVELS.size - 1;

    public enum Movement {
        NONE,
        MOVE_UP_IN
    }

    LevelScreen(ScreenExchangeObject exchangeObject, Movement movement) {
        super(exchangeObject);

        createLevelTiles();
        getExchangeObject().loopMusic();

        float viewportHeight = getStage().getCamera().viewportHeight;
        if (movement == Movement.MOVE_UP_IN) {
            getRootTable().setY(-viewportHeight);
            MoveUpAction action = new MoveUpAction();
            action.setMaxY(0);
            action.setAmountY(viewportHeight / 25.0f);
            getRootTable().addAction(action);
        }
    }

    private static Stage createStage(Batch batch) {
        return new Stage(new FitViewport(TomatoGame.VIEWPORT_WIDTH, TomatoGame.VIEWPORT_HEIGHT), batch);
    }

    private static TiledLevelData getLevel(int levelId) {
        String levelName = Integer.toString(levelId + 1);
        String mapPath = LEVELS.get(levelId);
        return new TiledLevelData(mapPath, levelName, levelId);
    }

    static TiledLevelData getNextLevel(int oldLevelId) {
        return getLevel(oldLevelId + 1);
    }

    private void createLevelTiles() {
        for (int levelId = 0; levelId < LEVELS.size; levelId++) {
            TiledLevelData level = getLevel(levelId);

            if (Gdx.files.internal(level.getMapPath()).exists()) {
                if (levelId != 0 && levelId % TILES_PER_ROW == 0) {
                    getRootTable().row();
                }

                Button tile = createLevelTile(level);

                int colspan;
                if (levelId == LAST_LEVEL_ID) {
                    tile.setColor(Color.BLACK);
                    colspan = TILES_PER_ROW;
                } else {
                    colspan = 1;
                }
                getRootTable().add(tile).colspan(colspan);

                if (!UNLOCK_ALL && !isLevelUnlocked(levelId)) {
                    tile.setVisible(false);
                }
            }
        }
    }

    private boolean isLevelUnlocked(int levelId) {
        return levelId == 0 || getExchangeObject().getSaveStateManager().isLevelCompleted(levelId - 1);
    }

    private Button createLevelTile(final TiledLevelData level) {
        Button button = getExchangeObject().createGreenButton(level.getName());
        button.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                chosenLevel = level;
            }
        });
        return button;
    }

    @Override
    public void render(float deltaTime) {
        super.render(deltaTime);
        if (Gdx.input.isKeyJustPressed(Keys.BACK) || Gdx.input.isKeyPressed(Keys.ESCAPE)) {
            Screen screen = new MainMenuScreen(getExchangeObject());
            getExchangeObject().getGame().setScreen(screen);
            dispose();
            return;
        }
        if (chosenLevel != null) {
            Screen screen = new LevelDetailScreen(getExchangeObject(), chosenLevel);
            getExchangeObject().getGame().setScreen(screen);
            dispose();
            return;
        }
    }
}
