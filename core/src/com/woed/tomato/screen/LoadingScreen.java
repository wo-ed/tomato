package com.woed.tomato.screen;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Array;
import com.woed.tomato.level.TiledLevel;
import com.woed.tomato.level.TiledLevelData;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.resource.TextureAtlasKeys;
import com.woed.tomato.util.AssetWrapper;
import com.woed.tomato.util.Util;
import com.woed.tomato.world.GameWorld;

import java.util.StringTokenizer;

public class LoadingScreen extends TomatoScreen {
    private AssetManager assetManager;
    private TiledLevelData levelData;

    public LoadingScreen(ScreenExchangeObject exchangeObject, TiledLevelData levelData) {
        super(exchangeObject);
        this.levelData = levelData;
        assetManager = getExchangeObject().getAssetManager();
    }

    public void load() {
        assetManager.load(Assets.SpriteSheets.TEXTURES_ATLAS, TextureAtlas.class);
        TmxMapLoader.Parameters mapParams = new TmxMapLoader.Parameters();
        mapParams.textureMinFilter = Texture.TextureFilter.Nearest;
        mapParams.textureMagFilter = Texture.TextureFilter.Nearest;
        assetManager.load(levelData.getMapPath(), TiledMap.class, mapParams);
        assetManager.load(Assets.Sounds.BITE_OGG, Sound.class);

        ParticleEffectLoader.ParticleEffectParameter parameter = new ParticleEffectLoader.ParticleEffectParameter();
        parameter.atlasFile = Assets.SpriteSheets.TEXTURES_ATLAS;
        parameter.atlasPrefix = TextureAtlasKeys.PARTICLES;
        assetManager.load(Assets.ParticleEffects.DEAD_BLOOD_P, ParticleEffect.class, parameter);
        assetManager.load(Assets.ParticleEffects.DEAD_SWAMP_P, ParticleEffect.class, parameter);
        assetManager.load(Assets.ParticleEffects.MAGICAL_BUTTON_P, ParticleEffect.class, parameter);
    }

    private GameWorld gameWorld = null;
    private TiledLevel level = null;

    @Override
    public void render(float delta) {
        if (assetManager.update()) {
            if (gameWorld == null) {
                TiledMap map = assetManager.get(levelData.getMapPath());
                level = new TiledLevel();
                level.setData(levelData);
                level.setMap(map);
                level.getForeground().addAll(tokenizeStringValue(map.getProperties(), "foreground"));
                level.getBackground().addAll(tokenizeStringValue(map.getProperties(), "background"));
                level.setMusic(getMusic(map.getProperties()));
                level.setFallingSound(getFallingSound(map.getProperties()));
                level.setGameObjectHitSound(assetManager.get(Assets.Sounds.BITE_OGG, Sound.class));

                String sBgColorTop = map.getProperties().get("bgColorTop", null, String.class);
                String sBgColorBottom = map.getProperties().get("bgColorBottom", null, String.class);
                String sBatchColor = map.getProperties().get("batchColor", null, String.class);

                Color bgColorTop = null;
                Color bgColorBottom = null;
                Color batchColor = null;
                if (sBgColorTop != null) {
                    bgColorTop = Color.valueOf(sBgColorTop);
                } else {
                    bgColorTop = Color.BLACK;
                }
                if (sBgColorBottom != null) {
                    bgColorBottom = Color.valueOf(sBgColorBottom);
                } else {
                    bgColorBottom = Color.BLACK;
                }
                if (sBatchColor != null) {
                    batchColor = Color.valueOf(sBatchColor);
                } else {
                    batchColor = Color.WHITE;
                }
                level.setBgColorTop(bgColorTop);
                level.setBgColorBottom(bgColorBottom);
                level.setBatchColor(batchColor);

                gameWorld = new GameWorld(getExchangeObject().getBatch(), level, assetManager, getExchangeObject().getSettings());
                gameWorld.loadDependencies();
            } else {
                Screen screen = new GameScreen(getExchangeObject(), gameWorld, level);
                getExchangeObject().getGame().setScreen(screen);
                dispose();
            }
        }
    }

    private Array<String> tokenizeStringValue(MapProperties props, String key) {
        Array<String> values = new Array<>();
        StringTokenizer tokenizer = new StringTokenizer(props.get(key, "", String.class), ",");
        while (tokenizer.hasMoreTokens()) {
            values.add(tokenizer.nextToken().trim().toLowerCase());
        }
        return values;
    }

    private AssetWrapper<Sound> getFallingSound(MapProperties props) {
        AssetWrapper<Sound> soundWrapper = null;
        String key = props.get("fallingSound", "", String.class).trim().toLowerCase();
        if (!key.isEmpty()) {
            key = Assets.Sounds.PATH + "/" + key;
            soundWrapper = new AssetWrapper<>(key, assetManager, Sound.class);
            soundWrapper.loadAsset();
        }
        return soundWrapper;
    }

    private AssetWrapper<Music> getMusic(MapProperties props) {
        AssetWrapper<Music> musicWrapper = null;
        String key = props.get("sound", "", String.class).trim().toLowerCase();
        if (!key.isEmpty()) {
            key = Assets.Sounds.PATH + "/" + key;
            musicWrapper = new AssetWrapper<>(key, assetManager, Music.class);
            musicWrapper.loadAsset();
        }
        return musicWrapper;
    }
}
