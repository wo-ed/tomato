package com.woed.tomato.screen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.storage.PreferencesSaveStateManager;
import com.woed.tomato.storage.SaveStateManager;
import com.woed.tomato.util.AdsController;
import com.woed.tomato.util.OsManager;

public class ScreenExchangeObject implements Disposable {

    private static final int DEFAULT_FONT_SIZE = 80;
    public static final String TOMATO_GREEN = "143800ff";
    static final String TOMATO_RED = "990000ff";
    private static final String TOMATO_GRAY = "b3b3b3ff";

    private BitmapFont mediumSizedFont;
    private Skin defaultSkin;
    private Music menuMusic;
    private Sound laughter;

    private Game game;
    private Batch batch;
    private Viewport menuViewport;
    private Viewport gameViewport;
    private InputMultiplexer inputMultiplexer = new InputMultiplexer();
    private AssetManager assetManager = new AssetManager();
    private SaveStateManager saveStateManager = new PreferencesSaveStateManager();
    private boolean updated;
    private I18NBundle bundle;
    private TextButton.TextButtonStyle greenButtonStyle;
    private CheckBox.CheckBoxStyle greenCheckBoxStyle;
    private OsManager osManager;

    private Settings settings;

    public ScreenExchangeObject(OsManager osManager) {
        this.osManager = osManager;
        settings = new Settings(osManager);
        mediumSizedFont = generateFreeTypeFont(DEFAULT_FONT_SIZE, Assets.Ui.MVBOLI_TTF);
        assetManager.load(Assets.Ui.UISKIN_JSON, Skin.class);
        assetManager.load(Assets.Ui.GREEN_UP_PNG, Texture.class);
        assetManager.load(Assets.Ui.GREEN_DOWN_PNG, Texture.class);
        assetManager.load(Assets.Properties.BUNDLE_PROPERTIES.replace(".properties", ""), I18NBundle.class);
        //assetManager.load(Assets.Sounds.MUSICBOX2_OGG, Music.class);
        assetManager.load(Assets.Sounds.LAUGHTER_OGG, Sound.class);

        TmxMapLoader tmxMapLoader = new TmxMapLoader();
        assetManager.setLoader(TiledMap.class, tmxMapLoader);
    }

    public boolean areDefaultResourcesLoaded() {
        if (updated) {
            return true;
        }
        updated = assetManager.update();
        if (updated) {
            defaultSkin = assetManager.get(Assets.Ui.UISKIN_JSON);
            Texture greenUp = assetManager.get(Assets.Ui.GREEN_UP_PNG);
            Texture greenDown = assetManager.get(Assets.Ui.GREEN_DOWN_PNG);
            bundle = assetManager.get(Assets.Properties.BUNDLE_PROPERTIES.replace(".properties", ""));
            //menuMusic = assetManager.get(Assets.Sounds.MUSICBOX2_OGG);
            laughter = assetManager.get(Assets.Sounds.LAUGHTER_OGG);

            SpriteDrawable greenUpDrawable = new SpriteDrawable(new Sprite(greenUp));
            SpriteDrawable greenDownDrawable = new SpriteDrawable(new Sprite(greenDown));

            greenButtonStyle = new TextButton.TextButtonStyle();
            greenButtonStyle.font = getMediumSizedFont();
            greenButtonStyle.fontColor = Color.valueOf(ScreenExchangeObject.TOMATO_GRAY);
            greenButtonStyle.up = greenUpDrawable;
            greenButtonStyle.down = greenDownDrawable;
            greenButtonStyle.downFontColor = Color.DARK_GRAY;

            greenCheckBoxStyle = new CheckBox.CheckBoxStyle();
            greenCheckBoxStyle.font = getMediumSizedFont();
            greenCheckBoxStyle.fontColor = Color.valueOf(ScreenExchangeObject.TOMATO_GRAY);
            greenCheckBoxStyle.checked = greenUpDrawable;
        }
        return updated;
    }

    void loopMusic() {
        if (menuMusic != null) {
            if (!menuMusic.isPlaying()) {
                menuMusic.play();
            }
            menuMusic.setLooping(true);
        }
    }

    void stopMusic() {
        if (menuMusic != null) {
            menuMusic.stop();
        }
    }

    BitmapFont generateFreeTypeFont(int size, String fontType) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontType));
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = size;
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();
        return font;
    }

    BitmapFont getMediumSizedFont() {
        return mediumSizedFont;
    }

    Skin getDefaultSkin() {
        return defaultSkin;
    }

    Music getMenuMusic() {
        return menuMusic;
    }

    Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    AdsController getAdsController() {
        return osManager.getAdsController();
    }

    public OsManager getOsManager() {
        return osManager;
    }

    public Batch getBatch() {
        return batch;
    }

    public void setBatch(Batch batch) {
        this.batch = batch;
    }

    Viewport getMenuViewport() {
        return menuViewport;
    }

    public void setMenuViewport(Viewport menuViewport) {
        this.menuViewport = menuViewport;
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    I18NBundle getBundle() {
        return bundle;
    }

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    @Override
    public void dispose() {
        mediumSizedFont.dispose();
        if (menuMusic != null) {
            menuMusic.stop();
        }
        assetManager.dispose();
    }

    public InputMultiplexer getInputMultiplexer() {
        return inputMultiplexer;
    }

    SaveStateManager getSaveStateManager() {
        return saveStateManager;
    }

    public Button createLocalizedGreenButton(String key) {
        return createGreenButton(bundle.get(key));
    }

    public Button createGreenButton(String caption) {
        Button button = new TextButton(caption, greenButtonStyle);
        return button;
    }

    CheckBox.CheckBoxStyle getGreenCheckBoxStyle() {
        return greenCheckBoxStyle;
    }

    TextButton.TextButtonStyle getGreenButtonStyle() {
        return greenButtonStyle;
    }

    public Sound getLaughter() {
        return laughter;
    }

    public Viewport getGameViewport() {
        return gameViewport;
    }

    public void setGameViewport(Viewport gameViewport) {
        this.gameViewport = gameViewport;
    }
}
