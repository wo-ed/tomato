package com.woed.tomato.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.woed.tomato.camera.CameraMover;
import com.woed.tomato.input.CameraGestureListener;
import com.woed.tomato.level.TiledLevelData;
import com.woed.tomato.resource.Assets;
import com.woed.tomato.util.AnimatedActor;

public class LevelDetailScreen extends MenuScreen {

    private TiledMap map;
    private TiledLevelData levelData;
    private OrthographicCamera movingCamera = new OrthographicCamera();
    private Viewport mapViewport = new ScreenViewport(movingCamera);
    private boolean isLoaded;
    private OrthogonalTiledMapRenderer renderer;
    private Button startLevelButton;
    private CameraMover cameraMover;
    private GestureDetector gestureDetector;
    private AssetManager assetManager;
    private Group lanternGroup;

    private Texture lantern0 = new Texture(Assets.Textures.LANTERN0_PNG);
    private Texture lantern1 = new Texture(Assets.Textures.LANTERN1_PNG);
    private TextureRegion lantern0Region = new TextureRegion(lantern0);
    private TextureRegion lantern1Region = new TextureRegion(lantern1);

    public LevelDetailScreen(ScreenExchangeObject exchangeObject, TiledLevelData levelData) {
        super(exchangeObject);

        this.levelData = levelData;
        assetManager = getExchangeObject().getAssetManager();
        assetManager.load(levelData.getMapPath(), TiledMap.class);

        movingCamera.setToOrtho(false);

        cameraMover = new CameraMover(movingCamera);
        cameraMover.setIdleVelocity(new Vector2(128, 0));
        cameraMover.idle();

        gestureDetector = new GestureDetector(new CameraGestureListener(cameraMover));
        getExchangeObject().getInputMultiplexer().addProcessor(gestureDetector);

        initializeUI();
    }

    private void initializeUI() {
        startLevelButton = getExchangeObject().createLocalizedGreenButton("play");
        startLevelButton.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y,
                                int pointer, int button) {
                LoadingScreen screen = new LoadingScreen(getExchangeObject(), levelData);
                getExchangeObject().getGame().setScreen(screen);
                dispose();
                screen.load();
            }
        });

        Group buttonGroup = new VerticalGroup().fill();
        buttonGroup.addActor(startLevelButton);

        getRootTable().row().expand().center();
        getRootTable().add(buttonGroup);

        lanternGroup = new HorizontalGroup();
        for (int i = 0; i < 3; i++) {
            lanternGroup.addActor(new Lantern());
        }

        getRootTable().row().bottom().right();
        getRootTable().add(lanternGroup);
    }

    @Override
    public void render(float deltaTime) {
        backgroundColor();
        if (Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            Screen screen = new LevelScreen(getExchangeObject(), LevelScreen.Movement.NONE);
            getExchangeObject().getGame().setScreen(screen);
            dispose();
            return;
        }

        if (assetManager.update() && !isLoaded) {
            map = assetManager.get(levelData.getMapPath());

            renderer = new OrthogonalTiledMapRenderer(map, getExchangeObject().getBatch());
            cameraMover.setMinPosition(new Vector2(movingCamera.viewportWidth / 2, movingCamera.viewportHeight / 2));
            MapProperties props = map.getProperties();
            int width = props.get("width", Integer.class);
            int height = props.get("height", Integer.class);
            int tileWidth = props.get("tilewidth", Integer.class);
            int tileHeight = props.get("tileheight", Integer.class);
            cameraMover.setMaxPosition(new Vector2(width * tileWidth - movingCamera.viewportWidth / 2,
                    height * tileHeight - movingCamera.viewportHeight / 2));
            isLoaded = true;
        }

        if (isLoaded) {
            cameraMover.move(deltaTime);
            renderer.setView(movingCamera);
            getExchangeObject().getBatch().setColor(Color.BLACK);
            mapViewport.apply();
            renderer.render();
        }

        getStage().act(deltaTime);
        getExchangeObject().getBatch().setColor(Color.WHITE);
        getStage().getViewport().apply();
        getStage().draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        if (renderer != null) {
            renderer.dispose();
        }
        getExchangeObject().getInputMultiplexer().removeProcessor(gestureDetector);
        assetManager.unload(levelData.getMapPath());
        lantern0.dispose();
        lantern1.dispose();
    }

    private class Lantern extends AnimatedActor {
        public Lantern() {
            super(1 / 32f,
                    lantern0Region, lantern1Region);
            getAnimation().setPlayMode(Animation.PlayMode.LOOP_RANDOM);
        }
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        mapViewport.update(width, height, false);
    }
}
