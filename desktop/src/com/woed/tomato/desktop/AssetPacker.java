package com.woed.tomato.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import com.badlogic.gdx.utils.Array;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

public class AssetPacker implements ApplicationListener {

    public static final int MAX_WIDTH;
    public static final int MAX_HEIGHT;

    static {
        MAX_WIDTH = MAX_HEIGHT = 2048;
    }

    public static final String TEXTURES_INPUT_PATH = "../images/packed";
    public static final String ASSETS_PATH = "../android/assets/";
    public static final String spriteSheets_OUTPUT_PATH = ASSETS_PATH + "spriteSheets";
    public static final String TEXTURE_PACK_FILE_NAME = "textures";
    public static final String COM = "com";
    public static final String COMPANY = "woed";
    public static final String APP_NAME = "tomato";
    public static final String PACKAGE_NAME = "resource";
    public static final String ASSETS_CLASS_NAME = "Assets";
    public static final String ATLAS_KEYS_CLASS_NAME = "AtlasKeysTest";
    public static final String RESOURCES_PACKAGE_PREFIX = "../core/src/" + COM + "/" + COMPANY + "/" + APP_NAME +
            "/" + PACKAGE_NAME;
    public static final String RESOURCES_CLASS_FILE_PATH = RESOURCES_PACKAGE_PREFIX + "/" + ASSETS_CLASS_NAME + ".java";
    public static final String ATLAS_KEYS_FILE_PATH = RESOURCES_PACKAGE_PREFIX + "/" + ATLAS_KEYS_CLASS_NAME + ".java";

    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        new LwjglApplication(new AssetPacker(), config);
    }

    @Override
    public void create() {
        packTextures();
        createAtlasKeysFile();
        createResourcesFile();
        packTilesets();

        Gdx.app.exit();
    }

    private void packTilesets() {
        String inputPath = "../projects/tiles";
        FileHandle[] inputFolderHandles = Gdx.files.local(inputPath).list();
        Stream<FileHandle> tileHandles = Arrays.stream(inputFolderHandles).filter(f -> f.extension().equals("png"));
        tileHandles.forEach(tileHandle -> packTileset(tileHandle.file(), tileHandle.nameWithoutExtension()));
    }

    private void packTileset(File input, String packFileName) {
        Settings texturePackerSettings = createTexturePackerSettings();
        texturePackerSettings.grid = true;
        texturePackerSettings.pot = false;
        texturePackerSettings.alias = false;

        String tmpFolderPath = "tmp";
        FileHandle tmpFolder = Gdx.files.local(tmpFolderPath);
        tmpFolder.deleteDirectory();
        tmpFolder.mkdirs();
        String outputPath = ASSETS_PATH + "levels";

        final BufferedImage source;
        try {
            source = ImageIO.read(input);

            int idx = 0;
            int tileSize = 128;
            int columns = MathUtils.ceil(source.getWidth() / tileSize);
            int rows = MathUtils.ceil(source.getHeight() / tileSize);

            for (int y = 0; y < rows; y++) {
                for (int x = 0; x < columns; x++) {
                    String name = String.format(Locale.getDefault(), "tile_%02d", idx);
                    File tileFile = Gdx.files.internal(tmpFolderPath + "/" + name + ".png").file();
                    ImageIO.write(source.getSubimage(x * tileSize, y * tileSize, tileSize, tileSize), "png", tileFile);
                    idx++;
                }
            }

            texturePackerSettings.maxWidth = source.getWidth() + (columns + 1) * texturePackerSettings.paddingX;
            texturePackerSettings.maxHeight = source.getHeight() + (rows + 1) * texturePackerSettings.paddingY;

        } catch (IOException e) {
            e.printStackTrace();
        }

        TexturePacker.process(texturePackerSettings, tmpFolderPath, outputPath, packFileName);
        tmpFolder.deleteDirectory();
    }

    private static void createAtlasKeysFile() {
        TextureAtlas atlas = null;
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(ATLAS_KEYS_FILE_PATH))) {

            System.out.println("Writing atlas keys class");

            StringBuffer stringBuffer = createStringBufferWithPackageDeclaration();
            stringBuffer.append(System.lineSeparator());
            stringBuffer.append("public final class ");
            stringBuffer.append(ATLAS_KEYS_CLASS_NAME);
            stringBuffer.append(" {");

            stringBuffer.append(System.lineSeparator());
            appendSpaces(stringBuffer, 1);
            stringBuffer.append("public static final class ");
            String filename = TEXTURE_PACK_FILE_NAME;
            stringBuffer.append((filename.charAt(0) + "").toUpperCase() + filename.substring(1, filename.length()));
            stringBuffer.append(" {");

            atlas = new TextureAtlas(spriteSheets_OUTPUT_PATH + "/" + TEXTURE_PACK_FILE_NAME + ".atlas");
            Array<String> usedRegionNames = new Array<>();
            for (TextureAtlas.AtlasRegion region : atlas.getRegions()) {
                if (usedRegionNames.contains(region.name, false)) {
                    continue;
                }
                usedRegionNames.add(region.name);
                stringBuffer.append(System.lineSeparator());
                appendSpaces(stringBuffer, 2);
                stringBuffer.append("public static final String ");
                stringBuffer.append(region.name.toUpperCase().replace('/', '_'));
                stringBuffer.append(" = ");
                stringBuffer.append('\"');
                stringBuffer.append(region.name);
                stringBuffer.append('\"');
                stringBuffer.append(";");
            }
            stringBuffer.append(System.lineSeparator());
            appendSpaces(stringBuffer, 1);
            stringBuffer.append("}");
            stringBuffer.append(System.lineSeparator());
            stringBuffer.append("}");

            writer.write(stringBuffer.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (atlas != null) {
                    atlas.dispose();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void packTextures() {
        Settings settings = createTexturePackerSettings();
        TexturePacker.process(settings, TEXTURES_INPUT_PATH, spriteSheets_OUTPUT_PATH, TEXTURE_PACK_FILE_NAME);
    }

    private static Settings createTexturePackerSettings() {
        Settings settings = new Settings();
        settings.maxWidth = MAX_WIDTH;
        settings.maxHeight = MAX_HEIGHT;
        settings.combineSubdirectories = true;
        settings.duplicatePadding = true;
        settings.premultiplyAlpha = false;
        return settings;
    }

    private static void createResourcesFile() {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(RESOURCES_CLASS_FILE_PATH))) {

            System.out.println("Writing resources class");

            StringBuffer stringBuffer = createStringBufferWithPackageDeclaration();
            Path rootPath = Paths.get(ASSETS_PATH);
            Stream<Path> pathStream = Files.list(rootPath);
            writeDirectory(ASSETS_CLASS_NAME, pathStream, stringBuffer, 0);
            writer.write(stringBuffer.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static StringBuffer createStringBufferWithPackageDeclaration() {
        StringBuffer stringBuffer = new StringBuffer("package ");
        stringBuffer.append(COM);
        stringBuffer.append(".");
        stringBuffer.append(COMPANY);
        stringBuffer.append(".");
        stringBuffer.append(APP_NAME);
        stringBuffer.append(".");
        stringBuffer.append(PACKAGE_NAME);
        stringBuffer.append(";");
        stringBuffer.append(System.lineSeparator());
        return stringBuffer;
    }

    private static void writeDirectory(String dirName, Stream<Path> pathStream, StringBuffer stringBuffer, int i) throws IOException {
        Iterator<Path> iterator = pathStream.iterator();
        boolean empty = !iterator.hasNext();
        if (!empty) {
            stringBuffer.append(System.lineSeparator());
            appendSpaces(stringBuffer, i);
            stringBuffer.append("public ");
            if (i > 0) {
                stringBuffer.append("static ");
            }
            stringBuffer.append("final class ");
            String className = (dirName.charAt(0) + "").toUpperCase() + dirName.substring(1, dirName.length());
            stringBuffer.append(className);
            stringBuffer.append(" {");

            if (i > 0) {
                stringBuffer.append(System.lineSeparator());
                appendSpaces(stringBuffer, (i + 1));
                stringBuffer.append("public static final String PATH = ");
                stringBuffer.append('\"');
                stringBuffer.append(dirName.toString().replace(ASSETS_PATH, "").replace(File.separatorChar, '/'));
                stringBuffer.append('\"');
                stringBuffer.append(";");
            }
        }
        while (iterator.hasNext()) {
            Path path = iterator.next();
            if (Files.isDirectory(path)) {
                writeDirectory(path.getFileName().toString(), Files.list(path), stringBuffer, i + 1);
            } else {
                writeFile(path, stringBuffer, i + 1);
            }
        }
        if (!empty) {
            stringBuffer.append(System.lineSeparator());
            appendSpaces(stringBuffer, i);
            stringBuffer.append("}");
        }
    }

    private static void appendSpaces(StringBuffer stringBuffer, int level) {
        for (int j = 0; j < level * 4; j++) {
            stringBuffer.append(" ");
        }
    }

    public static final String FIELD_PREFIX = "public static final String ";

    private static void writeFile(Path path, StringBuffer stringBuffer, int level) {
        stringBuffer.append(System.lineSeparator());
        appendSpaces(stringBuffer, level);
        stringBuffer.append(FIELD_PREFIX);
        stringBuffer.append(path.getFileName().toString().toUpperCase().replace(".", "_"));
        stringBuffer.append(" = \"");
        stringBuffer.append(path.toString().replace(ASSETS_PATH, "").replace(File.separatorChar, '/'));
        stringBuffer.append("\"");
        stringBuffer.append(";");
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
