package com.woed.tomato.desktop;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.badlogic.gdx.utils.XmlWriter;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

public class MapRescaler {

    public static final String levelsPath = "assets/levels/";
    public static final float factor = 4;

    public static void main(String[] args) throws IOException {

        String level = levelsPath + "map99.tmx";
        XmlReader reader = new XmlReader();
        Element root = reader.parse(new FileInputStream(level));

        Array<Element> objects = root.getChildByName("objectgroup").getChildrenByName("object");
        for (Element obj : objects) {
            obj.setAttribute("x", "" + (int) (Integer.parseInt(obj.getAttribute("x")) * factor));
            obj.setAttribute("y", "" + (int) (Integer.parseInt(obj.getAttribute("y")) * factor));
            obj.setAttribute("width", "" + (int) (Integer.parseInt(obj.getAttribute("width")) * factor));
            obj.setAttribute("height", "" + (int) (Integer.parseInt(obj.getAttribute("height")) * factor));
        }
        XmlWriter writer = new XmlWriter(new FileWriter(level));
        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        writer.text(root.toString());
        writer.close();
    }
}
