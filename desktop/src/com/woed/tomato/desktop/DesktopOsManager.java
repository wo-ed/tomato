package com.woed.tomato.desktop;

import com.woed.tomato.util.AdsController;
import com.woed.tomato.util.DummyAdsController;
import com.woed.tomato.util.OsManager;

public class DesktopOsManager implements OsManager {
    private DummyAdsController adsController = new DummyAdsController();

    @Override
    public AdsController getAdsController() {
        return adsController;
    }

    @Override
    public boolean isScreenAlwaysOn() {
        return false;
    }

    @Override
    public void setScreenAlwaysOn(boolean screenAlwaysOn) {
    }
}
