package com.woed.tomato.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.woed.tomato.main.TomatoGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 0;
        config.height = 0;
        //config.fullscreen = true;
        new LwjglApplication(new TomatoGame(new DesktopOsManager()), config);
    }
}
