<?xml version="1.0" encoding="UTF-8"?>
<tileset name="snow128" tilewidth="128" tileheight="128" tilecount="18" columns="6" margin="1" spacing="2">
 <image source="snow128.png" width="768" height="384"/>
 <tile id="1">
  <properties>
   <property name="groundType" value="snow.ogg"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="groundType" value="snow.ogg"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="groundType" value="snow.ogg"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="groundType" value="snow.ogg"/>
  </properties>
 </tile>
</tileset>
