<?xml version="1.0" encoding="UTF-8"?>
<tileset name="leaves128" tilewidth="128" tileheight="128" tilecount="24" columns="8" margin="1" spacing="2">
 <image source="leaves128.png" width="1024" height="384"/>
 <tile id="1">
  <properties>
   <property name="groundType" value="leaves.ogg"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="groundType" value="splash.ogg"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="groundType" value="splash.ogg"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="groundType" value="splash.ogg"/>
  </properties>
  <animation>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
  </animation>
 </tile>
 <tile id="6">
  <properties>
   <property name="groundType" value="splash.ogg"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="groundType" value="splash.ogg"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="groundType" value="leaves.ogg"/>
  </properties>
 </tile>
</tileset>
