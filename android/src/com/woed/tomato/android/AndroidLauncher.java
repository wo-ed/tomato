package com.woed.tomato.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.utils.Array;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.woed.tomato.main.TomatoGame;
import com.woed.tomato.util.AdsController;
import com.woed.tomato.util.OsManager;

public class AndroidLauncher extends AndroidApplication implements AdsController, OsManager {

    public static final String BANNER_AD_UNIT_ID = "ca-app-pub-5519384153835422/2811367393";
    public static final String INTERSTITIAL_AD_UNIT_ID = "ca-app-pub-5519384153835422/6795093799";
    public static final Array<String> TEST_DEVICES = new Array<>();

    static {
        TEST_DEVICES.add("84A841D29931BF5948506C89AEC01C7A"); //Sony Xperia SP
        TEST_DEVICES.add("90C78A0C40FF3B601102B1E0476649BF"); //LG G4
    }

    private AdView bannerAd;
    private InterstitialAd interstitialAd;
    private View gameView;
    private AdRequest.Builder builder;

    private RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        //config.useImmersiveMode = true;

        // Define the layout
        layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        //params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        layout.setLayoutParams(params1);

        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        // Create a gameView and a bannerAd AdView
        gameView = initializeForView(new TomatoGame(this), config);
        layout.addView(gameView, params2);

        if (areAdsEnabled()) {
            builder = new AdRequest.Builder();
            for (String testDevice : TEST_DEVICES) {
                builder.addTestDevice(testDevice);
            }
            setUpAds();
        }
        setContentView(layout);
    }

    private void setUpAds() {
        if (!areAdsEnabled()) {
            return;
        } else if (isWifiConnected()) {
            setupBannerAd();
            RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            layout.addView(bannerAd, params3);
        }
        setUpInterstitialAd();
    }

    private void setupBannerAd() {
        bannerAd = new AdView(this);
        bannerAd.setVisibility(View.INVISIBLE);
        bannerAd.setBackgroundColor(0xff000000); // black
        bannerAd.setAdUnitId(BANNER_AD_UNIT_ID);
        bannerAd.setAdSize(AdSize.SMART_BANNER);
        AdRequest bannerRequest = builder.build();
        bannerAd.loadAd(bannerRequest);
    }

    private void setUpInterstitialAd() {
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(INTERSTITIAL_AD_UNIT_ID);
        AdRequest interstitialRequest = builder.build();
        interstitialAd.loadAd(interstitialRequest);
    }

    @Override
    public void showBannerAd() {
        if (!areAdsEnabled()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bannerAd.setVisibility(View.VISIBLE);
                AdRequest.Builder builder = new AdRequest.Builder();
                AdRequest ad = builder.build();
                bannerAd.loadAd(ad);
            }
        });
    }

    @Override
    public void hideBannerAd() {
        if (!areAdsEnabled()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bannerAd.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public boolean isWifiConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return (ni != null && ni.isConnected());
    }

    @Override
    public void showInterstitialAd(final Runnable then) {
        if (!areAdsEnabled()) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (then != null) {
                    interstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            Gdx.app.postRunnable(then);
                            AdRequest.Builder builder = new AdRequest.Builder();
                            AdRequest ad = builder.build();
                            interstitialAd.loadAd(ad);
                        }
                    });
                }
                if (interstitialAd.isLoaded()) {
                    interstitialAd.show();
                }
            }
        });
    }

    @Override
    public boolean areAdsEnabled() {
        return false;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        gameView.dispatchKeyEvent(event);

        //BannerAd bugfix
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    @Override
    public AdsController getAdsController() {
        return this;
    }

    @Override
    public boolean isScreenAlwaysOn() {
        int flags = getWindow().getAttributes().flags;
        return (flags & WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON) != 0;
    }

    @Override
    public void setScreenAlwaysOn(final boolean screenAlwaysOn) { //TODO: save preferences here?
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                if (screenAlwaysOn) {
                    getWindow().addFlags(flags);
                } else {
                    getWindow().clearFlags(flags);
                }
            }
        });
    }
}